var SnippetLogin = function ()
{
	var e = $("#m_login"),planName,cancel;var forgetMobileNumber;
		i = function (e, i, a,value)
		{ 	
			if(a == undefined || a == null || a == ""){
				e.find(".alert").remove()
			}
			else
			{
				var l = $('<div class="m-alert m-alert--outline alert alert-' + i + ' alert-dismissible" role="alert">\t\t\t<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\t\t\t<span></span>\t\t</div>');
				e.find(".alert").remove(), l.prependTo(e), mUtil.animateClass(l[0], "fadeIn animated"), l.find("span").html(a)
				$(".close").click(function(e){
					var l = $("#view_body").find(".plan-update");l.removeClass("plan_update");
				});
			}
		},
		a = function ()
		{
			e.removeClass("m-login--forget-password"), e.removeClass("m-login--signup"), e.addClass("m-login--signin"), mUtil.animateClass(e.find(".m-login__signin")[0], "flipInX animated")
		},
		l = function ()
		{
			$("#m_login_forget_password").click(function (i)
			{
				$("#prices-list,.m-login__signin,.m-login__account").hide();$(".m-login__forget-password").resetForm().show();
				i.preventDefault(), e.removeClass("m-login--signin"), e.removeClass("m-login--signup"), e.addClass("m-login--forget-password"), mUtil.animateClass(e.find(".m-login__forget-password")[0], "flipInX animated")
			}), $(".m_login_forget_password_cancel").click(function (h)
			{	
				var g = $(".password_set");g.validate().resetForm(),
				$(".m-login__signup,.m-login__forget-password").hide();
				h.preventDefault(), a();
				$(".m-login__account,.m-login__signin").show();$(".m-login__signin").validate();
				var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
				setTimeout(function(){
					if(isMobile) 
						app.appendSavedDetails();
					else
						cookiesRetrive();
				},100)
			   
			}), /*$("#m_login_signup").click(function (i)
			{
				i.preventDefault(), e.removeClass("m-login--forget-password"), e.removeClass("m-login--signin"), e.addClass("m-login--signup"), mUtil.animateClass(e.find(".m-login__signup")[0], "flipInX animated")
			}),*/ $("#m_login_signup_cancel").click(function (i)
			{	
				$(".m-login__signup").hide();
				$(".m-login__signin,.m-login__account").show();cancel = true;
				window.sessionStorage.removeItem("GeneratedOTP")
				window.sessionStorage.removeItem("verfiedOTP")
			})
		};
	return {
		init: function ()
		{
			var plan,amount,backRetrive;
			$(".purchase_value").unbind().click(function(f){
				var l = $("#m_login").find(".m-login__signup");
				//i(l,"","",true);
				backRetrive = true;
				plan = $(this).attr("name");
				amount = $(this).attr("value"); 
				var planCount = $(this).attr("id"); 
				$("#prices-list,.close").hide();
				if(planCount != null || planCount != undefined){ 
					//window.sessionStorage.setItem("verfiedOTP",true);
					//shopRenewalSubscription(plan,amount);
				}
				else
				{  
					$(".m-login__signup,.sign_up_form").show();$(".forget_password_title").text("Sign Up");
					$(".m-login__desc").text(plan);
					if(cancel) 
					{
						cancel = false;
						$(".m-login__signup").validate().resetForm();
					}
					
					setTimeout(function(e){
						planPurchase(e,i);
					},200);
				}
				var l = $(".m-login__signup");
			}),
			$(".click").unbind().click(function(e){ 
				setTimeout(function(e){
					$(".button_2").hide();
				},100)
				
				$(".m-login__signup,.sign_up_form").hide();
				$("#prices-list,.close").show();
			});
			$("#m_login_signup").click(function (l){
				
				$(".m-login__signin,.m-login__account, .password_set,.m-login__signup").hide();var l = $(".m-login__signup");l.clearForm(),l.resetForm();
				$("#prices-list,.close").show();$(".button_2").hide();
			});
			$("#m_guest_login").click(function (l){
				$("#mobile").val("9999999999");
				$("#password").val("India@2019");
				$("#m_login_signin_submit").addClass("m-loader m-loader--right m-loader--light").attr("disabled", !0);
				
				setTimeout(function(e){
					loginUser(a,i,l);
				},80)
				
			});
			l(), $("#m_login_signin_submit").click(function (e)
			{  
				$(".m-login__account").show();
				e.preventDefault();
				var a = $(this),
					l = $(this).closest("form");
				
				l.validate(
				{
					rules:
					{
						mobile:
						{
							required: !0
							
						},
						password:
						{
							required: !0
						}
					}
				}), l.valid() && (a.addClass("m-loader m-loader--right m-loader--light").attr("disabled", !0), l.ajaxSubmit(
				{
					url: "",
					success: function (e, t, r, s)
					{
						loginUser(a,i, l);
						/*setTimeout(function ()
						{
							a.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(l, "danger", "Incorrect username or password. Please try again.")
						}, 2e3)*/
						
					}
				}))
			}), 
			$("#m_login_signup_submit").click(function (l)
			{ 
				/*$(".m-login__desc").text("");*/
				//window.sessionStorage.removeItem("GeneratedOTP")
				l.preventDefault();
				var t = $(this),
					r = $(this).closest("form");
				r.validate(
				{
					rules:
					{
						shopName:
						{
							required: !0
						},
						mobileNumber:
						{
							required: !0,
							
							
						},
						agree:
						{
							required: !0
						}
						/*password:
						{
							required: !0,
							minlength:8,
							//atLeastOneUppercaseLetter: true,
						},
						rpassword:
						{
							required: !0,
							equalTo: "#register_password",
						},*/
					}
					
					/*messages: { // custom messages for radio buttons and checkboxes
			               
			                password:{
			                	
			                	//required:"password is required",
			                	minlength:"Enter minimum 8 characters",
			                	atLeastOneUppercaseLetter : "enter atlest one capital letter"
			                	//atLeastOneSymbol:"enter atlest one one special character"
			                },
			            }*/
				}), r.valid() && (t.addClass("m-loader m-loader--right m-loader--light").attr("disabled", !0), r.ajaxSubmit(
				{
					url: "",
					success: function (l, s, n, o)
					{	
						/*setTimeout(function ()
						{
							t.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), r.clearForm(), r.validate().resetForm(), a();
							var l = e.find(".m-login__signin form");
							l.clearForm(), l.validate().resetForm(), i(l, "success", "Thank you. To complete your registration please check your email.")
						}, 2e3)*/
						t.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1)
						if(!backRetrive){
							var signUpData = JSON.parse(window.sessionStorage.getItem("signUpData"));
							plan = signUpData.planName;
							amount = signUpData.planPrice;
						}
						var dataToSend = JSON.stringify({ 
								shopName: 	 $('#shopname').val(),
								mobileNumber:$('#number').val(),
								planName:plan,
								planPrice:amount,
						});
						window.sessionStorage.setItem("signUpData",dataToSend);
						shopRegisterPayment(l, s, n, o, t, r, a, e, i,plan,amount);
					}
				}))
			}), 
			
			$(".button_1").click(function(e){
				$("#prices-list,.close").hide();
				$(".m-login__signin,.m-login__account").show();
				setTimeout(function(e){
					if($("#remember").val() !== "YES")
						$(".m-login__signin").validate().resetForm();
				},50);
				checkRememberCookies();
				
				var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
				if(isMobile) 
					 app.appendSavedDetails();
				else
					cookiesRetrive();
			});
			$(".m_login_signup_submit_1").click(function (l)
					{  
						var forgetId = $(this).attr("id");
						$(".m_sweetalert_demo_8").show();$(".success_ok").removeClass("fa fa-check-circle");

						l.preventDefault();
						var t = $(this),
							r = $(this).closest("form");
						r.validate(
						{
							rules:
							{
//								pass:
//								{
//									required: !0,
//									//minlength:8,
//									//atLeastOneUppercaseLetter: true,
//								},
								rpassword:
								{
									required: !0,
									equalTo: "#register_password",
								},
								
							}
							
							/*messages: { // custom messages for radio buttons and checkboxes
					               
					                password:{
					                	
					                	//required:"password is required",
					                	minlength:"Enter minimum 8 characters",
					                	atLeastOneUppercaseLetter : "enter atlest one capital letter"
					                	//atLeastOneSymbol:"enter atlest one one special character"
					                },
					            }*/
						}), r.valid() && (t.addClass("m-loader m-loader--right m-loader--light").attr("disabled", !0), r.ajaxSubmit(
						{
							url: "",
							success: function (l, s, n, o)
							{   
								/*setTimeout(function ()
								{
									t.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), r.clearForm(), r.validate().resetForm(), a();
									var l = e.find(".m-login__signin form");
									l.clearForm(), l.validate().resetForm(), i(l, "success", "Thank you. To complete your registration please check your email.")
								}, 2e3)*/
								setTimeout(function(){
									if(forgetId == "reset_actions")
										forgetPasswordService(forgetMobileNumber,t);
									else
									registerUser(l, s, n, o, t, r, a, e, i);
								},100)
								
							}
						}))
					}), 
					
			$("#m_login_forget_password_submit").click(function (l)
			{
				
				l.preventDefault();
				var t = $(this),
					r = $(this).closest("form");
					forgetMobileNumber = $("#m_mobile").val();
				r.validate(
				{
					rules:
					{
						mobile:
						{
							required: !0
						}
					}
				}), r.valid() && (t.addClass("m-loader m-loader--right m-loader--light").attr("disabled", !0), r.ajaxSubmit(
				{
					url: "",
					success: function (l, s, n, o)
					{
						setTimeout(function ()
						{
							//t.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), r.clearForm(), r.validate().resetForm(), a();
							/*var l = e.find(".m-login__signin form");
							l.clearForm(), l.validate().resetForm(), i(l, "success", "Cool! Password recovery instruction has been sent to your email.")*/
							forgetPassword(forgetMobileNumber,t,l,i,r);
						}, 2e3)
					}
				}))
			}),
			$(".toggle-password-signup").click(function() {  
					
				  $(this).toggleClass("fa-eye fa-eye-slash");
				  var input = $($(this).attr("toggle")); 
				  //var input = $("#register_password"); 
				  if (input.attr("type") == "password") {  
				    input.attr("type", "text");
				  } else {
				    input.attr("type", "password"); 
				  }
			}),
			$(".toggle-password-login").click(function() {  
										
				  $(this).toggleClass("fa-eye fa-eye-slash");
				  var input = $($(this).attr("toggle")); 
				  if (input.attr("type") == "password") {   
				    input.attr("type", "text");
				  } else {
				    input.attr("type", "password"); 
				  }
			});
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}();
jQuery(document).ready(function ()
{
	SnippetLogin.init();
});
