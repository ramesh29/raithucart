var finalNumber;
$(document).ready(function(){
	
	var status = getUrlParameter('status');
	if(window.location.href.indexOf("index") > -1 &&  window.sessionStorage.getItem("login"))
	{  
		window.location.href = "./home.html";
	}
	
	var logout = getUrlParameter('logout');
	
	if(logout == 'true')
	{
		$('#user_login_error').text("Logout successful");	
	}else if(logout == 'expired')
	{
		$('#user_login_error').text("Session Expried.Please relogin");
	}
	
	var signUpData = JSON.parse(window.sessionStorage.getItem("signUpData"));
	var paymentMode = getUrlParameter('subscriptionFor');
	window.sessionStorage.setItem("plan",getUrlParameter('plan'));
	window.sessionStorage.setItem("subscriptionFor",getUrlParameter('subscriptionFor'));
	
	if(status == 'OK')
	{	
		if(paymentMode == 'RenewalPayment')
		{ 
			var dataToSend = JSON.stringify({ 
				userId:window.sessionStorage.getItem("userId"),
				plan:window.sessionStorage.getItem("plan")
			});
			$("#content").empty();
			
			setTimeout(function(e){
				loadProfile();
				setTimeout(function(e){
					var data = callingAdminServices("POST","application/json","json","loginService/renewShop",dataToSend);
					if(data.success)
					{
						var l = $("#m_portlet_tab_1_1").find("#plan-update");l.addClass("plan_update");
						i(l, "success", data.messege)
					}
					else
					{
						var l = $("#m_portlet_tab_1_1").find("#plan-update");l.addClass("plan_update");
						i(l, "danger", data.messege)
					}
					var userDetails = callingAdminServices("GET","application/json","json","shopService/shopLogin/"+window.sessionStorage.getItem("userId"),null);
			    	window.sessionStorage.setItem("userDetails",JSON.stringify(userDetails));
				},160)
			},300);
		}
		else
		{
			$(".m-login__signup,.password_set").show();
			$(".sign_up_form,.m-login__signin,.m-login__account").hide();
			window.sessionStorage.removeItem("verfiedOTP");
			window.sessionStorage.setItem("plan",getUrlParameter('plan'));
			window.sessionStorage.setItem("subscriptionFor",getUrlParameter('subscriptionFor'));
			var l = $("#m_login").find(".password_set");
			i(l, "success", "Payment is successful.Please set the password")
		}
	}
	else if(status == 'F')
	{	
		if(paymentMode == 'RenewalPayment')
		{	$("#content").empty();
			setTimeout(function(e){
				loadProfile();
				setTimeout(function(e){
					var l = $("#m_portlet_tab_1_1").find("#plan-update");l.addClass("plan_update");
					i(l, "danger", "Payment failed.Please try again")
				},160)
			},300 );
		}
		else
		{
			$(".m_sweetalert_demo_8").hide();$(".success_ok").addClass("fa fa-check");
			$(".m-login__signup").show();$(".password_set,.m-login__signin,.m-login__account").hide();
			$(".m-login__desc").text(signUpData.planName);
			var shopName = signUpData.shopName;
			var mobileNumberS = signUpData.mobileNumber;
			mobileNumber = mobileNumberS;
			setTimeout(function(){
				$('#shopname').val(shopName);
				$('#number').val(mobileNumberS);finalNumber = signUpData.mobileNumber;
			},100);
			var l = $("#m_login").find(".m-login__signup");
			 l.validate(),l.resetForm(),i(l, "danger", "Payment failed.Please try again");
			planPurchase();
		}

	}else if(status == 'C')
	{	
		if(paymentMode == 'RenewalPayment')
		{	$("#content").empty();
			setTimeout(function(e){
				loadProfile();
				setTimeout(function(e){
					var l = $("#m_portlet_tab_1_1").find("#plan-update");l.addClass("plan_update");
					i(l, "danger", "Payment Cancelled.Please try again")
				},160)
			},300 );
		}
		else
		{
			$(".m_sweetalert_demo_8").hide();$(".success_ok").addClass("fa fa-check");
			$(".m-login__signup").show();$(".password_set,.m-login__signin,.m-login__account").hide();
			$(".m-login__desc").text(signUpData.planName);
			var shopName = signUpData.shopName;
			var mobileNumberS = signUpData.mobileNumber; 
				mobileNumber = mobileNumberS;
			setTimeout(function(){
				$('#shopname').val(shopName);
				$('#number').val(mobileNumberS);finalNumber = signUpData.mobileNumber;
			},100);
			var l = $("#m_login").find(".m-login__signup");
			i(l, "danger", "Payment Cancelled.Please try again");
		}

	}
	if(document.getElementById("number") != null && document.getElementById("number") != undefined)
	{
		document.getElementById("number").addEventListener('keyup', function() {
			if((this.value).length == 10 && finalNumber == this.value)
			{  
				$(".m_sweetalert_demo_8").hide();$(".success_ok").addClass("fa fa-check");
				window.sessionStorage.setItem("verfiedOTP",true)
			}
			else
			{
				$(".m_sweetalert_demo_8").hide();$(".success_ok").addClass("fa fa-check");
				$(".m-login__signup").show();$(".password_set,.m-login__signin,.m-login__account").hide();
				$(".m-login__desc").text(signUpData.planName);
				var l = $("#m_login").find(".m-login__signup");
				i(l, "", "");
				//planPurchase();
			}
		});
	}
	var uri = window.location.toString();
	if (uri.indexOf("?") > 0) {
		    var clean_uri = uri.substring(0, uri.indexOf("?"));
		    window.history.replaceState({}, document.title, clean_uri);
	}
	if(document.getElementById("number") != null && document.getElementById("number") != undefined)
		{
		document.getElementById("number").addEventListener('keyup', function() {
			if((this.value).length == 10 && finalNumber == this.value)
			{  
				$(".m_sweetalert_demo_8").hide();$(".success_ok").addClass("fa fa-check");
				window.sessionStorage.setItem("verfiedOTP",true)
			}	
			else
			{
				$(".m_sweetalert_demo_8").show();$(".success_ok").removeClass("fa fa-check");
				window.sessionStorage.setItem("verfiedOTP",false);
			}
		});
		}
	
	$("input[type='checkbox']").change(function() {
		
		if ($('#remember').is(":checked"))
			$("input[type='checkbox']").val("YES");
		else
			$("input[type='checkbox']").val("NO");
	});	
	var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    if(!isMobile){
    	try{ cookiesRetrive(); }catch(e){}
    } 
});


function cookiesRetrive()
{
	var remember = $.cookie('remember');
	if ( remember == 'true' ) 
	{
		var username = $.cookie('username');
		var password = $.cookie('password');
		// autofill the fields
		$('#mobile').val(username);
		$('#password').val(password);
		$("input[type='checkbox']").prop( "checked", true )
	}
}


function savedDetails(data)
{
	const[userName,password,remValue] = data.split("/");
	$('#mobile').val(userName);
	$('#password').val(password);
	$("#remember").val(remValue);
	if(remValue === "YES")
		$("input[type='checkbox']").prop( "checked", true );
	else
		$("input[type='checkbox']").prop( "checked", false );
}


function loginUser(a,i,l)
{	
	window.sessionStorage.setItem("adminApiKey",JSON.stringify("abc"));
	getApiKey();
	var dataToSend = JSON.stringify({ 
		mobileNumber:$('#mobile').val(), 
		password: 	 $('#password').val()
    });
	
	var data = callingAdminServices("POST","application/json","json","loginService/shopLogin",dataToSend);/*alert(JSON.stringify(data.mobileNumber))*/
	window.sessionStorage.setItem("mobileNumber",JSON.stringify(data.mobileNumber));
	var finalJson = data;
	if(finalJson.success)
	{
		/*if(false){
			Swal.fire({
				title: 'Your Plan expired.Please click on renewal to continue services',
				input: 'number',
				inputPlaceholder: "Enter OTP",
				showCancelButton: true,
			}).then(function(result) {
				
		 	});
		}else
		{*/
			sessionVariablesSet(finalJson);
			/*var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    		if(isMobile) 
			registerDeviceUUID();*/
			try{checkRememberCookies();}catch(e){}
			window.location.href = "./home.html";
		//}
			
	}
	else
	{
		$("#m_login_signin_submit").removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(l, "danger", "Incorrect username or password. Please try again.")
	}
	//var uuid = new DeviceUUID().get();console.log(uuid);alert(uuid)
	
}


function checkRememberCookies(remCheck)
{
	var mobileNumber = $('#mobile').val();
	var password 	 = $('#password').val();
	if ($('#remember').is(":checked"))
			$("input[type='checkbox']").val("YES");
		else
			$("input[type='checkbox']").val("NO");
	/*var remeberValue;
	if(remCheck === "YES")
		remeberValue = remCheck;
	else*/
		remeberValue = $("#remember").val();
	var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
	
    if(isMobile) {
    	if(remeberValue === "YES")
    		app.remeberDetails(mobileNumber,password,remeberValue);
    	else
    		app.remeberDetails("","",remeberValue);
    }
    else
    	 desktopRemeberMe(remeberValue)
}

function desktopRemeberMe(remeberValue)
{
	if (remeberValue === "YES") 
	{
		var username = $('#mobile').val();
		var password = $('#password').val();
		$.cookie('username', username);
		$.cookie('password', password);
		$.cookie('remember', true);
		// set cookies to expire in 14 days
		/*$.cookie('username', username, { expires: 14 });*/
	} 
	else 
	{
		$.cookie('username', null);
		$.cookie('password', null);
		$.cookie('remember', "NO");
	}
}

function registerDeviceUUID()
{
	var du = new DeviceUUID().parse();
    var dua = [
        du.language,
        du.platform,
        du.os,
        du.cpuCores,
        du.isAuthoritative,
        du.silkAccelerated,
        du.isKindleFire,
        du.isDesktop,
        du.isMobile,
        du.isTablet,
        du.isWindows,
        du.isLinux,
        du.isLinux64,
        du.isMac,
        du.isiPad,
        du.isiPhone,
        du.isiPod,
        du.isSmartTV,
        du.pixelDepth,
        du.isTouchScreen
    ];
   /* var uuid = du.hashMD5(dua.join(':'));
    var dataToSend = JSON.stringify({ 
		 uuid:uuid,
		 userId:window.sessionStorage.getItem("userId")
		});
    var data = callingAdminServices("PUT","application/json","json","loginService/updateUUID",dataToSend);*/
    //alert(JSON.stringify(data))
    alert(dua[7])
    var uuid = du.hashMD5(dua.join(':'));alert(uuid);
    window.localStorage.setItem("deviceUUID",uuid)
	var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    if(isMobile) {
    	var dataToSend = JSON.stringify({ 
			 uuid:uuid,
			 userId:window.sessionStorage.getItem("userId")
			});
	    var data = callingAdminServices("PUT","application/json","json","loginService/updateUUID",dataToSend);
	  	alert(JSON.stringify(data))
	    window.sessionStorage.setItem("deviceUUID",data.uuid);
	    webSocket.send(window.sessionStorage.getItem("userId")+"_open"+"/"+data.uuid);
	 
    }
    
}


function registerUser(l, s, n, o, t, r, a, e, i)
{

	var signUpData = JSON.parse(window.sessionStorage.getItem("signUpData"));
	var dataToSend = JSON.stringify({ 
					 shopName:signUpData.shopName,
					 mobileNumber:signUpData.mobileNumber,	
					 plan:window.sessionStorage.getItem("plan"),
					 subscriptionFor:window.sessionStorage.getItem("subscriptionFor"),
					 password:$('#register_password').val(),
					});
	var data = callingAdminServices("POST","application/json","json","loginService/registerShop",dataToSend);
	var finalJson = data;
	
	if(finalJson.success)
	{
		window.sessionStorage.removeItem("plan");
		window.sessionStorage.removeItem("subscriptionFor");
		$(".m-login__signin,.m-login__account").show();$(".password_set,.m-login__signup").hide();
		t.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), r.clearForm(), r.validate().resetForm(), a();
		var l = e.find(".m-login__signin form");
		l.clearForm(), l.validate().resetForm(), i(l, "success", "Your shop is successfully registered.Please login.")
		var h = $(".m-login__signin");h.clearForm();
	}
	else
	{
		window.sessionStorage.removeItem("plan");
		window.sessionStorage.removeItem("subscriptionFor");
		$(".m-login__signin,.m-login__account").show();$(".password_set,.m-login__signup").hide();
		t.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), r.clearForm(), r.validate().resetForm(), a();
		var l = e.find(".m-login__signin form");
		l.clearForm(), l.validate().resetForm(), i(l, "danger", "Shop is already registered with this Mobile Number.Please register with new Mobile Number")
	}
}

function sessionVariablesSet(finalJson)
{	
	window.sessionStorage.setItem("login",true);
	window.sessionStorage.setItem("userId",finalJson.userId);
	window.sessionStorage.setItem("userDetails",JSON.stringify(finalJson));
}

function logOut()
{
	window.sessionStorage.removeItem("login");
	window.sessionStorage.removeItem("userId");
	window.sessionStorage.removeItem("shopName");
	window.sessionStorage.removeItem("userDetails");
	window.sessionStorage.removeItem("adminApiKey");
	window.sessionStorage.removeItem("selectedArray");
	window.sessionStorage.removeItem("itemDetails");
	window.sessionStorage.removeItem("creditCustomers");
	window.sessionStorage.removeItem("creditMobileNumber");
	window.sessionStorage.removeItem("creditCustomerName");
	window.sessionStorage.removeItem("customerTransactions");
	window.sessionStorage.removeItem("purchaseOrSellItems");
	window.sessionStorage.removeItem("previewItems");
	window.sessionStorage.removeItem("inventoryTransactions");
	window.sessionStorage.removeItem("onSaveItem");
	window.sessionStorage.removeItem("updateInvoiceTransactions");
	window.sessionStorage.removeItem("customerTransactionId");
	window.location.href = "./index.html?logout=true";
}

var getUrlParameter = function getUrlParameter(sParam) 
{
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var sucessOTP = false;

function planPurchase(e,i)
{	
	var genOTP;
	var fg =  $("#number").val();
	$('.m_sweetalert_demo_8').html("Send OTP").attr("disabled", !1);
	$('.m_sweetalert_demo_8').unbind().click(function(e) {
		$('.m_sweetalert_demo_8').addClass("m-loader m-loader--right m-loader--light").attr("disabled", !0);
		var a = $(this);
		window.sessionStorage.removeItem("GeneratedOTP")	
		var mobileNumber = $('#number').val(); 
		var dataToSend = JSON.stringify({ 
			mobileNumber:mobileNumber
		});
		var sucess = false;
		if((isNaN(mobileNumber)) || (mobileNumber.length < 10))
		{	
			var l = $("#m_login").find(".m-login__signup");
			l.clearForm(), l.validate(), i(l, "danger", "Please enter correct mobile number");
			a.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1);
		}
		else
		{	
			if(window.sessionStorage.getItem("GeneratedOTP") == null )
			{	
				setTimeout(function(e){
					
				
				var data1 = callingAdminServices("POST","application/json","json","loginService/checkUsers",dataToSend);
				if(!data1.success)
				{
					$(".m-login__signin,.m-login__account").show();$(".password_set,.m-login__signup").hide();
					a.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1);
					var l = $(".m-login__signin");$(".m_sweetalert_demo_8").show();$(".success_ok").removeClass("fa fa-check");
					var j = $("#m_login").find(".m-login__signup");j.find(".alert").remove();
					l.clearForm(), l.validate(), i(l, "danger", "Shop is already registered with this Mobile Number.Please register with new Mobile Number")
				}
				else
				{
					var data = callingAdminServices("POST","application/json","json","loginService/generateOTP",dataToSend);
					window.sessionStorage.setItem("mobileNumberforOTP",JSON.stringify(mobileNumber));
					window.sessionStorage.setItem("GeneratedOTP",JSON.stringify(data.otp));
					
					
					setTimeout(function(e){
						setTimeout(function(){
							$("#otp").text(data.otp);
						},50)
					Swal.fire({
							title: 'Please enter OTP',
							input: 'number',
							inputPlaceholder: "Enter OTP",
							html: '<a  id="color-change" onclick="sendOtp();" class="disabled" >Resend</a> OTP in : <span id="timer"></span>' +
								  '<br><a  id="color-change1" style="color:green" class="disabled1">OTP sent your mobile</a>',
								  /*'<br><a  id="otp" style="color:green;font-size: 10px;" class="disabled1"></a>',*/
							showCancelButton: true,
							inputValidator: (value) => {
								genOTP = JSON.parse(window.sessionStorage.getItem("GeneratedOTP"));
								if(value != genOTP )
								{
									sucessOTP = true;
									return "Enter valid OTP";
								}
								else
									sucess = true;
							},
					 }).then(function(result) {
							a.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1);
							timerOn = false;
							if(sucess)
							{	
								finalNumber = $("#number").val();
								window.sessionStorage.setItem("verfiedOTP",true);
								$(".m_sweetalert_demo_8").hide();$(".success_ok").addClass("fa fa-check");
								$(".m-login__signup").find(".alert").remove();
								//a.html("Verified").attr("disabled", !0);
							}
					 	});
					},150)
					timerOn = true;
					timer(60);
				}
				},120)
			}
		} 
	});
}


let timerOn = false;

function timer(remaining) 
{  
  $("#color-change").click(function(e){
	$("#color-change").addClass("disabled");
  });
  
  var m = Math.floor(remaining / 60);
  var s = remaining % 60;
  
  m = m < 10 ? '0' + m : m;
  s = s < 10 ? '0' + s : s;
  
  try{
	  document.getElementById('timer').innerHTML = m + ':' + s;
	  remaining -= 1;
  }
  catch(e){}
  
  if(remaining >= 0 && timerOn) {
    setTimeout(function() {
        timer(remaining);
    }, 1000);
    return;
  }

  if(!timerOn) {
    // Do validate stuff here
    return;
  }
  
  // Do timeout stuff here
  //alert('Timeout for otp');
  document.getElementById("color-change").style.color = "red";
  $("#color-change").removeClass("disabled");
}


function sendOtp()
{	
	 var mobileNumber = JSON.parse(window.sessionStorage.getItem("mobileNumberforOTP"));
	 var dataToSend = JSON.stringify({ 
			mobileNumber:mobileNumber
	 });
	 timer(60);
	 $("#color-change1").text("OTP Resent successfully");$("#color-change").addClass("disabled");
	 $(".swal2-input").focus();
	 document.getElementById("color-change").style.color = "";
	 var data = callingAdminServices("POST","application/json","json","loginService/generateOTP",dataToSend);
	 window.sessionStorage.setItem("GeneratedOTP",JSON.stringify(data.otp));
}


function shopRegisterPayment(l, s, n, o, t, r, a, e, i,plan,subscriptionCharge)
{		
	var condition = JSON.parse(window.sessionStorage.getItem("verfiedOTP"));
	if(condition)
	{  
		var dataToSend = JSON.stringify({
				mobile:$('#number').val(),
				plan:plan,
				paymentFor:"New",
				amount:subscriptionCharge
			});
		var data = callingAdminServices("POST","application/json","json","bookingService/payNowForSubscription",dataToSend);
		if(data.success)
		{
			var paymentDTO = data.data[0]; 
			window.sessionStorage.setItem("paymentTransaction",JSON.stringify(paymentDTO));
			window.open(paymentDTO.redirectURL,"_self");
		}
		else
		{
			var l = e.find(".m-login__signup");
			l.clearForm(), l.validate(),l.resetForm(), i(l, "danger", "Payment failed.Please try again")
		}
	}
	else
	{
		var l = e.find(".sign_up_form");
		e.find(".alert").remove();
		i(l, "danger", "Please verifiy mobile number");
	}
}


function shopRenewalSubscription(plan,subscriptionCharge){
	
	var dataToSend = JSON.stringify({
		
		mobile:JSON.parse(window.sessionStorage.getItem("mobileNumber")),
		plan:plan,
		paymentFor:"RenewalPayment",
		amount:subscriptionCharge
	});
	var data = callingAdminServices("POST","application/json","json","bookingService/payNowForSubscription",dataToSend);
	
	if(data.success)
	{
		var paymentDTO = data.data[0]; 
		window.sessionStorage.setItem("paymentTransaction",JSON.stringify(paymentDTO));
		window.sessionStorage.setItem("renewalPayment",true);
		window.open(paymentDTO.redirectURL,"_self");
	}
	else
	{
		var l = e.find(".m-login__signup");
		l.clearForm(), l.validate(),l.resetForm(), i(l, "danger", "Payment failed.Please try again")
	}
}

function termsCondition()
{
	$('#loaderDiv').show();
	$(".m-grid__item").hide();
	$("#main_portlet").show();
	$("#forTermsPage").hide();
	
	setTimeout(function(){
		$('#loaderDiv').hide();
	},100);
}

function back()
{
	$("#forTermsPage").show();
	$(".m-grid__item").show();
	$("#main_portlet").hide();
	
}

function forgetPassword(forgetMobileNumber,t,l,i,r)
{
	var sucess = false;
	var dataToSend = JSON.stringify({ 
		mobileNumber:forgetMobileNumber
	});
	var data = callingAdminServices("POST","application/json","json","loginService/checkUsers",dataToSend);
	if(!data.success)
	{	
		var a = $(this);
		window.sessionStorage.removeItem("GeneratedOTP");
		window.sessionStorage.setItem("mobileNumberforOTP",JSON.stringify(forgetMobileNumber));
		var mobileNumber = $('#number').val();
		var data1 = callingAdminServices("POST","application/json","json","loginService/generateOTP",dataToSend);
		window.sessionStorage.setItem("GeneratedOTP",JSON.stringify(data1.otp))
		setTimeout(function(e){
			Swal.fire({
				title: 'Please enter OTP',
				input: 'number',
				inputPlaceholder: "Enter OTP",
				html: '<a  id="color-change" onclick="sendOtp();" class="disabled" >Resend</a> OTP in : <span id="timer"></span>' +
					  '<br><a  id="color-change1" style="color:green" class="disabled1">OTP sent your mobile for reset password</a>',
					  
				showCancelButton: true,
				inputValidator: (value) => {
					genOTP = JSON.parse(window.sessionStorage.getItem("GeneratedOTP"));
					if(value != genOTP )
					{
						sucessOTP = true;
						return "Enter valid OTP";
					}
					else
						sucess = true;
				},
			}).then(function(result) {
				$("#m_login_forget_password_submit").removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1);
				timerOn = false;
				if(sucess)
				{
					$(".m-login__signup,.password_set").show();$(".sign_up_actions,.m-login__forget-password").hide();
					$(".forget_password_actions").show();
					$(".sign_up_form,.m-login__signin,.m-login__account").hide();
					$(".forget_password_title").text("Forgotten Password ?");
					window.sessionStorage.removeItem("GeneratedOTP");
				}
		 	});
			timerOn = true;
			timer(60);
		},200)
	}
	else{
		
		t.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1); /*r.clearForm(), r.validate().resetForm()*/
		var l = $("#m_login").find(".m-login__forget-password");
		i(l, "danger", "User not exists")
	}
}


function forgetPasswordService(number,t)
{
	var dataToSend = JSON.stringify({ 
		mobileNumber: number,
		password:$('#register_password').val()
	});
	var data = callingAdminServices("POST","application/json","json","loginService/forgetShopPassword",dataToSend);
	
	if(data.success){
		$(".m-login__signin,.m-login__account").show();$(".password_set,.m-login__signup").hide();/*$("#m_login").find(".m-login__signin").resetForm();*/
		var l = $("#m_login").find(".m-login__signin");	
		l.clearForm(), l.resetForm(), i(l, "success", "Password reset successfully.Please relogin")
	}
	else
	{
		$(".password_set").show();
		var l = $("#m_login").find(".password_set").show();	
		$("#reset_actions").removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1);
		l.clearForm(), l.resetForm(), i(l, "danger", "Password reset failed.")
	}
}

function changePassword()
{
	var form = $(".validate-form-1"); 
	if (!form.valid()) {
         return;
	}
	var dataToSend = JSON.stringify({ 
		mobileNumber: JSON.parse(window.sessionStorage.getItem("mobileNumber")),
		existingPassword:$("#oldPassword").val(),
		password:$('#register_password').val()
	}); 
	var data = callingAdminServices("POST","application/json","json","loginService/updateShopPassword",dataToSend);
	
	if(data.success)
	{
		var l = $(".validate-form-1");l.clearForm();l.resetForm();
		i(l, "success", "Password reset successfully"); 
	}
	else
	{
		var l = $(".validate-form-1");
		i(l, "danger", data.messege);
		
	}
}


/*function changePassword(Number,t){
	
	var mobileNumber;
	if(Number != null || Number != undefined){
		mobileNumber = Number; 
	}
	else
	{
		var form = $(".validate-form-1"); 
		if (!form.valid()) {
	         return;
	    }
		mobileNumber = JSON.parse(window.sessionStorage.getItem("mobileNumber"));
	}
	var dataToSend = JSON.stringify({ 
		mobileNumber: mobileNumber,
		existingPassword:$("#oldPassword").val(),
		password:$('#register_password').val()
	}); 
	var data = callingAdminServices("POST","application/json","json","loginService/updateShopPassword",dataToSend);
	console.log(JSON.stringify(data));
	if(data.success)
	{
		if(Number != null || Number != undefined){
			$(".m-login__signin,.m-login__account").show();$(".password_set,.m-login__signup").hide();
			var l = $("#m_login").find(".m-login__signin");	
			l.clearForm(), l.resetForm(), i(l, "success", "Password reset successfully.")
		}
		else
		{
			var l = $(".validate-form-1");l.clearForm();l.resetForm();
			i(l, "success", "Password reset successfully"); 
		}
	}
	else
	{	if(Number != null || Number != undefined)
		{
			$(".password_set").show();$("#m_login").find(".m-login__signin").resetForm();
			var l = $("#m_login").find(".password_set").show();	
			$("#reset_actions").removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1);
			l.clearForm(), l.resetForm(), i(l, "danger", "Password reset failed.")
		}
		else
		{
			var l = $(".validate-form-1");
			i(l, "danger", data.messege);
		}
	}
}
*/

/*function renewalPlan(descr,count)
{
	if(count == 1){
	$("#m_aside_left").removeClass("m-aside-left--on");
	$("#m_aside_left_offcanvas_toggle").removeClass("m-brand__toggler--active");
	$('ul li').removeClass("m-menu__item--active");
	$("#renewShop").addClass("m-menu__item--active");
	$(".m-aside-left-overlay").removeClass("m-aside-left-overlay");
		$('#loaderDiv').show();
		$("#content").empty();
		$.get(".pages/index.html", function (data) {
	       $("#content").append(data);
	       $('#loaderDiv').show();
	       $(".m-login__signin,.m-login__signup,.m-login__account").hide();
				$("#prices-list,.close").show();var countValue = [];
				countValue = document.getElementsByClassName("purchase_value");
				$(".button_1").hide();
				if(descr === 'profilePage')
				{
					$("#button_2").show();
					$("#button_3").hide();
				}
				else{
					$("#button_3").show();
					$("#button_2").hide();
				}
				for(var i =0;i<countValue.length;)
				{
					countValue[i].id = i++;
				}
				$(".button_2").unbind().click(function(){ 
					var id = $(this).attr('id');
					if(id == "button_2")
						loadProfile();
					else
						loadHome();
				});$('#loaderDiv').hide();
		}); 
		count++;
	}
}*/












function getApiKey()
{	
	window.sessionStorage.setItem("adminApiKey",callingAdminServices("GET","application/json","text","loginService/apikey",""));
}