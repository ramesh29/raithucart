var webSocket = new WebSocket(webSocketURL);
webSocket.onerror = function(event) {
	onError(event)
};

webSocket.onopen = function(event) {
	onOpen(event)
};

webSocket.onmessage = function(event) {
	onMessage(event)
};

function onMessage(event) {
	
	var data = event.data;
	const[value,value1] = data.split("/");
	var userId = window.sessionStorage.getItem("userId");
	var user = userId+"_open";
	var deviceId = window.localStorage.getItem("deviceUUID");
	var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
	if((value === user) && (value1 === deviceId))
		app.scanBarcode();
	else if(window.sessionStorage.getItem("userId") == value){
		//scanAppendData(event.data)
	    if(isMobile){} else
	    	scanAppendData(value1);
	}
	else if(user == value && (window.sessionStorage.getItem("adminApiKey") != null))
		window.sessionStorage.setItem("returnDbDeviceId",value1)
}

function onOpen(event) {
	console.log("connection established");
}

function onError(event) {
	console.log(event.data);
}

function send(uuid,userId) {
	webSocket.send(userId+"_open"+"/"+uuid);
	return false;
}
