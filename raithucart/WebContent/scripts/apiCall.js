//var serviceURL = "http://35.154.178.223:8080/QuickBookServices/rest/";
//var serviceURL = "http://192.168.0.106:8081/MyMarineCourseServices/";
//var serviceURL = "http://www.mymarinecourse.com/MyMarineCourseServices/";
var serviceURL 	 = "http://192.168.0.3:8080/MyMarineCourseServices/";
//var webSocketURL = "ws://localhost:8080/MyMarineCourseServices/websocket";

var apiKey;

function callingAdminServices(requestType,contentType,datatype,serviceName,data)
{
	var returnData;
	$('#loaderDiv').show();
	$.ajax({
	    type: requestType,
		dataType: datatype,
		url: serviceURL+"rest/"+serviceName,
		async:false,
		processData: false,
		headers:{"Content-Type" :contentType,"Authorization":"Bearer "+JSON.parse(window.sessionStorage.getItem("adminApiKey"))},
		data : data,
		success: function(data, textStatus)
		{	

			if(datatype == 'text')
			{
				returnData = JSON.stringify(data);
			}else
			{
				returnData = data;
			}
			
			//$('#loaderDiv').hide();
			setTimeout(function(){ 
			$('#loaderDiv').hide();
			},1000);
		},
	    error: function(xhr, textStatus, errorThrown)
	    {
	    	if(xhr.status == 401)
    		{
	    		window.sessionStorage.removeItem("login");
	    		window.sessionStorage.removeItem("userId");
	    		window.sessionStorage.removeItem("userDetails");
	    		window.sessionStorage.removeItem("adminApiKey");
	    		window.sessionStorage.removeItem("selectedArray");
	    		window.sessionStorage.removeItem("itemDetails");
	    		window.sessionStorage.removeItem("creditCustomers");
	    		window.sessionStorage.removeItem("purchaseOrSellItems");
	    		window.sessionStorage.removeItem("previewItems");
	    		window.location.href = "./index.html?logout=expired";
    		}
	    	returnData="error";
	    	$('#loaderDiv').hide();
	    }
	});
	
	return returnData;
}