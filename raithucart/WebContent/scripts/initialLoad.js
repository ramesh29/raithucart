$( document ).ready(function() {
	loadHome();
	mLayout.init();
	
});

var barcodeValue;
var mobileVersion = false;
var collectArray = [];
var sortValue = 0;
var manageArray = [];
var inputValueOfPs;
var loadData;
var scanPageID;
var scrollCondtion;
var onScrollValue = false;
var customerTransactionId;
$(window).on('scroll', onScroll); 
function onScroll(){ 
    
	if ((window.innerHeight + window.pageYOffset) >= document.body.scrollHeight - 2){
    	if(scrollCondtion && modelScroll && onScrollValue)
    	{
    		$('#paginationLoader').show();
    		//setTimeout(function(){
    			try{
    				if($(".for_auto_complete")[0].value === null || $(".for_auto_complete")[0].value === undefined || $(".for_auto_complete")[0].value === "")
    				loadData();
    			}catch(e){}
    			setTimeout(function(){$('#paginationLoader').hide();},500)
    		//},300)
    	}
    }
}

function loadHome()
{	
	
	$('ul li').removeClass("m-menu__item--active");
	$("#m_aside_left_offcanvas_toggle").removeClass("m-brand__toggler--active");
	$('#loaderDiv').show();
	$("#home").addClass("m-menu__item--active");
	$("#content").empty();
	var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    if(!isMobile) $(".barcode").hide();
    var finalJson;
	if(sortValue == 1)
		sortValue = 0;
	var userDetails = [];
	if(window.sessionStorage.getItem("userDetails") != null && window.sessionStorage.getItem("userDetails") != undefined && window.sessionStorage.getItem("userDetails") != "undefined")
	{
		userDetails = JSON.parse(window.sessionStorage.getItem("userDetails"));
	}
	else
	{
		userDetails = callingAdminServices("GET","application/json","json","shopService/shopLogin/"+window.sessionStorage.getItem("userId"),null);
    	window.sessionStorage.setItem("userDetails",JSON.stringify(userDetails));
	}
	
	var shopName = userDetails.shopName;
	var currentStockValue = userDetails.currentStockValue;
	var currentSoldValue = userDetails.currentSoldValue;
	var profitForecast = userDetails.profit;
	var sold = userDetails.totalSoldValue;
	var credit = userDetails.totalCreditValue;
	var todaySoldValue = userDetails.todaySoldValue;
	window.sessionStorage.setItem("shopName",userDetails.shopName);
	$('#userName').text(shopName);
	$('#phoneNumber').text(userDetails.mobileNumber);
	 $.get("./pages/menu.html", function (htmldata) {
		 htmldata = htmldata.replace(/:shopName/g,shopName);
		 htmldata = htmldata.replace(/:todaySoldValue/g,parseFloat(todaySoldValue).toFixed(2));
		 htmldata = htmldata.replace(/:totalStockValue/g,parseFloat(currentStockValue).toFixed(2));
		 htmldata = htmldata.replace(/:totalSoldValue/g,parseFloat(sold).toFixed(2));
		 htmldata = htmldata.replace(/:totalCreditValue/g,parseFloat(credit).toFixed(2));
		 htmldata = htmldata.replace(/:profitEstimate/g,parseFloat(profitForecast).toFixed(2));
		 htmldata = htmldata.replace(/:soldValue/g,parseFloat(sold).toFixed(2));
		 $(htmldata).clone().appendTo("#content");
      });
	 setTimeout(function(){
		 $('#loaderDiv').hide();
	 },150) 
}

// To be changed
function loadViewStock(sortArray)
{	
	$("#m_aside_left").removeClass("m-aside-left--on");
	$("#m_aside_left_offcanvas_toggle").removeClass("m-brand__toggler--active");
	$('#loaderDiv').show();
	$("#content").empty();
	$('ul li').removeClass("m-menu__item--active");
	$("#view").addClass("m-menu__item--active");
	$(".m-aside-left-overlay").removeClass("m-aside-left-overlay");
	onScrollValue = false;
	$.get("./pages/viewStock.html", function (data) {
        $("#content").append(data);
        var finalJson = [];
        if(sortValue == 1){
        	finalJson = sortArray;
        	sortValue = 0;
        }
        else
        {
			if(window.sessionStorage.getItem("itemDetails") != null && window.sessionStorage.getItem("itemDetails") != undefined && window.sessionStorage.getItem("itemDetails") != "undefined")
			{
				finalJson = JSON.parse(window.sessionStorage.getItem("itemDetails"));
			}else
			{
				var data = callingAdminServices("GET","application/json","json","shopService/getShopItems/"+window.sessionStorage.getItem("userId"),null);
		    	var finalJson =data.data;
		    	window.sessionStorage.setItem("itemDetails",JSON.stringify(finalJson));
			}
        }
    	var quantityClass = 1;
    	var countValue = 0;
    	var k =0;
    	var autoCompleteData = [];
    	$.each( finalJson, function( key, value ) {
  		  autoCompleteData[k++] = value.itemName;
  		});
    	//autoComplete(document.querySelector(".for_auto_complete"), autoCompleteData);
    	var removedDuplicateArray = [];
    	removedDuplicateArray = removeDuplicates(autoCompleteData);
    	function removeDuplicates(autoCompleteData) {
    		  return autoCompleteData.filter((a, b) => autoCompleteData.indexOf(a) === b)
    	};
    	autoComplete(document.querySelector(".for_auto_complete"), removedDuplicateArray);
    	
    	
    	
    	
    	
    	
    	var startIndex = 0,stopIndex,serachInput,finaArray = [];
    	loadData = function(closeCondtion,value,loader)
    	{
    		serachInput = value;
    		if(closeCondtion == true)

    		{
    			startIndex = 0;
    			countValue= 0;
    		}

    		if(finalJson.length !== 0)
    		{
    			var html;
    			if(serachInput != null && serachInput != undefined && serachInput != "")
    			{
    				$('#list').empty();
    				finalJson = JSON.parse(window.sessionStorage.getItem("itemDetails"));
    				finaArray = finalJson.filter(a=>a.itemName == serachInput);
    			}
    			else if(finalJson.length >= 10)
    			{	
    				var displayCount = 30;
    				stopIndex = startIndex + displayCount;
    				finaArray = finalJson.slice(startIndex,stopIndex);
    				startIndex = stopIndex;

    				countValue += finaArray.length; 
    				console.log(countValue)
    				if(finalJson.length == countValue)
    					scrollCondtion = false;
    				else
    					scrollCondtion = true;
    			}
    			else
    			{
    				finaArray = finalJson; 
    				scrollCondtion = false;
    			}
    			$.get("./holders/onHandItems.html", function (loadHTML) {
    				onScrollValue = false;
    				html = loadHTML;
    				finaArray.forEach(function(value){
    					
    					var htmldata = html;
    					htmldata = htmldata.replace(/:itemName/g,value.itemName);
    					htmldata = htmldata.replace(/:itemDescription/g,value.itemDesc);
    					htmldata = htmldata.replace(/:buyPrice/g,parseFloat(value.itemPurchaseCost).toFixed(2));
    					htmldata = htmldata.replace(/:sellingPrice/g,parseFloat(value.itemSoldCost).toFixed(2));
    					htmldata = htmldata.replace(/:uom/g,value.itemUOM);
    					htmldata = htmldata.replace(/:quantity/g,parseFloat(value.totalQuantity).toFixed(2));
    					htmldata = htmldata.replace(/:imageName/,serviceURL+"rest/shopService/showImage/"+window.sessionStorage.getItem("userId")+"/"+value.imageName);
    					htmldata = htmldata.replace(/:amount/g,parseFloat(value.itemSoldCost * value.totalQuantity).toFixed(2));
    					htmldata = htmldata.replace(/:value/g,quantityClass);
    					$('#list').append(htmldata);
    					var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    				    if(!isMobile)$("#myInput").focus();
    				    /*else
    				    $(".stockValue").addClass("row");*/
    					var styleQuantity = $(".quantity_"+(quantityClass++));
    					if(value.totalQuantity > value.lowQuantityValue)
    					{
    						styleQuantity.removeClass("m-badge m-badge--danger");
    						styleQuantity.removeClass("fa fa-arrow-down");
    					}
    					else
    					{
    						styleQuantity.addClass("m-badge m-badge--danger");
    						styleQuantity.addClass("fa fa-arrow-down");
    					}
    			
    				});
    				onScrollValue = true;
    				setTimeout(function(){
    					$('#loaderDiv').hide();
    				},150)
    			
    			}); 
    		}
    		else
    		{
    			$.get("./pages/noContent.html", function (data) {
    				$("#list").append(data);
    				$("#items_error").text("No items available please add items");
    				document.getElementById("dropdownMenuButton").disabled = true; 
    				$('#loaderDiv').hide();
    			});
    		}
    	}
    	loadData();
    });
	
    var previous = "";
    setTimeout(function(e){
    	
    	$("#list .m-widget5__title").each(function() {   
	        var current = this.innerText[0].toUpperCase();
	        if (current != previous) { 
	            $(this).attr("id", "first_letter_" + current);
	            previous = current;
	        }
    	});
   },1000);

	setTimeout(function(){
		mApp.init();
	},100);
	
}



// To be changed
function loadManageStock(sortArray)
{	
	$("#m_aside_left").removeClass("m-aside-left--on");
	$("#m_aside_left_offcanvas_toggle").removeClass("m-brand__toggler--active");
	$('#loaderDiv').show();
	$("#content").empty();
	$('ul li').removeClass("m-menu__item--active");
	$("#manage").addClass("m-menu__item--active");
	$(".m-aside-left-overlay").removeClass("m-aside-left-overlay");
	onScrollValue = false;
	$.get("./pages/manageStock.html", function (data) {
        $("#content").append(data);
       
        var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    		if(!isMobile) $("#myInput").focus();
    	var finalJson = [];
    	if(sortValue == 1){
         	finalJson = sortArray;
         	sortValue = 0;
        }
        else
        {
	    	if(window.sessionStorage.getItem("itemDetails") != null && window.sessionStorage.getItem("itemDetails") != undefined && window.sessionStorage.getItem("itemDetails") != "undefined")
			{
	    		finalJson = JSON.parse(window.sessionStorage.getItem("itemDetails"));
			}
			else
			{
				var data = callingAdminServices("GET","application/json","json","shopService/getShopItems/"+window.sessionStorage.getItem("userId"),null);
	        	var finalJson =data.data;
	        	window.sessionStorage.setItem("itemDetails",JSON.stringify(finalJson));
			}
        }
    	var quantityClass = 1; 
    	var k =0;
    	var autoCompleteData = [];
    	$.each( finalJson, function( key, value )  {
    		  autoCompleteData[k++] = value.itemName;
    	});
    	autoComplete(document.querySelector(".for_auto_complete"), autoCompleteData)
    	var startIndex = 0,stopIndex,serachInput,finaArray = [];var countValue = 0;
    	loadData = function(closeCondtion,value)
    	{   
    		serachInput = value;
    		if(closeCondtion == true)
    		{
    			startIndex = 0;
    			countValue = 0;
    		}

    		if(finalJson.length !== 0)
    		{  
    			var html;
    			if(serachInput != null && serachInput != undefined && serachInput != "")
    			{
    				$('#list').empty();
    				finalJson = JSON.parse(window.sessionStorage.getItem("itemDetails"));
    				finaArray = finalJson.filter(a=>a.itemName == serachInput);
    			}
    			else if(finalJson.length >= 10)
    			{	
    				var displayCount = 30;
    				stopIndex = startIndex + displayCount;
    				finaArray = finalJson.slice(startIndex,stopIndex);
    				startIndex = stopIndex;

    				countValue += finaArray.length; 
    				console.log(countValue)
    				if(finalJson.length == countValue)
    					scrollCondtion = false;
    				else
    					scrollCondtion = true;
    			}
    			else
    			{
    				finaArray = finalJson; 
    				scrollCondtion = false;
    			}
				
    			$.get("./holders/transactionalItems.html", function (loadHTML) {
    				onScrollValue = false;
    				html = loadHTML;
    				finaArray.forEach(function(value){
    				
    					var htmldata = html;
    					htmldata = htmldata.replace(/:itemName/g,value.itemName);
    					htmldata = htmldata.replace(/:itemDescription/g,value.itemDesc);
    					htmldata = htmldata.replace(/:buyPrice/g,parseFloat(value.itemPurchaseCost).toFixed(2));
    					htmldata = htmldata.replace(/:sellingPrice/g,parseFloat(value.itemSoldCost).toFixed(2));
    					htmldata = htmldata.replace(/:uom/g,value.itemUOM);
    					htmldata = htmldata.replace(/:quantity/g,parseFloat(value.totalQuantity).toFixed(2));
    					htmldata = htmldata.replace(/:amount/g,parseFloat(value.totalAmount).toFixed(2));
    					htmldata = htmldata.replace(/:itemId/g,value.itemId);
    					htmldata = htmldata.replace(/:imageName/g,serviceURL+"rest/shopService/showImage/"+window.sessionStorage.getItem("userId")+"/"+value.imageName);
    					htmldata = htmldata.replace(/:value/g,quantityClass);
    					$('#list').append(htmldata);$('#paginationLoader').hide();
    					
    					var styleQuantity = $(".quantity_"+(quantityClass++));
    					if(value.totalQuantity > value.lowQuantityValue)
    					{
    						styleQuantity.removeClass("m-badge m-badge--danger");
    						styleQuantity.removeClass("fa fa-arrow-down");
    					}
    					else
    					{
    						styleQuantity.addClass("m-badge m-badge--danger");
    						styleQuantity.addClass("fa fa-arrow-down");
    					}
    					onScrollValue = true;		
    					
    				});
    				
    				setTimeout(function(){
    					mApp.init();
    					highlightCells();
    					$('#loaderDiv').hide();
    				},100);
    			});
    		}
    		else
    		{
    			$.get("./pages/noContent.html", function (data) {
    				$("#list").append(data);
    				$("#items_error").text("No items available please add items");
    				document.getElementById("dropdownMenuButton").disabled = true;$('#loaderDiv').hide();
    			});
    		}
    		
    		/*setTimeout(function(){
    			$('#loaderDiv').hide();
    		},150)*/
    		
    	} 
    	loadData();
    });
	
}	
		
function loadSettings(sortArray,addedItem,sortAppendValue)
{	
	$("#m_aside_left").removeClass("m-aside-left--on");
	$("#m_aside_left_offcanvas_toggle").removeClass("m-brand__toggler--active");
	$('#loaderDiv').show();
	$("#content").empty();
	$('ul li').removeClass("m-menu__item--active");
	$("#setup").addClass("m-menu__item--active");
	$(".m-aside-left-overlay").removeClass("m-aside-left-overlay");
	onScrollValue = false;
	$.get("./pages/settings.html", function (data) {
		data = data.replace(/:userId/g,window.sessionStorage.getItem("userId"));
        $("#content").append(data);
        if(sortAppendValue != null && sortAppendValue != undefined )
        	$("#sort_append_value").val(sortAppendValue)
        var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        if(!isMobile) $("#myInput").focus();
       /* var userDetails;
        if(window.sessionStorage.getItem("userDetails") != null && window.sessionStorage.getItem("userDetails") != undefined && window.sessionStorage.getItem("userDetails") != "undefined")
    	{
    		userDetails = JSON.parse(window.sessionStorage.getItem("userDetails"));
    	}
    	else
    	{
    		userDetails = callingAdminServices("GET","application/json","json","shopService/shopLogin/"+window.sessionStorage.getItem("userId"),null);
        	window.sessionStorage.setItem("userDetails",JSON.stringify(userDetails));
    	}
        
        if(userDetails.gst == "NO")
        	$("#gstTag").hide();*/
        var finalJson = [];
       // scanPageId = "settings";
        if(addedItem !== null && addedItem !== undefined && addedItem !== "" )
        {
        	var l = $("#add_item_success")
        	$("#myInput").val(addedItem);$(".close_action").addClass("la-close");
        	alertMessage("Item added successfully","primary");
        	$("#list").addClass("list_margin_1");
        	document.getElementById("add_item_success").style.textAlign = "center";
        	$(".close").unbind().click(function(e){
        		$("#list").removeClass("list_margin_1");
        	})
        }
    	if(sortValue == 1){
    		finalJson = sortArray;
    	}else
		{
    		if(window.sessionStorage.getItem("itemDetails") != null && window.sessionStorage.getItem("itemDetails") != undefined && window.sessionStorage.getItem("itemDetails") != "undefined")
	    		var finalJson = JSON.parse(window.sessionStorage.getItem("itemDetails"));
			else
			{
				var data = callingAdminServices("GET","application/json","json","shopService/getShopItems/"+window.sessionStorage.getItem("userId"),null);
	        	var finalJson =data.data;
	        	window.sessionStorage.setItem("itemDetails",JSON.stringify(finalJson));
			}
		}
    	
    	var k = 0;var countValue = 0;
    	var autoCompleteData = [];
    	$.each( finalJson, function( key, value ) {
  		  autoCompleteData[k++] = value.itemName;
  		  
    	});
    	//autoComplete(document.querySelector(".for_auto_complete"), autoCompleteData);
    	var removedDuplicateArray = [];
    	removedDuplicateArray = removeDuplicates(autoCompleteData);
    	function removeDuplicates(autoCompleteData) {
    		  return autoCompleteData.filter((a, b) => autoCompleteData.indexOf(a) === b)
    	};
    	autoComplete(document.querySelector(".for_auto_complete"), removedDuplicateArray);
    	var startIndex = 0,stopIndex,serachInput,finaArray = [];
    	loadData = function(closeCondtion,value)
    	{ 
    		serachInput = value;
    		$("#list").removeClass("list_margin_1");
    		if(closeCondtion == true)
    		{
    			startIndex = 0;
    			countValue= 0;
    		}
    		if(finalJson.length !== 0)
        	{	
        		var html;
        		
        		if(serachInput != null && serachInput != undefined && serachInput != ""){
    				$('#list').empty();
    				finalJson = JSON.parse(window.sessionStorage.getItem("itemDetails"));
    				finaArray = finalJson.filter(a=>a.itemName == serachInput);
    			}
    			else if(finalJson.length >= 10)
    			{	
    				var displayCount = 30;
    				stopIndex = startIndex + displayCount;
    				finaArray = finalJson.slice(startIndex,stopIndex);
    				startIndex = stopIndex;

    				countValue += finaArray.length; 
    				console.log(countValue)
    				if(finalJson.length == countValue)
    					scrollCondtion = false;
    				else
    					scrollCondtion = true;
    			}
    			else
    			{
    				finaArray = finalJson; 
    				scrollCondtion = false;
    			}
        		$.get("./holders/setupItems.html", function (loadHTML) {
        			onScrollValue = false;
        			html = loadHTML;
        			finaArray.forEach(function(value){
        				
            			var htmldata = html;
            			htmldata = htmldata.replace(/:itemName/g,value.itemName);
    					htmldata = htmldata.replace(/:itemDescription/g,value.itemDesc);
    					htmldata = htmldata.replace(/:buyPrice/g,parseFloat(value.itemPurchaseCost).toFixed(2));
    					htmldata = htmldata.replace(/:sellingPrice/g,parseFloat(value.itemSoldCost).toFixed(2));
    					htmldata = htmldata.replace(/:uom/g,value.itemUOM);
    					htmldata = htmldata.replace(/:quantity/g,parseFloat(value.totalQuantity).toFixed(2));
    					htmldata = htmldata.replace(/:amount/g,parseFloat(value.totalAmount).toFixed(2));
    					htmldata = htmldata.replace(/:itemId/g,value.itemId);
    					htmldata = htmldata.replace(/:imageName/g,serviceURL+"rest/shopService/showImage/"+window.sessionStorage.getItem("userId")+"/"+value.imageName);
        	      		$('#list').append(htmldata);
            		});
        			onScrollValue = true;
        			setTimeout(function(){
        				$('#loaderDiv').hide();
        			},150)
        			modelScroll = true;
                });
        		
        	}
        	else
        	{
        		$.get("./pages/noContent.html", function (data) {
        			$("#list").append(data);
        			$("#items_error").text("No items available please add items");
        			document.getElementById("dropdownMenuButton").disabled = true; $('#loaderDiv').hide();
        		});
        	}
    		try{

    			$(".clear_model_form").click(function(){
    				$(".add_item").clearForm().resetForm();
    			});
    		}catch(e){}
    		$(".scaner").unbind().click(function(){
    			var userId = this.id;
				scan = 1;
				var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
			    if(!isMobile)
			    	send(window.sessionStorage.getItem("returnDbDeviceId"),userId);
			    else
			    {
			    	mobileVersion = true;
			    	app.scanBarcode();
			    }
			});
	}
    if((addedItem != null) || (addedItem != undefined) )
    	loadData("",addedItem);
    else
    	loadData();
	}); 
	setTimeout(function(){
		mApp.init();
	},100);
}


function alertMessage(e,color){

	$.notify({
		/*options
		icon: 'glyphicon glyphicon-warning-sign',
		title: 'Bootstrap notify',
		url: 'https://github.com/mouse0270/bootstrap-notify',
		target: '_blank'*/
		message: e,		
	},{
		// settings
		element: 'body',
		position: null,
		//type: "primary",
		type: color,
		allow_dismiss: true,
		newest_on_top: false,
		showProgressbar: false,
		placement: {
			from: "top",
			align: "right"
		},
		offset: 20,
		spacing: 10,
		z_index: 1031,
		delay: 2000,
		timer: 1000,
		// url_target: '_blank',
		mouse_over: null,
		animate: {
			enter: 'animated fadeInDown',
			exit: 'animated fadeOutUp'
		},
		onShow: null,
		onShown: null,
		onClose: null,
		onClosed: null,
		icon_type: 'class',
		template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
			'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
			'<span data-notify="icon"></span> ' +
			'<span data-notify="title">{1}</span> ' +
			'<span data-notify="message">{2}</span>' +
			'<div class="progress" data-notify="progressbar">' +
				'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
			'</div>' +
			'<a href="{3}" target="{4}" data-notify="url"></a>' +
		'</div>' 
	});
}

var arrows;
if (mUtil.isRTL()) {
    arrows = {
        leftArrow: '<i class="la la-angle-right"></i>',
        rightArrow: '<i class="la la-angle-left"></i>'
    }
} else {
    arrows = {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    }
}

function loadTransactions(sortArray)
{
	$("#m_aside_left").removeClass("m-aside-left--on");
	$("#m_aside_left_offcanvas_toggle").removeClass("m-brand__toggler--active");
	$('#loaderDiv').show();
	$("#content").empty();
	$('ul li').removeClass("m-menu__item--active");
	$("#transactions").addClass("m-menu__item--active");
	$(".m-aside-left-overlay").removeClass("m-aside-left-overlay");
	onScrollValue = false;
	$.get("./pages/inventoryTransactions.html", function (data) {
        $("#content").append(data);
        var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    	if(!isMobile) $("#inventory").focus();
        $(".select2-search__field").text("select items");
        	var date = new Date();
        	var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        	var dd = firstDay.getDate(); 
            var mm = firstDay.getMonth() + 1; 
            var yyyy = firstDay.getFullYear(); 
            if (dd < 10) { 
                dd = '0' + dd; 
            } 
            if (mm < 10) { 
                mm = '0' + mm; 
            } 
            var firstDay = mm + '/' + dd + '/' + yyyy; 
            $('#start').val(firstDay);
        	
        	var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        	var dd = lastDay.getDate(); 
            var mm = lastDay.getMonth() + 1; 

            var yyyy = lastDay.getFullYear(); 
            if (dd < 10) { 
                dd = '0' + dd; 
            } 
            if (mm < 10) { 
                mm = '0' + mm; 
            } 
            var lastDay = mm + '/' + dd + '/' + yyyy; 
        	
        	$('#end').val(lastDay);
        	loadFilters();
        	var startdate=firstDay;
        	var enddate=lastDay;
        	var transactionType=[];
        	var itemIds=[];
        	
        	
        	var dataToSend = JSON.stringify({ 
        		userId : window.sessionStorage.getItem("userId"),
        		startDate:startdate,
        		endDate:enddate,
        		transactionType:transactionType,
        		itemIds:itemIds
            });
        	
        	var data = callingAdminServices("POST","application/json","json","shopService/getInventoryTransactions",dataToSend);
        	var finalJson =data.data;
        	window.sessionStorage.setItem("inventoryTransactions",JSON.stringify(finalJson));
        	
        	var i = 0;var countValue = 0;
        	$('#loaderDiv').show();
        	var autoCompleteData = [];var k = 0;
        	$.each( finalJson, function( key, value ) {
        		  autoCompleteData[k++] = value.itemName;
        		  autoCompleteData[k++] = value.customerName;
        		});
        	
        	var removedDuplicateArray = [];
        	removedDuplicateArray = removeDuplicates(autoCompleteData);
        	function removeDuplicates(autoCompleteData) {
        		  return autoCompleteData.filter((a, b) => autoCompleteData.indexOf(a) === b)
        	};
        	removedDuplicateArray.splice(removedDuplicateArray.findIndex(v => v === "Stock Adjustment"), 1);
        	autoComplete(document.querySelector(".for_auto_complete"), removedDuplicateArray);
        	var startIndex = 0,stopIndex,serachInput,finaArray = [];
        	loadData = function(closeCondtion,value)
        	{   
        		serachInput = value;
        		if(closeCondtion == true)
        		{
        			startIndex = 0;countValue= 0;
        		}
        		if(finalJson.length !== 0)
        		{
        			if(serachInput != null && serachInput != undefined && serachInput != "")
        			{
        				$('#list').empty();
        				finalJson = JSON.parse(window.sessionStorage.getItem("inventoryTransactions"));
        				finaArray = finalJson.filter(a=>a.itemName === serachInput);
        				if(finaArray.length == 0)
        					finaArray = finalJson.filter(a=>a.customerName === serachInput);
        			}
        			else if(finalJson.length >= 10)
        			{	
        				var displayCount = 30;
    					stopIndex = startIndex + displayCount;
    					finaArray = finalJson.slice(startIndex,stopIndex);
    					startIndex = stopIndex;
    					countValue += finaArray.length; 
    					console.log(countValue);
    					if(finalJson.length == countValue)
    						scrollCondtion = false;
    					else
    						scrollCondtion = true;
        			}
        			else
        			{
        				finaArray = finalJson; 
        				scrollCondtion = false;
        			}
        		
        		$.get("./holders/inventoryTransactions_costupdate.html", function (costupdateHTML) {
        			onScrollValue = false;
        			var costupdate = costupdateHTML;
        			
        			$.get("./holders/inventoryTransactions.html", function (inventoryTransactionsHTML) {
        				var inventoryTransactions = inventoryTransactionsHTML;
            			
        				finaArray.forEach(function(value){
        					
        					if("Cost Update" == value.transactionType)
        	     			{	
        						var htmldata = costupdate;
        						htmldata = htmldata.replace(/:itemId/g,value.itemId);
    	               		 	htmldata = htmldata.replace(/:itemName/g,value.itemName);
    	               		 	htmldata = htmldata.replace(/:dateTime/g,value.transactionDate);
    	               		 	htmldata = htmldata.replace(/:transactionType/g,value.transactionType);
    	               		 	htmldata = htmldata.replace(/:itemPurchaseCost/g,parseFloat(value.itemPurchaseCost).toFixed(2));
    	               		 	htmldata = htmldata.replace(/:itemSoldCost/g,parseFloat(value.itemSoldCost).toFixed(2));
    	               		 	htmldata = htmldata.replace(/:newPurchaseCost/g,parseFloat(value.newPurchaseCost).toFixed(2));
    	               		 	htmldata = htmldata.replace(/:newSoldCost/g,parseFloat(value.newSoldCost).toFixed(2));
    	               		 	htmldata = htmldata.replace(/:transactionQty/g,parseFloat(value.transactionQty).toFixed(2));
    	               			$('#list').append(htmldata);
        	        			
        	     			}else
    	     				{
        	     				var htmldata = inventoryTransactions;
        	     				htmldata = htmldata.replace(/:itemId/g,value.itemId);
            	        		htmldata = htmldata.replace(/:itemName/g,value.itemName);
            	        		htmldata = htmldata.replace(/:dateTime/g,value.transactionDate);
            	        		htmldata = htmldata.replace(/:transactionType/g,value.transactionType);
            	        		htmldata = htmldata.replace(/:customerName/g,value.customerName);
            	        		htmldata = htmldata.replace(/:itemPurchaseCost/g,parseFloat(value.itemPurchaseCost).toFixed(2));
            	        		htmldata = htmldata.replace(/:itemSoldCost/g,parseFloat(value.itemSoldCost).toFixed(2));
            	        		htmldata = htmldata.replace(/:newPurchaseCost/g,parseFloat(value.newPurchaseCost).toFixed(2));
            	        		htmldata = htmldata.replace(/:newSoldCost/g,parseFloat(value.newSoldCost).toFixed(2));
            	        		htmldata = htmldata.replace(/:transactionQty/g,parseFloat(value.transactionQty).toFixed(2));
            	        		htmldata = htmldata.replace(/:uom/g,value.uom);
            	        		var txnAmount = 0;
            	        		if("Buy" == value.transactionType)
        	        			{
            	        			htmldata = htmldata.replace(/:costOrPrice/g,"Price");
            	        			htmldata = htmldata.replace(/:txnVal/g,parseFloat(value.itemPurchaseCost).toFixed(2));
            	        			txnAmount = value.transactionQty*value.itemPurchaseCost;
        	        			}
            	        		else
	               				{
        	        				htmldata = htmldata.replace(/:costOrPrice/g,"Cost");
        	        				htmldata = htmldata.replace(/:txnVal/g,parseFloat(value.itemSoldCost).toFixed(2));
        	        				txnAmount = value.transactionQty*value.itemSoldCost;
	               				 }
            	        		 htmldata = htmldata.replace(/:txnAmount/g,parseFloat(txnAmount).toFixed(2));
            	        		 $('#list').append(htmldata);
    	     				}
                		});
        				onScrollValue = true;	
        			});
                });
        		
        		setTimeout(function(){
        			$('#loaderDiv').hide();
        		},150)
        	}
        	else
        	{
        		$.get("./pages/noContent.html", function (data) {
        			$("#list").removeClass("m-timeline-1 m-timeline-1--fixed");
        			$("#list").append(data);
        			$("#icon_animation").addClass("pulsing");
        			$("#items_error").text("No Transactions");
        			document.getElementById("dropdownMenuButton").disabled = true;
        		});$('#loaderDiv').hide(); 
        	}
        }
        loadData();
        setTimeout(function(){
        	mApp.init();
        	$('#m_datepicker_5').datepicker({
        		rtl: mUtil.isRTL(),
        		todayHighlight: true,
        		templates: arrows
        	});
        	$('.selectpicker').selectpicker();
        },100);
    });
}

function loadProfile(success)
{	
	if(success == "empty")
		$("#content").empty();
	else
	{
	$("#m_aside_left").removeClass("m-aside-left--on");
	$("#m_aside_left_offcanvas_toggle").removeClass("m-brand__toggler--active");
	$('#loaderDiv').show();
	$("#content").empty();
	$('ul li').removeClass("m-menu__item--active");
	$("#profile").addClass("m-menu__item--active");
	$(".m-aside-left-overlay").removeClass("m-aside-left-overlay");
	var userDetails = JSON.parse(window.sessionStorage.getItem("userDetails"));
	$.get("./pages/profile.html", function (data) {
        $("#content").append(data);
        setTimeout(function(){
	        $("#shopname").val(userDetails.shopName);
	        $("#mobileNumber").val(JSON.parse(window.sessionStorage.getItem("mobileNumber")));
	        $("#address").val(userDetails.address);
	        $("#city").val(userDetails.city);
	        $("#state").val(userDetails.state);
	        $("#pincode").val(userDetails.zip);
	        if(userDetails.discount === "YES")
	        {		
	        	$("input[name='checkBox_0']").prop( "checked", true );
	        	$("input[name='checkBox_0']").val(userDetails.discount);
	        }
	        if(userDetails.gst === "YES")
	        {
	        	$("input[name='checkBoxGst']").prop( "checked", true );
	        	$("input[name='checkBoxGst']").val(userDetails.gst);
	        }
	        loadInvoiceSettings();
	        $('.input100').each(function(){ 
				
				if($(this).val().trim() != "") 
				{
					$(this).addClass('has-val');
				}
				else 
				{
					$(this).removeClass('has-val');
				}
			}); 
			
			$(".input100").on("blur",function(){
				if($(this).val().trim() !=""){
					$(this).addClass('has-val');
				}
				else
				{
					$(this).removeClass('has-val');
				}
			});
			$('#loaderDiv').hide();
			$("input[placeholder='settings']").change(function() 
			{
				if ($('#checkBox').is(":checked"))
					$("input[type='checkbox']").val("YES");
				else
					$("input[type='checkbox']").val("NO");
			});	
			$("input[placeholder = 'settingsGst']").change(function()
			{
				if ($('#GST').is(":checked"))
					$("input[name='checkBoxGst']").val("YES");
				else
					$("input[name='checkBoxGst']").val("NO");
			});
				
		},200);
        
        var mobileNumber = JSON.parse(window.sessionStorage.getItem("mobileNumber"));
        if(mobileNumber == "9999999999"){
        	document.getElementById("oldPassword").disabled = true;
        	$( "#shopname" ).prop( "disabled", true );
        	$( "#mobileNumber" ).prop( "disabled", true );
        }
        if(success){
    		var l = $(".validate-form");
    		i(l, "success", "Profile updated successfully"); 
    	}
        else if((success === undefined) ||(success === null )){}
    	else
    	{
    		var l = $(".validate-form");
			i(l, "danger", "Profile update failed.Please try again");
    	}
	});
	}
}


function renewalPlan(descr)
{
	$("#m_aside_left").removeClass("m-aside-left--on");
	$("#m_aside_left_offcanvas_toggle").removeClass("m-brand__toggler--active");
	$('ul li').removeClass("m-menu__item--active");
	$("#renewShop").addClass("m-menu__item--active");
	$(".m-aside-left-overlay").removeClass("m-aside-left-overlay");
		$('#loaderDiv').show();
		$("#content").empty();
		$.get("./pages/shopPlans.html", function (data) {
	       $("#content").append(data);
	       $('#loaderDiv').show();
	       $(".m-login__signin,.m-login__signup,.m-login__account").hide();
				$("#prices-list,.close").show();var countValue = [];
				countValue = document.getElementsByClassName("purchase_value");
				$(".button_1").hide();
				if(descr === 'profilePage')
				{
					$("#button_2").show();
					$("#button_3").hide();
				}
				else{
					$("#button_3").show();
					$("#button_2").hide();
				}
				for(var i =0;i<countValue.length;)
				{
					countValue[i].id = i++;
				}
				$(".button_2").unbind().click(function(){ 
					var id = $(this).attr('id');
					if(id == "button_2")
						loadProfile();
					else
						loadHome();
				});$('#loaderDiv').hide();
				
				$(".purchase_value").unbind().click(function(e){
					backRetrive = true;
					plan = $(this).attr("name");
					amount = $(this).attr("value"); 
					var planCount = $(this).attr("id"); 
					if(planCount != null || planCount != undefined) 
						shopRenewalSubscription(plan,amount);
				});
		}); 
}
function saveProfile(e)
{	
	 var form = $(".validate-form");
	 if (!form.valid()) {
         return;
     }
	var existedUser = JSON.parse(window.sessionStorage.getItem("userDetails"));
	var formData = $('.validate-form')[0];
	var dataToSend = JSON.stringify({ 
			shopName: 		$('#shopname').val(),
			address: 		$('#address').val(),
			city: 			$('#city').val(),
			state:		 	$('#state').val(),
			zip: 			$('#pincode').val(),
			userId:			window.sessionStorage.getItem("userId"),
			discount:		$("#checkBox").val(),
			shopLogo: 		existedUser.shopLogo,
			showDetails: 	existedUser.showDetails,
			imageName : 	existedUser.imageName,
			gst:			$("#GST").val()
		});
	if($("#shopLogo").val() == null || $("#shopLogo").val() === undefined ||$("#shopLogo").val() == "" )
	{
		var data = callingAdminServices("POST","application/json","json","loginService/updateShopInvoice",dataToSend);
		window.sessionStorage.removeItem("userDetails");
		var userDetails = callingAdminServices("GET","application/json","json","shopService/shopLogin/"+window.sessionStorage.getItem("userId"),null);
		
		if(userDetails.messege == "Invalid Mobile Number/password.Please try again"){
			
			window.sessionStorage.setItem("userDetails",JSON.stringify(data));
			loadProfile(data.success);
			
		}
		else{
			
			window.sessionStorage.setItem("userDetails",JSON.stringify(userDetails));
    		loadProfile(userDetails.success);
			
		}
    	/*window.sessionStorage.setItem("userDetails",JSON.stringify(userDetails));
    	loadProfile(userDetails.success);*/
		/*var userDetails = data;
		userDetails.currentSoldValue = existedUser.currentSoldValue;
		window.sessionStorage.setItem("userDetails",JSON.stringify(userDetails));*/
		
	}
	else
	{
		var data = new FormData(formData);
		data.append("userId",window.sessionStorage.getItem("userId"));
		data.append("shopItemDTO",dataToSend);
		$.ajax({
			type: "POST",
			enctype: 'multipart/form-data',
			url:  serviceURL+"rest/loginService/updateShop", 
			data: data,
			headers:{"Authorization":"Bearer "+JSON.parse(window.sessionStorage.getItem("adminApiKey"))},
			processData: false,
			contentType: false,
			cache: false,
			timeout: 600000,
			success: function (data) {
        		
				var existedUser = JSON.parse(window.sessionStorage.getItem("userDetails"));
        		window.sessionStorage.removeItem("userDetails");
        		var userDetails = callingAdminServices("GET","application/json","json","shopService/shopLogin/"+window.sessionStorage.getItem("userId"),null);
            	
				if(userDetails.messege == "Invalid Mobile Number/password.Please try again"){
					window.sessionStorage.setItem("userDetails",JSON.stringify(data));
					loadProfile(data.success);
				}
				else{
					
					window.sessionStorage.setItem("userDetails",JSON.stringify(userDetails));
            		loadProfile(userDetails.success);
				}
        		/*var userDetails = data;
        		userDetails.currentSoldValue = existedUser.currentSoldValue;
        		window.sessionStorage.setItem("userDetails",JSON.stringify(userDetails));*/
        		
			},
			error: function (e) {
				alert('Unable to update item.Please contact administrator.');
			}
		});
	}
    loadProfile("empty");
}

function loadFilters()
{
	var items = JSON.parse(window.sessionStorage.getItem("itemDetails"));
	if(items != null && items.length > 0)
	{}else
	{
		var data = callingAdminServices("GET","application/json","json","shopService/getShopItems/"+window.sessionStorage.getItem("userId"),null);
    	var finalJson =data.data;
    	items = finalJson;
    	window.sessionStorage.setItem("itemDetails",JSON.stringify(finalJson));
	}
	var data = [];
    function format(item) { return item; }
	$.each(items, function(key, value) {
	     $('#itemsList')
	          .append($('<option>', { value : value.itemId })
	          .text(value.itemName)
	          );
         data.push(value);
	     
	});
	
	$('#itemsList').select2({width:'100%',dropdownAutoWidth:true,placeholder: "Select items",
			closeOnSelect : false,
			allowHtml: true,
	});
}



function applyTransactionFilters()
{
	$("#list").empty();
	var startdate= $('#start').val();
	var enddate=$('#end').val();
	var transactionType=[];
	$.each($("input[name='transType']:checked"), function(){            
		transactionType.push($(this).val());
    });
	var itemIds=$('#itemsList').val();
	var dataToSend = JSON.stringify({ 
		userId : window.sessionStorage.getItem("userId"),
		startDate:startdate,
		endDate:enddate,
		transactionType:transactionType,
		itemIds:itemIds
    });
	
	var data = callingAdminServices("POST","application/json","json","shopService/getInventoryTransactions",dataToSend);
	var finalJson =data.data;
	var autoCompleteData = [];var k = 0;
	$.each( finalJson, function( key, value ) {
		  autoCompleteData[k++] = value.itemName;
		  autoCompleteData[k++] = value.customerName;
		});
	
	var removedDuplicateArray = [];
	removedDuplicateArray = removeDuplicates(autoCompleteData);
	function removeDuplicates(autoCompleteData) {
		  return autoCompleteData.filter((a, b) => autoCompleteData.indexOf(a) === b)
	};
	removedDuplicateArray.splice(removedDuplicateArray.findIndex(v => v === "Stock Adjustment"), 1);
	autoComplete(document.querySelector(".for_auto_complete"), removedDuplicateArray);
	var startIndex = 0,stopIndex,serachInput,finaArray = [];var countValue = 0;
	onScrollValue = false;
	loadData = function(closeCondtion,value)
	{   
		serachInput = value;
		if(closeCondtion == true)
		{
			startIndex = 0;countValue= 0;
		}
		if(finalJson.length !== 0)
		{
			if(serachInput != null && serachInput != undefined && serachInput != "")
			{
				$('#list').empty();
				finalJson = JSON.parse(window.sessionStorage.getItem("inventoryTransactions"));
				finaArray = finalJson.filter(a=>a.itemName == serachInput);
				scrollCondtion = false;
			}
			else if(finalJson.length >= 10)
			{	
				var displayCount = 30;
				stopIndex = startIndex + displayCount;
				finaArray = finalJson.slice(startIndex,stopIndex);
				startIndex = stopIndex;
				countValue += finaArray.length; 
				console.log(countValue)
				if(finalJson.length == countValue)
					scrollCondtion = false;
				else
					scrollCondtion = true;
			}
			else
			{
				finaArray = finalJson; 
				scrollCondtion = false;
			}
		
			$.get("./holders/inventoryTransactions_costupdate.html", function (costupdateHTML) {
				onScrollValue = false;
				var costupdate = costupdateHTML;
				$.get("./holders/inventoryTransactions.html", function (inventoryTransactionsHTML) {
					var inventoryTransactions = inventoryTransactionsHTML;
					$("#list").addClass("m-timeline-1 m-timeline-1--fixed");
					document.getElementById("dropdownMenuButton").disabled = false; 
					finaArray.forEach(function(value){
						$('#loaderDiv').show();
						if("Cost Update" == value.transactionType)
		     			{	
							var htmldata = costupdate;
							htmldata = htmldata.replace(/:itemId/g,value.itemId);
		           		 	htmldata = htmldata.replace(/:itemName/g,value.itemName);
		           		 	htmldata = htmldata.replace(/:dateTime/g,value.transactionDate);
		           		 	htmldata = htmldata.replace(/:transactionType/g,value.transactionType);
		           		 	htmldata = htmldata.replace(/:itemPurchaseCost/g,parseFloat(value.itemPurchaseCost).toFixed(2));
		           		 	htmldata = htmldata.replace(/:itemSoldCost/g,parseFloat(value.itemSoldCost).toFixed(2));
		           		 	htmldata = htmldata.replace(/:newPurchaseCost/g,parseFloat(value.newPurchaseCost).toFixed(2));
		           		 	htmldata = htmldata.replace(/:newSoldCost/g,parseFloat(value.newSoldCost).toFixed(2));
		           		 	htmldata = htmldata.replace(/:transactionQty/g,value.transactionQty);
		           			$('#list').append(htmldata);
		        			
		     			}else
		 				{
		     				var htmldata = inventoryTransactions;
		     				htmldata = htmldata.replace(/:itemId/g,value.itemId);
			        		 htmldata = htmldata.replace(/:itemName/g,value.itemName);
			        		 htmldata = htmldata.replace(/:dateTime/g,value.transactionDate);
			        		 htmldata = htmldata.replace(/:transactionType/g,value.transactionType);
			        		 htmldata = htmldata.replace(/:customerName/g,value.customerName);
			        		 htmldata = htmldata.replace(/:itemPurchaseCost/g,parseFloat(value.itemPurchaseCost).toFixed(2));
			        		 htmldata = htmldata.replace(/:itemSoldCost/g,parseFloat(value.itemSoldCost).toFixed(2));
			        		 htmldata = htmldata.replace(/:newPurchaseCost/g,parseFloat(value.newPurchaseCost).toFixed(2));
			        		 htmldata = htmldata.replace(/:newSoldCost/g,parseFloat(value.newSoldCost).toFixed(2));
			        		 htmldata = htmldata.replace(/:transactionQty/g,value.transactionQty);
			        		 htmldata = htmldata.replace(/:uom/g,value.uom);
			        		 var txnAmount = 0;
			        		 if("Buy" == value.transactionType)
		        			 {
			        			htmldata = htmldata.replace(/:costOrPrice/g,"Price");
			        			 htmldata = htmldata.replace(/:txnVal/g,parseFloat(value.itemPurchaseCost).toFixed(2));
			        			txnAmount = value.transactionQty*value.itemPurchaseCost;
		        			 }else
		       				 {
		        				htmldata = htmldata.replace(/:costOrPrice/g,"Cost");
		        				htmldata = htmldata.replace(/:txnVal/g,parseFloat(value.itemSoldCost).toFixed(2));
		        				txnAmount = value.transactionQty*value.itemSoldCost;
		       				 }
			        		 htmldata = htmldata.replace(/:txnAmount/g,parseFloat(txnAmount).toFixed(2));
			        		 $('#list').append(htmldata);
		 				}
						onScrollValue = true;
		    		});
					
					$('#loaderDiv').hide();
				});
		    });
			
			if($("input[name='excelSheet']").is(":checked") && finalJson != null)
				saveExcel(finalJson)
			
				
			
			
			
			
			
			
	}
	else
	{
		 swal({
	            title: 'No Transactions',
	            //text: "You won't be able to save without entering values",
	            type: 'error',
	        });
		setTimeout(function(){
			
		
		 $.get("./pages/noContent.html", function (data) {
			$("#list").removeClass("m-timeline-1 m-timeline-1--fixed");
			$("#list").append(data);
			$("#icon_animation").addClass("pulsing");
			$("#items_error").text("No Transactions");
			document.getElementById("dropdownMenuButton").disabled = true; 
		});},50)
	}
	}
	loadData();
	setTimeout(function(){
		mApp.init();
		$('#m_datepicker_5').datepicker({
			rtl: mUtil.isRTL(),
			todayHighlight: true,
			templates: arrows
		});
		$('.selectpicker').selectpicker();
		$("#m_modal_5 .close").click();
	},100);
}

function editItem(e)
{
	var finalJson =JSON.parse(window.sessionStorage.getItem("itemDetails",JSON.stringify(finalJson)));
	var arr = finalJson.filter(a=>a.itemId == e.id);
	scanPageId = e.id; 
	if(arr.length > 0)
	{
		var value = arr[0];
		$('.itemName').val(value.itemName);
		$('.itemDescription').val(value.itemDesc);
		$('.purchase').val(parseFloat(value.itemPurchaseCost).toFixed(2));
		$('.selling').val(parseFloat(value.itemSoldCost).toFixed(2));
		$('.uom').val(value.itemUOM);
		$('.upcCode').attr('id',value.itemId+"_item");
		$(".image_value").attr('id',value.itemId+"_image");
		$(".save_item").attr('id',value.itemId+"_save");
		$('.itemId').val(value.itemId);
		$('.minimuQuantityforItem').val(value.lowQuantityValue);
		$('.upcCode').val(value.upcCode);
	}
}
var modelScroll = true;		
function saveItem(e)
{	
	 var form = $(".edit_item");
	 if (!form.valid()) {
         return;
     }
	 var form1 = $('.edit_item')[0];
	 var itemId = e.id.replace ( /[^\d.]/g, '' ); 
	 $('#loaderDiv').show();
	
	 // ADD WITHOUT IMAGE
	 if($("#"+itemId+"_image").val() == null || $("#"+itemId+"_image").val() === undefined || $("#"+itemId+"_image").val() == "" )
	 {
		 var dataToSend = JSON.stringify([{ 
				itemName: $('.itemName').val(), 
				itemDesc: $('.itemDescription').val(),
				newPurchaseCost: $('.purchase').val(),
				newSoldCost: $('.selling').val(),
				itemPurchaseCost: $('.purchaseOld').val(),
				itemSoldCost: $('.sellingOld').val(),
				itemUOM: $('.uom').val(),
				itemId: $('.itemId').val(),
				lowQuantityValue: $('.minimuQuantityforItem').val(),
				upcCode:$('.upcCode').val(),
				userId:window.sessionStorage.getItem("userId")
		 }]);
		 var data = callingAdminServices("POST","application/json","json","shopService/updateCost",dataToSend);
		 if(data.responseCode == 400)
		 {
			$('#m_modal_5').modal('hide');
			modelScroll = false;
			setTimeout(function(){
				window.sessionStorage.removeItem("userDetails");
				window.sessionStorage.removeItem("itemDetails");
				if(sortValue == 1)
				{
					sortArrayDepend();
				}
				else
				{
					if($("#myInput")[0].value === null || $("#myInput")[0].value === undefined || $("#myInput")[0].value === "")
						loadSettings();
					else
					{
						var data = callingAdminServices("GET","application/json","json","shopService/getShopItems/"+window.sessionStorage.getItem("userId"),null);
						var finalJson =data.data;
						window.sessionStorage.setItem("itemDetails",JSON.stringify(finalJson));
						loadData('',$("#myInput")[0].value);
						$("#myInput").focus();
					}
				}
				alertMessage("Item details updated successfully","primary")
			},180);
		}
		else
		{
			var l = $("#edit_item_error")
			i(l, "danger", "Unable to edit item.Please try again");
			//alertMessage("Unable to edit item.Please try again","danger")
		}
	}
	//ADD WITH IMAGE
	else
	{
		var dataToSendWithImage = JSON.stringify({ 
				itemName: $('.itemName').val(), 
				itemDesc: $('.itemDescription').val(),
				newPurchaseCost: $('.purchase').val(),
				newSoldCost: $('.selling').val(),
				itemPurchaseCost: $('.purchaseOld').val(),
				itemSoldCost: $('.sellingOld').val(),
				itemUOM: $('.uom').val(),
				itemId: $('.itemId').val(),
				lowQuantityValue: $('.minimuQuantityforItem').val(),
				upcCode:$('.upcCode').val(),
				userId:window.sessionStorage.getItem("userId")
		});
		
		var data = new FormData(form1);
		data.append("userId",window.sessionStorage.getItem("userId"));
		data.append("shopItemDTO",dataToSendWithImage);
	    $.ajax({
	         type: "POST",
	         enctype: 'multipart/form-data',
	         url:  serviceURL+"rest/shopService/updateCostWithImage",
	         data: data,
	         headers:{"Authorization":"Bearer "+JSON.parse(window.sessionStorage.getItem("adminApiKey"))},
	         processData: false,
	         contentType: false,
	         cache: false,
	         timeout: 600000,
	         success: function (data) {
	        	 			
	        	 if(data.responseCode == 400)
	        	 {
	        			$('#m_modal_5').modal('hide');
	        			modelScroll = false;
	        			setTimeout(function(){
	        				window.sessionStorage.removeItem("userDetails");
	        				window.sessionStorage.removeItem("itemDetails");
	        				if(sortValue == 1)
	        				{
	        					sortArrayDepend();
	        				}
	        				else
	        				{
	        					if($("#myInput")[0].value === null || $("#myInput")[0].value === undefined || $("#myInput")[0].value === "")
	        						loadSettings();
	        					else
	        					{
	        						var data = callingAdminServices("GET","application/json","json","shopService/getShopItems/"+window.sessionStorage.getItem("userId"),null);
	        						var finalJson =data.data;
	        						window.sessionStorage.setItem("itemDetails",JSON.stringify(finalJson));
	        						loadData('',$("#myInput")[0].value);
	        						$("#myInput").focus();
	        					}
	        				}
	        			},180);
	        			alertMessage("Item details updated successfully","primary")
	        	 }
	        	 else
	        	 {
	        		 var l = $("#edit_item_error")
	        		 i(l, "danger", "Unable to edit item.Please try again");
	        	}
	        	 	
	         },
	         error: function (e) {
	        	
	        	/* if(data.responseCode == 405){
	        		 var l = $("#add_item_error")
	        		 i(l, "danger", "Item existed "); 
	        	}*/
	        	 var l = $("#edit_item_error")
	     		i(l, "danger", "Unable to edit item.Please try again");
	         }
	     });
		
		}
	
}		



function deleteItem(e)
{
	var itemId = e.id.replace ( /[^\d.]/g, '' ); 
	var dataToSend = JSON.stringify({
		itemId:itemId
	});
	
	Swal({
    		title: "Are you sure?",
    		text: "Data related to this Item will be wiped off and cannot be reversed.Please confirm you want to proceed to delete ?",
    		type: "warning",
    		showCancelButton: true,
    		confirmButtonText: "Yes, delete",
    							  
    	}).then(function(result){
    		if (result.value) {  
    				var data = callingAdminServices("POST","application/json","json","shopService/deleteItem",dataToSend);
    				if(data.success)
    				{
    					window.sessionStorage.removeItem("itemDetails");
    					window.sessionStorage.removeItem("userDetails");
    					var data = callingAdminServices("GET","application/json","json","shopService/getShopItems/"+window.sessionStorage.getItem("userId"),null);
    		        	var finalJson =data.data;
    		        	window.sessionStorage.setItem("itemDetails",JSON.stringify(finalJson));
    		        	if(window.sessionStorage.getItem("selectedArray") !== null && window.sessionStorage.getItem("selectedArray") !== undefined && window.sessionStorage.getItem("selectedArray") !== "")
    					{
    						var selectArrayIds = JSON.parse(window.sessionStorage.getItem("selectedArray"));
    						const index = selectArrayIds.indexOf(itemId);
    						if (index > -1) {
    							selectArrayIds.splice(index, 1);
    						}
    						window.sessionStorage.setItem("selectedArray",JSON.stringify(selectArrayIds))
    					}
    		        	console.log($("#sort_append_value").val())
    		        	if(sortValue == 1 && $("#sort_append_value").val() === "ZtoA")
    		        	{
    		        		zToASort(finalJson);
    		        		loadSettings(finalJson);
    		        	}
    		        	else if(sortValue == 1 && $("#sort_append_value").val() === "AtoZ")
    		        	{
    		        		aToZSort(finalJson);
    		        		loadSettings(finalJson);
    		        	}
    		        	else
    		        	{
    		        		loadSettings();
    		        	}	
    					alertMessage("Item deleted successfully","danger");
    				}
    				else
    				{
    					alertMessage("Unable to delete the Item","danger");
    				}
    			  }
    		});
}

function editCustomer(e)
{
	var customerId = e.id.replace ( /[^\d.]/g, '' ); 
	var dataToSend = JSON.stringify({
		cc_id:customerId
	});
	
	var finalJson = JSON.parse(window.sessionStorage.getItem("creditCustomers"));
	var arr = finalJson.filter(a=>a.cc_id == customerId);
	$("#editCustomerName").val(arr[0].cc_name);
	$("#editCustomerNumber").val(arr[0].cc_mobile);
	$(".edi_customer").prop( "id", "edit_"+customerId);
	
}

function saveEditedCustomer(e)
{
	$('#loaderDiv').hide();
	var customerId = e.id.replace ( /[^\d.]/g, '' ); 
	var form = $("#EditCustomerForm");
	if (!form.valid()) {
		return;
	}
	var dataToSend = JSON.stringify({
		cc_mobile :	$("#editCustomerNumber").val(),
		cc_name	  : $("#editCustomerName").val(),
		cc_id	  : customerId,
		userId    :	window.sessionStorage.getItem("userId")
	});
	
	var data = callingAdminServices("PUT","application/json","json","shopService/updateCreditCustomer",dataToSend);
	if(data.success)
	{
		$('#m_modal_12').modal('hide');
		setTimeout(function(){
		window.sessionStorage.removeItem("creditCustomers");
		loadCredit(null,data.data[0].cc_name);
		alertMessage("Customer details updated successfully","primary");
		},180);
	}
	else
		alertMessage("customer update failed","danger");
}


function deleteCreditCustomer(e)
{
	var customerId = e.id.replace ( /[^\d.]/g, '' ); 
	var dataToSend = JSON.stringify({
		cc_id:customerId
	});
	
	Swal({
    		title: "Are you sure?",
    		text: "Data related to this customer will be wiped off and cannot be reversed.Please confirm you want to proceed to delete?",
    		type: "warning",
    		showCancelButton: true,
    		confirmButtonText: "Yes, delete",
    							  
    	}).then(function(result){
    		if (result.value) {  
    			var data = callingAdminServices("POST","application/json","json","shopService/deleteCustomer",dataToSend);
    			if(data.success){
    				window.sessionStorage.removeItem("creditCustomers");
    				window.sessionStorage.removeItem("userDetails");
    				loadCredit();
    				alertMessage("Customer deleted successfully","danger");
    			}
    			else{
    				alertMessage("Unable to delete the Customer","danger");
    			}
    		}
    	});
}

function selectItem(e)
{								
	var array = [];
	var selectedItemId = e.id;
	var storedArray = JSON.parse(window.sessionStorage.getItem("selectedArray"));
	if(storedArray != undefined && storedArray != null)
	{
		array = storedArray;
	}
	if(array.length > 0)
	{
		var exists = false;
		for(var i =0; i < array.length; i++)
		{
			if(e.id == array[i])
			{
				exists= true;
				break;
			}
		}
		
		if(exists)
		{
			$('#'+e.id).removeClass('highlightBackground');
			array.splice( array.indexOf(e.id), 1 );
			window.sessionStorage.setItem("selectedArray",JSON.stringify(array));
		}else
		{
			$('#'+e.id).addClass('highlightBackground');
			array.push(e.id);
			window.sessionStorage.setItem("selectedArray",JSON.stringify(array));
		}
	}else
	{
		$('#'+e.id).addClass('highlightBackground');
		array.push(e.id);
		window.sessionStorage.setItem("selectedArray",JSON.stringify(array));
		
	}
	if(array.length>0)
	{
		$('.actionbuttons').show();
		$("#list").addClass("manage_widget");
	}else
	{
		$('.actionbuttons').hide();
		$("#list").removeClass("manage_widget");
	}
}


function highlightCells()
{															
	var array = [];
	var storedArray = JSON.parse(window.sessionStorage.getItem("selectedArray")); 
	if(storedArray != undefined && storedArray != null)
	{
		array = storedArray;
	}
	if(array.length > 0)
	{
		for(var i =0; i < array.length; i++)
		{
			$('#'+array[i]).addClass('highlightBackground');
		}
		$('.actionbuttons').show();
		$("#list").addClass("manage_widget");
		
	}else
	{
		$('.actionbuttons').hide();
		$("#list").removeClass("manage_widget");
		
	}
}

var selectArrays = [];
function loadPurchaseOrSell(inputValue)
{	
	$('#loaderDiv').show();
	$("#content").empty();
	var inputs;var positionId;
	selectArrays = [];
	inputValueOfPs = inputValue;
	window.sessionStorage.removeItem("onSaveItem");
	onScrollValue = false;
	$.get("./pages/purchaseorsell.html", function (data){ 
		$("#content").append(data);
		$(".scan_button").empty();
		$(".search_class").addClass("manage_serach");
    	$('#title').text(inputValue);
    	if(inputValue == 'Sell')
    	{
    		
    		$('#icon').addClass('la-angle-double-up'); 
    	}
    	else
    	{
    		$('#icon').addClass('la-angle-double-down');
    		$('#title').text("Buy");
    	}
        	
    	var array = [];
    	var storedArray = JSON.parse(window.sessionStorage.getItem("selectedArray"));  
    	if(storedArray != undefined && storedArray != null)
    	{								
    		array = storedArray;
    	}
    	var finalJson = JSON.parse(window.sessionStorage.getItem("itemDetails"));
    	var storeValue = 0;
    	 
    	manageArray =[];	
    	for(var i= 0; i< array.length; i++)
    	{
    		var arr = finalJson.filter(a=>a.itemId == array[i]);
    		manageArray.push(arr[0]);
    	}
    	
    	var k =0;
    	var autoCompleteData = [];
    	manageArray.forEach(function(element) {
    		autoCompleteData[k++] = element.itemName;
    	});
    	autoComplete(document.querySelector(".for_auto_complete"), autoCompleteData);
    	window.sessionStorage.setItem("purchaseOrSellItems" ,JSON.stringify(manageArray));
    	var startIndex = 0,stopIndex,serachInput,finaArray = [];
    	var countValue = 0;var id =1;
    	loadData = function(closeCondtion,value)
    	{   
    		var onSaveItems = JSON.parse(window.sessionStorage.getItem("onSaveItem"));
    		serachInput = value;
    		if(closeCondtion == true)
    		{
    			startIndex = 0;countValue= 0;
    		}
    		var html;
    		if(serachInput != null && serachInput != undefined && serachInput != "")
    		{
    			$('#list').empty();
    			finalJson = JSON.parse(window.sessionStorage.getItem("purchaseOrSellItems"));
    			finaArray = manageArray.filter(a=>a.itemName == serachInput || a.upcCode == serachInput);
    			
    			if(finaArray.length == 0) $("#myInput").val("no item available")
    		}
    		else if(manageArray.length >= 10)
    		{	
    			var finaValeu = 30;
    			stopIndex = startIndex + finaValeu;
    			finaArray = manageArray.slice(startIndex,stopIndex);
    			startIndex = stopIndex;

    			countValue += finaArray.length; 
    			console.log(countValue)
				if(manageArray.length == countValue)
					scrollCondtion = false;
				else
					scrollCondtion = true;
    		}
    		else
    		{
    			$('#list').empty();
    			finaArray = manageArray; 
    			scrollCondtion = false;
    		}
    		$.get("./holders/purchaseorsellItems.html", function (loadHTML) { 
    			onScrollValue = false;
    			html = loadHTML;
    			finaArray.forEach(function(value){
				var htmldata = html;
				htmldata = htmldata.replace(/:itemName/g,value.itemName);
				htmldata = htmldata.replace(/:itemDescription/g,value.itemDesc);
				htmldata = htmldata.replace(/:buyPrice/g,parseFloat(value.itemPurchaseCost).toFixed(2));
				htmldata = htmldata.replace(/:sellingPrice/g,parseFloat(value.itemSoldCost).toFixed(2));
				htmldata = htmldata.replace(/:uom/g,value.itemUOM);
				htmldata = htmldata.replace(/:quantity/g,parseFloat(value.totalQuantity).toFixed(2));
				htmldata = htmldata.replace(/:amount/g,parseFloat(value.totalAmount).toFixed(2));
				htmldata = htmldata.replace(/:itemId/g,value.itemId);
				htmldata = htmldata.replace(/:imageName/g,serviceURL+"rest/shopService/showImage/"+window.sessionStorage.getItem("userId")+"/"+value.imageName);
				htmldata = htmldata.replace(/:offset/g, value.itemId);
				if(inputValue == 'Sell')
				{
					htmldata = htmldata.replace(/:sellOption/g,1);
					maxLimit = value.totalQuantity;
				}
				else
				{
					htmldata = htmldata.replace(/:sellOption/g,0);
				}
				$(htmldata).clone().appendTo("#list");
				$(".customer_quantity").empty();
				var maxLimit = 100000000;
				if(inputValue == 'Purchase')
				{  
					$('.purchase').show();
					$('.sell').hide();
				}
				else
				{
					$('.purchase').hide();
					$('.sell').show();
				} 
				inputs = $(".qunatity");
				// inputs[0].focus();
				try
				{
					$(inputs).keypress(function(e)
					{  
						if (e.keyCode == 13)
						{
							try {
								
								myFunction(e.target.id);
								inputs[inputs.index(this)+1].focus();
									
								function myFunction(e) {
									  var elmnt = document.getElementById("position_form_"+e);
									  var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
								    	if(!isMobile) 
								    		 window.scroll({top: elmnt.offsetTop- (-40), behavior: 'smooth'});
								    	else
								    		window.scroll({top: elmnt.offsetTop-55, behavior: 'smooth'});
									}
							}
							catch (e) 
							{  
								if($(".m-widget5__item ").length == manageArray.length)
								{
									$("html").animate({ scrollTop: 0 }, "slow");
									document.getElementById("save").focus();
								}
							}        	        		  
						}
					});
				}catch(e){};
				
				if(onSaveItems !== null && onSaveItems !== 0 )
				{	var onSaveItem = onSaveItems.filter(a=>a.itemId == value.itemId);
					
					if(onSaveItem.length !== 0){
						$("."+value.itemId+"_manage_value").val(onSaveItem[0].newQty);
						onManageUpdate(value.itemId);
				}
			}
				
    		});
    		onScrollValue = true;	
    		$('.m_touchspin,.buy_quatity').on('change', function() {
    			onSave(); 
    		});
    		$(".scaner").unbind().click(function(){
				scan = 1;
				app.scanBarcode();
			});
    		setTimeout(function(){
    			$('#loaderDiv').hide();
    		},150)
		});	
    }
    loadData();
    
    setTimeout(function(){
		inputs[0].focus();
		onManageUpdate(inputs[0].id);
	},350);
    		
    });
}

function onSave()
{
	var actionType = $('#title').html();
	$(".m_touchspin").each(function() 
	{ 
		var filtered = JSON.parse(window.sessionStorage.getItem("itemDetails"));
		var saveItems = filtered.filter(a=>a.itemId==this.id);
		var value = saveItems[0];
		var item = value; 
		if(this.value > 0 && saveItems.length>0 && value.itemId == this.id)
		{ 	
			   item.newQty=this.value;
	    	   item.transactionQty=this.value;
	    	   if(actionType == "Sell")
	   		   {
	    		   item.transactionType = "Sold";
	    		   item.newSoldCost=$("#"+this.id+"_sell").val();
	   		   }
	    	   else if(actionType == "Buy")
	   		   {	
	    		   item.transactionType = "Buy";
	    		   item.newPurchaseCost=$("#"+this.id+"_buy").val();
	   		   }
	    	   item.existingQty = value.totalQuantity;
	    	   delete value.totalQuantity;
	    	   delete value.lowQuantityValue;
	    	   delete value.newQuantity;
	    	   $.each(payloadArr, function(el, i){
	    		   if (this.itemId == item.itemId){
	    			   var removeIndex = payloadArr.map(function(item) { return item.itemId; }).indexOf(this.itemId);
	    			   payloadArr.splice(removeIndex, 1);
	    	   }
		    });
		    payloadArr.push(item);
        }
		else
		{
			$.each(payloadArr, function(el, i){
				if (this.itemId == item.itemId){
					var removeIndex = payloadArr.map(function(item) { return item.itemId; }).indexOf(this.itemId);
						payloadArr.splice(removeIndex, 1);
			    }
			});
		}
		window.sessionStorage.setItem("onSaveItem",JSON.stringify(payloadArr));
    });
}

function onSaveItem()
{
	var payloadArr = JSON.parse(window.sessionStorage.getItem("onSaveItem"));
	if(payloadArr == null || payloadArr == 0)
	{
	 swal({
           title: 'Please enter quantity values',
           text: "You won't be able to save without entering values",
           type: 'warning',
       });
	}
	else
	{
		var data = callingAdminServices("POST","application/json","json","shopService/updateTransaction",JSON.stringify(payloadArr)); 
		if(data.responseCode == 400)
		{	
			$('#loaderDiv').hide();
			setTimeout(function(){
				var array = [];
				window.sessionStorage.setItem("selectedArray",JSON.stringify(array));
				window.sessionStorage.removeItem("userDetails");
				window.sessionStorage.removeItem("itemDetails");
				window.sessionStorage.removeItem("onSaveItem");
				loadManageStock();
			},100);
		
		}
		else
		{
			$('#loaderDiv').hide();
			$(".show").show();
			$("#page_error").text("Sorry unable to save items")
		}
	}
	$('#loaderDiv').hide();
}

function addItem()
{
	 var form = $(".add_item");
	 if (!form.valid()) {
		 $("#loaderDiv").hide();
         return;
     }
	 var form = $('.add_item')[0];
	 var dataToSend = JSON.stringify({ 
	    	itemName: $('#newItemName').val(), 
	    	itemDesc: $('#newItemDesc').val(),
	    	itemUOM: $('#newItemUOM').val(),
	    	itemPurchaseCost: $('#newItempurchase').val(),
	    	itemSoldCost: $('#newItemMrp').val(),
	    	lowQuantityValue: $('#minimuQuantityValue').val(),
	    	totalQuantity: $('#newQuantity').val(),
	    	gstValue :$("#newItemGST").val(),
	    	upcCode : $("#upc").val(),
	        userId:window.sessionStorage.getItem("userId")
	    });
	 
	//ADD ITEM WITHOUT IMAGE
	 if($("#addImage").val() == null || $("#addImage").val() === undefined ||$("#addImage").val() == "" )
	 {
		
		 var data = callingAdminServices("POST","application/json","json","shopService/addNewItem",dataToSend);
		 if(data.success)
		 {
			 	sortValue = 0;
             	if(data.responseMessage !== null || data.responseMessage != undefined)
             	{
         			var l = $("#add_item_error")
         			i(l, "danger", data.responseMessage);
         			$(".clear_model_form").click(function(){
         	    		$(".add_item").clearForm().resetForm();
         	    	});
         	    	
             	}
             	else
             	{
             		$('#m_modal_8').modal('hide');
             		window.sessionStorage.removeItem("itemDetails");
					window.sessionStorage.removeItem("userDetails");
             		setTimeout(function(e){
             			loadSettings(null,data.data[0].itemName);
             		},150)
             	}
		 }
		 else{
                 if(data.responseCode == 405){
        		 var l = $("#add_item_error")
        		 i(l, "danger", "Item existed "); 
        	}
		 }

	 }
	 
	 else
	 {
		 // ADD IMAGE WITH IMAGE
		// Create an FormData object
		 var data = new FormData(form);
		 data.append("userId",window.sessionStorage.getItem("userId"));
		 data.append("shopItemDTO",dataToSend);
		 $.ajax({
			 type: "POST",
			 enctype: 'multipart/form-data',
			 url:  serviceURL+"rest/shopService/addNewItemWithUpload",
			 data: data,
			 headers:{"Authorization":"Bearer "+JSON.parse(window.sessionStorage.getItem("adminApiKey"))},
			 processData: false,
			 contentType: false,
			 cache: false,
			 timeout: 600000,
			 success: function (data) {
        	 			
             	window.sessionStorage.removeItem("itemDetails");
             	window.sessionStorage.removeItem("userDetails");
             	sortValue = 0;
             	if(data.responseMessage !== null || data.responseMessage != undefined)
             	{
         			var l = $("#add_item_error")
         			i(l, "danger", data.responseMessage);
         			$(".clear_model_form").click(function(){
         	    		$(".add_item").clearForm().resetForm();
         	    	})
             	}
             	else
             	{
             		$('#m_modal_8').modal('hide');
             		setTimeout(function(e){
             			loadSettings(null,data.data[0].itemName);
             		},150)
             	}
			 },
			 error: function (e) {
        	
				 if(data.responseCode == 405){
					 var l = $("#add_item_error")
					 i(l, "danger", "Item existed "); 
				 }
			 }
		 });
	 	}
}

var mrpCost;
function gstCalculate(orginalAmount,gst)
{
	if($("#newItemMrp").val()){
		$("#newItemGST").prop("disabled", false);
	}
	else
		$("#newItemGST").prop("disabled", true);
	var mrp = parseInt(orginalAmount);
	var gstValue = parseInt(gst);
	var GSTAmount = ( mrp * gstValue )/100;
	var NetPrice = mrp + GSTAmount;
	console.log("after gst "+ NetPrice);
	
}

/**
 * ********************************************************CreditCustomer
 * Module****************************************************************************
 */

function loadCredit(sortArray,addedItem)
{	
	$("#m_aside_left").removeClass("m-aside-left--on");
	$("#m_aside_left_offcanvas_toggle").removeClass("m-brand__toggler--active");
	$('#loaderDiv').show();
	$("#content").empty();
	$('ul li').removeClass("m-menu__item--active");
	$("#credit").addClass("m-menu__item--active");
	$(".m-aside-left-overlay").removeClass("m-aside-left-overlay");
	// paginationValue === "cc";
	onScrollValue = false;
	$.get("./pages/customer.html", function (customerHtml) {
			var finalJson = [];
			if(sortValue == 1){
				finalJson = sortArray;
				sortValue = 0;
	    	}
	        else
	        {
	        	if(window.sessionStorage.getItem("creditCustomers") != null && window.sessionStorage.getItem("creditCustomers") != undefined && window.sessionStorage.getItem("creditCustomers") != "undefined")
	        	{
	        		finalJson = JSON.parse(window.sessionStorage.getItem("creditCustomers"));
	        	}else
	        	{
	        		var data = callingAdminServices("POST","application/json","json","shopService/getCreditCustomers/"+window.sessionStorage.getItem("userId"),null);
	        		var finalJson =data.data;
	        		window.sessionStorage.setItem("creditCustomers",JSON.stringify(finalJson));
	        		
	        	}
	        }
			var guest = finalJson.filter(a=>((a.cc_name).toUpperCase()) === "GUEST");
			if(finalJson.length != 0 && guest != null && guest != undefined && guest !="")
			{
				customerHtml = customerHtml.replace(/:guest_id/g,guest[0].cc_id);
				finalJson.splice(finalJson.findIndex(v => ((v.cc_name).toUpperCase()) === "GUEST"), 1);
			}
			
			$("#content").append(customerHtml);
			var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
		    if(!isMobile){
		    	$("#cc").focus();
		    	$(".addcontact").hide();
		    } 
			var form = $("#AddCustomerForm");
				form.validate({
								rules: {
										customerName: 
										{
											required: true
										},
										mobileNumber: 
										{
											required: true
										}
								}
							});
				
		if(addedItem !== null && addedItem !== undefined && addedItem !== "" )
		{
			$("#cc").val(addedItem);
			$(".close_action").addClass("la-close");
		}
			        	
		var k =0;
		var countValue = 0;
     	var autoCompleteData = [];
     	finalJson.forEach(function(element) { 
     		autoCompleteData[k++] = element.cc_name;
     		autoCompleteData[k++] = element.cc_mobile
     	}); 
     	var removedDuplicateArray = [];
    	removedDuplicateArray = removeDuplicates(autoCompleteData);
    	function removeDuplicates(autoCompleteData) {
    		  return autoCompleteData.filter((a, b) => autoCompleteData.indexOf(a) === b)
    	};
    	autoComplete(document.querySelector(".for_auto_complete"), removedDuplicateArray);
    	var startIndex = 0,stopIndex,serachInput,finaArray = [];
    	loadData = function(closeCondtion,value)
    	{	
    		serachInput = value;
    		if(closeCondtion == true)
    		{
    			startIndex = 0;
    			countValue= 0;
    		}

    		if(finalJson.length != 0)
    		{
    			if(serachInput != null && serachInput != undefined && serachInput != "")
    			{
    				$('#list').empty();
    				finaArray = finalJson.filter(function(a) {
    					  return (a.cc_name == serachInput || a.cc_mobile == serachInput)
    					});
    				
    			}
    			else if(finalJson.length >= 10)
    			{	
    				var displayCount = 30;
    				stopIndex = startIndex + displayCount;
    				finaArray = finalJson.slice(startIndex,stopIndex);
    				startIndex = stopIndex;

    				countValue += finaArray.length; 
    				console.log(countValue)
    				if(finalJson.length == countValue)
    					scrollCondtion = false;
    				else
    					scrollCondtion = true;
    			}
    			else
    			{
    				finaArray = finalJson; 
    				scrollCondtion = false;
    			}
        	var html;
    		$.get("./holders/customers.html", function (loadHTML) {
    			onScrollValue = false;
    			html = loadHTML;
    			finaArray.forEach(function(value){
        			var htmldata = html;
        			htmldata = htmldata.replace(/:mobile/g,value.cc_mobile);
    	      		htmldata = htmldata.replace(/:customerName/g,value.cc_name);
    	      		htmldata = htmldata.replace(/:status/g,value.status);
    	      		htmldata = htmldata.replace(/:pendingAmount/g,parseFloat(value.cc_credit_amount).toFixed(2));
    	      		//htmldata = htmldata.replace(/:paidAmount/g,parseFloat(value.cc_paid_amount-value.cc_credit_amount).toFixed(2));
    	      		htmldata = htmldata.replace(/:paidAmount/g,parseFloat(value.cc_paid_amount).toFixed(2));
    	      		htmldata = htmldata.replace(/:cc_id/g,value.cc_id);
    	      		if(value.status == "Pending")
    	  			{
    	      			htmldata = htmldata.replace(/:class/g,"m-badge--danger m_sweetalert_demo_8"); 
    	  			}
    	      		else
    				{
    	  				htmldata = htmldata.replace(/:class/g,"m-badge--brand");
    				}
    	      		$('#list').append(htmldata);
        		});
    			onScrollValue = true;
    			$('#loaderDiv').hide();
    			$('.m_sweetalert_demo_8').click(function(e) {
    	              
        			var cc_credit_amount = 0;
        			var creditCustomers = JSON.parse(window.sessionStorage.getItem("creditCustomers"));
        			var arr = creditCustomers.filter(a=>a.cc_id == e.target.id.replace("_status",""));
        			var name;
        			var phone;
        		
        			if(arr.length > 0)
    				{
        				cc_credit_amount = arr[0].cc_credit_amount;
        				name = arr[0].cc_name;
        				phone = arr[0].cc_mobile;
    				}
        			var clearValue;
            		Swal.fire({
    				  title: 'Please enter credit amount',
    				  input: 'text',
    				  inputValue: parseFloat(cc_credit_amount).toFixed(2),
    				  showCancelButton: true,
    		          footer: '<a href="#" onclick="sendRemainder(\''+name+'\',91'+phone+','+parseFloat(cc_credit_amount).toFixed(2)+');" class="btn btn-success m-btn m-btn--icon"><span><i class="la la-whatsapp"></i><span>Send Remainder</span></span></a>',
    				  inputValidator: (value) => {
    					    if(value > cc_credit_amount ||(isNaN(value)) || (value <= 0))
    				    	{	
    					    	 return 'Enter valid input or amount should not be greater than credit amount';
    				    	}
    					    
    					  }
    				}).then(function(result) {
    					clearValue = parseFloat(result.value).toFixed(2);
    					
    					if(result.value){
    						Swal({
    							title: "Are you sure?",
    						    text: "You will not be able to recover this credit",
    						    type: "warning",
    							showCancelButton: true,
    						    confirmButtonText: "Yes, clear",
    							  
    							}).then(function(result){
    								
    								if (result.value) {  
    			                    	var data = callingAdminServices("POST","application/json","json","shopService/clearCredit/"+e.target.id.replace("_status","")+"/"+clearValue+"/"+cc_credit_amount,null); 
    			                		if(data.responseCode == 400)
    			                		{	
    			                			window.sessionStorage.removeItem("creditCustomers");
    			                			window.sessionStorage.removeItem("userDetails");
    			                			$('#loaderDiv').hide();
    			                			loadCredit();
    			                			
    			                		}else
    			                		{
    			                			$('#loaderDiv').hide();
    			                			$('.show').show();
    			                		}
    			                    }
    							});
    					}
                     });
                });
            });
        }
        else
        {
			$.get("./pages/noContent.html", function (data) {
				$("#list").append(data);
				$(".icon_animation").addClass("pulsing");
				$("#customers_error").text("No customers available please create customers");
				document.getElementById("dropdownMenuButton").disabled = true; $('#loaderDiv').hide();
			});
        }
    	}
    	
    	if((addedItem != null) && (addedItem != undefined) && (addedItem != "") )
    	    loadData("",addedItem);
    	else
    	    loadData();
    	
    	$(".clear_model_form").click(function(){ 
    		$("#AddCustomerForm").clearForm().resetForm();
    	})
    });
	setTimeout(function(){
		mApp.init();
		$('#loaderDiv').hide();
	},100);
}

function createGuestCustomer(userId)
{
	var dataToSend = JSON.stringify({ 
		userId : userId,
		cc_name:"GUEST",
		cc_mobile:"0000000000"
	});
	var data = callingAdminServices("POST","application/json","json","shopService/createCreditCustomer",dataToSend);
	var finalJson =data.data;
	if(data.responseCode == 400)
	{}
	else
	{
		alert("guest customer creation error");
	}
}

function createCustomer()
{	
	var form = $("#AddCustomerForm");
	if (!form.valid()) {
		$("#loaderDiv").hide();
		return;
	}
	var dataToSend = JSON.stringify({ 
		userId : window.sessionStorage.getItem("userId"),
		cc_name:$("#customerName").val(),
		cc_mobile:$("#number").val()
	});
	$('#loaderDiv').show(); 
	setTimeout(function(){
		var data = callingAdminServices("POST","application/json","json","shopService/createCreditCustomer",dataToSend);
		var finalJson =data.data;
 	
		if(data.responseCode == 400)
		{	
			$('#m_modal_3').modal('hide');
			setTimeout(function(e){
				$('#loaderDiv').hide(); 
				var creditId = finalJson[0].cc_id;
				window.sessionStorage.removeItem("creditCustomers");
				showCreditItems(creditId);
			},180)
		}
		else
		{
			var l = $("#add_cutstomer_error")
			i(l, "danger", data.responseMessage);
		}
	 },150);
}



function showCreditItems(e,back,customerTransactionId)
{	
	
	$('#loaderDiv').show(); 
	$("#content").empty();
	// payloadArr =[];
	var creditId;
	var creditCustomerName;
	var mobileNumber;
	var inputs;
	var start =0;
	var array = [];
	onScrollValue = false;
	if(varCheckId){}
	else
		customerTransactionId = null;
	selectArrays = [];
	var finalJson = [];
	if(back == null || back == undefined || back == "")
		payloadArr = [];
	if(window.sessionStorage.getItem("creditCustomers") != null && window.sessionStorage.getItem("creditCustomers") != undefined && window.sessionStorage.getItem("creditCustomers") != "undefined")
	{
		finalJson = JSON.parse(window.sessionStorage.getItem("creditCustomers"));
	}
	else
	{
		var data = callingAdminServices("POST","application/json","json","shopService/getCreditCustomers/"+window.sessionStorage.getItem("userId"),null);
		var finalJson =data.data;
    	window.sessionStorage.setItem("creditCustomers",JSON.stringify(finalJson));
	}
	if(e == undefined || e == null)
	{	
		array = finalJson.slice(finalJson.length-1);
		creditId = array[start].cc_id;
	}
	else if(e != null && e != undefined && !isNaN(e))
	{
		creditId = e;  
	}
	else
	{
		creditId = e.id;
	}
	customerIdFor = e;
	var arr = finalJson.filter(a=>a.cc_id == creditId);
	if(arr.length > 0)
	{
		creditCustomerName = arr[0].cc_name;
		mobileNumber = "91"+arr[0].cc_mobile;
		window.sessionStorage.setItem("creditCustomerName",creditCustomerName);
		window.sessionStorage.setItem("creditMobileNumber",mobileNumber);
	}
	var finalJson = [];
	if(window.sessionStorage.getItem("itemDetails") != null && window.sessionStorage.getItem("itemDetails") != undefined && window.sessionStorage.getItem("itemDetails") != "undefined")
	{
		finalJson = JSON.parse(window.sessionStorage.getItem("itemDetails"));
	}
	else
	{  
		var data = callingAdminServices("GET","application/json","json","shopService/getShopItems/"+window.sessionStorage.getItem("userId"),null);
    	var finalJson =data.data;
    	window.sessionStorage.setItem("itemDetails",JSON.stringify(finalJson));
	}
	
	var filtered = finalJson.filter(a=>a.totalQuantity>0);
	var k =0;var countValue = 0;
  	var autoCompleteData = [];
  	$.each( filtered, function( key, value ) {
		  autoCompleteData[k++] = value.itemName;
		  autoCompleteData[k++] = value.upcCode;
  	});
  	var removeNullArray = autoCompleteData.filter(function (el) {
  	  return el != null;
  	});
  	
	var autoCompleteForGrams = ["50","100","150","200","250","300","350","400","450","500","550","600","650","700","750","800","850","900","950"];
  	scanPageId = "creditItems";
	$.get("./pages/purchaseorsell.html", function (htmldata) {
		htmldata = htmldata.replace(/:userId/g,window.sessionStorage.getItem("userId"));
		htmldata = htmldata.replace(/:cc_id/g,creditId);
		
		$(htmldata).clone().appendTo("#content");
		var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
		if(!isMobile)$(".print").html("save & Print");
		$('.title').text(creditCustomerName);
        $(".manage_back,.on_save,.manage_serach").hide();
        $(".search_class").addClass("customer_search");
        $(".cc_back,.credit_items_save").show();
        $(".managestock-items").hide();
		$(".cc-items").show();
		autoComplete(document.querySelector(".for_auto_complete"), removeNullArray);
		
		var startIndex = 0,stopIndex,serachInput,finaArray = [];var id = 1
		loadData = function(closeCondtion,value)
		{    
			var previewItems = JSON.parse(window.sessionStorage.getItem("previewItems"));
			serachInput = value;
			if(closeCondtion == true)
			{
				startIndex = 0;countValue= 0;
			}
			if(filtered.length > 0)
			{
				var html;
				if(serachInput != null && serachInput != undefined && serachInput != "")
				{
					$('#list').empty();
					finaArray = filtered.filter(function(a) {
  					  return (a.itemName == serachInput || a.upcCode == serachInput)
  					});
					if(finaArray.length == 0) $("#myInput").val("no item available")
					serachRetrive = false;
				}
				else if(filtered.length >= 10)
				{	
					var displayCount = 30;
					stopIndex = startIndex + displayCount;
					finaArray = filtered.slice(startIndex,stopIndex);
					startIndex = stopIndex;
					countValue += finaArray.length; 
					console.log(countValue)
    				if(filtered.length == countValue)
    					scrollCondtion = false;
    				else
    					scrollCondtion = true;
				}
				else
				{
					finaArray = filtered; 
					scrollCondtion = false;
				}
				$.get("./holders/purchaseorsellItems.html", function (loadHTML) {
					html = loadHTML;
					onScrollValue = false;
					finaArray.forEach(function(value){
						var htmldata = html;
						htmldata = htmldata.replace(/:itemName/g,value.itemName);
						htmldata = htmldata.replace(/:itemDescription/g,value.itemDesc);
						htmldata = htmldata.replace(/:buyPrice/g,parseFloat(value.itemPurchaseCost).toFixed(2));
						htmldata = htmldata.replace(/:sellingPrice/g,parseFloat(value.itemSoldCost).toFixed(2));
						if(value.itemUOM === "Each")
						{	
							htmldata = htmldata.replace(/:uom/g,value.itemUOM);
							htmldata = htmldata.replace(/:quantity/g,parseFloat(value.totalQuantity));
						}
						else
						{
	    					var uomValue = value.itemUOM;const[uom1,uom2] = uomValue.split("/");
	    					htmldata = htmldata.replace(/:uom/g,uom1);
	    					htmldata = htmldata.replace(/:gram/g,uom2);
	    					htmldata = htmldata.replace(/:quantity/g,parseFloat(value.totalQuantity).toFixed(2));
						}
						htmldata = htmldata.replace(/:amount/g,parseFloat(value.totalAmount).toFixed(2));
						htmldata = htmldata.replace(/:itemId/g,value.itemId);
						var disabledCount = id;
						htmldata = htmldata.replace(/:imageName/g,serviceURL+"rest/shopService/showImage/"+window.sessionStorage.getItem("userId")+"/"+value.imageName);
						htmldata = htmldata.replace(/:formId/g,"form_"+ id++);
						htmldata = htmldata.replace(/:offset/g, value.itemId);
						$('#list').append(htmldata);
						$("#mrp_change_"+ value.itemId).val(parseFloat(value.itemSoldCost).toFixed(2)); 
						$("#mrp_"+ value.itemId).prop("disabled", true);
						$(".purchase,.sell").hide();
						$(".cc_mrp").show();
						$(".manage_quantity").empty();
						var maxLimit = value.totalQuantity;
						$('.m_touchspin').TouchSpin({
							buttondown_class: 'btn btn-secondary',
							buttonup_class: 'btn btn-secondary',
							min: 0,
							max: maxLimit,
							step: 1,
							decimals: 0,
							boostat: 5,
							maxboostedstep: 10,
							// postfix: 'Kg'
						});
	    			
						$('.m_touchspin_1').TouchSpin({
							buttondown_class: 'btn btn-secondary',
							buttonup_class: 'btn btn-secondary',
							min: 0,
							max: 999,
							step: 50,
							decimals: 0,
							boostat: 5,
							maxboostedstep: 10,
							 // postfix: 'Gm'
						});
						var buttonClass = $(".m_touchspin")[0].parentNode.children[0].classList[0];$("."+buttonClass).hide();
						if(previewItems !== null && previewItems !== 0 )
						{	
							var previewItem = previewItems.filter(a=>a.itemId == value.itemId);
							if(previewItem.length !== 0)
							{
								if(previewItem[0].itemUOM === "Each")
								{
									$("."+value.itemId).val(previewItem[0].transactionQty);
									$("#mrp_change_"+value.itemId).val(previewItem[0].newSoldCost);
								}
									
								else
								{
									var transactionQty = previewItem[0].transactionQty;
									const[unit1,unit2] = transactionQty.split(".");
									$("."+value.itemId).val(unit1);
									$(".gram_"+value.itemId).val(unit2);
									$("#mrp_change_"+value.itemId).val(previewItem[0].newSoldCost);
								}
							}
						}
						if(value.itemUOM === "Each")
						{
							$("#width_changes_"+ value.itemId).addClass("width_changes_1");
							$("#width_changes_"+ value.itemId).removeClass("width_changes");
							$("#form_form_"+disabledCount).removeClass("qunatity");
							document.getElementById("qunatity_gram_form_"+disabledCount).style.display = "none";
						}
						else 
						{	
							$("#width_changes_"+ value.itemId).removeClass("width_changes_1");
	    					$("#width_changes_"+ value.itemId).addClass("width_changes");
						}
						inputs = $(".qunatity");
						if(finaArray.length == 1)
							inputs[0].focus();
						
						try{
							(inputs).keypress(function(e){ 
								
								var value = this.placeholder
								var id = this.name;
								if (e.keyCode == 13)
								{ 	
									try {
										inputs[inputs.index(this)+1].focus();
										document.querySelector(".scanButtonFocus").focus();
										if(value === "Grams" || value === "ML" || value === "Each")
											myFunction(id);
										function myFunction(e) 
										{
											  var elmnt = document.getElementById("position_form_"+e);
											  var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
											  if(isMobile) 
											  {
												  	const userAgent = navigator.userAgent.toLowerCase();
												  	const isTablet = /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(userAgent);
												  	if(isTablet){
												  		window.scroll({top: elmnt.offsetTop-(-160), behavior: 'smooth'});
												  	}
												  	else
												  		window.scroll({top: elmnt.offsetTop-20, behavior: 'smooth'});
											  }
											  else
										    	//desktop
										    		window.scroll({top: elmnt.offsetTop- (-85), behavior: 'smooth'});
										}	
									}
									catch (e) { 
										
										if(scan && ($(".m-widget5__item ").length == 1))
										{
											var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
										    if(!isMobile)
										    	send(window.sessionStorage.getItem("returnDbDeviceId"),window.sessionStorage.getItem("userId"));
										    else{}
										    	app.scanBarcode();
											scan = false;
										}
										if($(".m-widget5__item ").length == filtered.length)
										{
											$("html").animate({ scrollTop: 0 }, "slow");
											document.querySelector(".credit_items_save").focus();
										}
									}        	        		  
								}
							});
						}catch(e){};
						function myFunction(e) {
							  var elmnt = document.getElementById("position_"+e);
							  window.scroll({top: elmnt.offsetTop, behavior: 'smooth'});
						}
					});
					onScrollValue = true;
					$('.buy_quatity').on('change', function() {  
						
						setTimeout(function(){
							
							
								previewCreditItems(e,customerTransactionId);
						},50)
					});		
					$('.buy_quatity').on('focusout', function() { 
						previewCreditItems(e,customerTransactionId); 
					});
					setTimeout(function(){
						$('#loaderDiv').hide();
					},500);
					$("input[name='demo2']").on("touchspin.on.max", function () {
    					var id = $(this)[0].id; 
    					var item = filtered.filter(a=>a.itemId==$(this)[0].form[1].id);
    					var kgValue = ++($(this)[0].form[1].value);
    					if(kgValue > item[0].totalQuantity)
    						$(this)[0].form[1].value = item[0].totalQuantity;
    					$(".bootstrap-touchspin-up").click(function(){
    						if(($("#"+id).val()) >=999)
    							document.getElementById(id).value = 0;
    				})
    			});
        		$( ".m_touchspin_1" ).keydown(function(e) {  
	        		var id = $(this)[0].id;
	    			autoComplete(document.getElementById(id), autoCompleteForGrams);
    		  	});
            }); 
        }
        else
        {  
        	document.getElementById("dropdownMenuButton_1").disabled = true;
			document.querySelector(".credit_items_save").disabled = true;
        	$.get("./pages/noContent.html", function (data) {
				$("#list").append(data);
				$("#items_error").text("No items avilabale to buy");
			});$('#loaderDiv').hide();
        }
		$(".scaner").unbind().click(function(){
			userId = this.id;
			scan = true;
			var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
			if(!isMobile)
				send(window.sessionStorage.getItem("returnDbDeviceId"),userId);
			else
			{
				mobileVersion = true;
				app.scanBarcode();
			}
		});
		
	}
	loadData();
		setTimeout(function(){
			var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
			if(!isMobile) 
				$("#myInput").focus();
			else{
				setTimeout(function(){
					inputs[0].focus();
				},80)
			}
				
		},180)
	});
}

function closingFunctionapp(e){ 
	if(e.value >998)
		document.getElementById("finalOutcome").value = "";
}


var payloadArr = [];
var myArray;
function previewCreditItems(e,customerTransactionId)
{	
	var creditId = e.id;
	var oldCredit = 0;
	var discount = 0;
	var cc_prev_credit_amount = 0;
	if(window.sessionStorage.getItem("updateInvoiceTransactions") != null && window.sessionStorage.getItem("updateInvoiceTransactions") != undefined && window.sessionStorage.getItem("updateInvoiceTransactions") != "")
	{
		var custTransactionItems = JSON.parse(window.sessionStorage.getItem("updateInvoiceTransactions"));
		oldCredit = custTransactionItems[0].oldCredit;
		discount =  custTransactionItems[0].discount;
		cc_prev_credit_amount = custTransactionItems[0].cc_prev_credit_amount;
	}	
	var finalJson = [];
	var confirm = false; 
	var id = 1;
	var filtered = JSON.parse(window.sessionStorage.getItem("itemDetails"));
	$(".m_touchspin").each(function() {  
		
		if(($(this)[0].form[4].value).length == 2)
			confirm = true;
		var gramValue = Number("0."+$(this)[0].form[4].value); 
		var previewItems = filtered.filter(a=>a.itemId == this.id);
		var value = previewItems[0];
		if(value.itemUOM === "Each") gramValue = 0;
		var item = value;  	 	
		if((this.value > 0 && previewItems.length>0) || (gramValue > 0))
		{ 	
			var qunatityValue = Number(this.value);
			var totalQuantityValue;
			item.newSoldCost = $("#mrp_change_"+item.itemId).val();
			if(item.itemUOM === "KG/Grams" || item.itemUOM === "Litre/ML" )
			{
				if(confirm)
					totalQuantityValue = (qunatityValue+gramValue).toFixed(2);
				else
					totalQuantityValue = (qunatityValue+gramValue).toFixed(3);
			}
		    if(item.itemUOM === "Each" )
		    {
		    	item.transactionQty = parseFloat(qunatityValue);
		    	item.newQty= parseFloat(qunatityValue); 
		    }
		    else
		    {
		    	item.transactionQty= totalQuantityValue;
		    	item.newQty= totalQuantityValue;
		    }
		    item.existingQty=value.totalQuantity;
		    item.customerId= creditId;
		    item.transactionType= "Sold";
		    value.itemSoldCost = parseFloat($("#mrp_change_"+value.itemId).val());
		    value.newSoldCost = parseFloat($("#mrp_change_"+value.itemId).val());
		   // delete value.totalQuantity;
		    delete value.newQuantity;
		    if(customerTransactionId != null && customerTransactionId != undefined && customerTransactionId != "")
		    {
		    	var transactionItem = custTransactionItems.filter(a=>a.itemId == this.id);
		    	if(transactionItem.length == 0)
		    		 value.previousQuantity = 0;
		    	else
		    		value.previousQuantity = transactionItem[0].previousQuantity;
		    	value.customerTransactionId = customerTransactionId;
		    	value.oldCredit = oldCredit;
		    	value.discount  = discount;
		    	value.cc_prev_credit_amount = cc_prev_credit_amount;
		    }
		    
		    $.each(payloadArr, function(el, i){
		        if (this.itemId == item.itemId){
		        	 var removeIndex = payloadArr.map(function(item) { return item.itemId; }).indexOf(this.itemId);
		        	 payloadArr.splice(removeIndex, 1);
		        }
		    });
		    payloadArr.push(item);
       }
	   else
	   {
		   $.each(payloadArr, function(el, i){
			   if (this.itemId == item.itemId){
				   var removeIndex = payloadArr.map(function(item) { return item.itemId; }).indexOf(this.itemId);
				   payloadArr.splice(removeIndex, 1);
			   }
		   });
		}
		window.sessionStorage.setItem("previewItems",JSON.stringify(payloadArr));
    });

	if(payloadArr.length == 0){
		window.sessionStorage.setItem("previewItems",null);
	}
	//var finalJson = JSON.parse(window.sessionStorage.getItem("previewItems"));
}


function previewInvoiceChange(e,customerTransactionId)
{
	var finalJson = JSON.parse(window.sessionStorage.getItem("previewItems")); 
	var payloadArr = [];
	var creditCustomerId = e.id.replace ( /[^\d.]/g, '' ); 
	$.each(finalJson, function(key,value){
	
		var item = value;
		if(item.itemUOM === "KG/Grams" || item.itemUOM === "Litre/ML" )
		{
			item.transactionQty = parseFloat($("#"+item.itemId+"_qty").val()).toFixed(3);
			item.newQty= parseFloat($("#"+item.itemId+"_qty").val()).toFixed(3); 
		}
		if(item.itemUOM === "Each" )
		{
			item.transactionQty = $("#"+item.itemId+"_qty").val();
			item.newQty=$("#"+item.itemId+"_qty").val(); 
		}
		item.newSoldCost = $("#"+item.itemId+"_mrp").val();
		item.customerId = creditCustomerId;
		if($("#finalCredit").text()>0)
		{
			item.status="Credit";
			item.paidAmount=$("#totalAmountPaid").val();
			item.discount = $("#discount").val();
			item.transactionAmount = $("#totalAmount").text();
		}
		else
		{
			item.status="Paid";
			item.paidAmount=$("#totalAmountPaid").val();
			item.discount = $("#discount").val();
			item.transactionAmount = $("#totalAmount").text();
		}
		item.oldCredit = $("#previousCredit").text();
		/*item.transactionQty = $("#"+item.itemId+"_qty").val();
		item.newQty= $("#"+item.itemId+"_qty").val();*/ 
		
		/*delete item.lowQuantityValue;
		delete item.totalQuantity;*/
		if(customerTransactionId != null && customerTransactionId != undefined && customerTransactionId != "" )
		{
			delete item.paidAmount;
			delete item.transactionAmount;
		}
		payloadArr.push(item);
	});
	window.sessionStorage.setItem("previewItems",JSON.stringify(payloadArr));
	showCreditItems(creditCustomerId,'back',customerTransactionId);
}

function previewItemsInVoice(e)
{
	var creditId = e.id;
	var finalJson = JSON.parse(window.sessionStorage.getItem("previewItems"));
	var userDetails = callingAdminServices("GET","application/json","json","shopService/shopLogin/"+window.sessionStorage.getItem("userId"),null);
	window.sessionStorage.setItem("userDetails",JSON.stringify(userDetails));
	if(finalJson == null || finalJson == 0)
	{
        swal({
            title: 'Please select items',
            text: "You won't be able to buy without selection",
            type: 'warning',
        });$('#loaderDiv').hide();
	}
	else
	{	
		aToZSort(finalJson);
		setTimeout(function(){
			$('#m_modal_2').modal('show');
			$("#previewItems").empty();
			var creditCustomers = JSON.parse(window.sessionStorage.getItem("creditCustomers"));
			var creditCustomer = creditCustomers.filter(a=>a.cc_id==creditId);
			var totalAmount=0.0;
			var html;
    		$.get("./holders/previewItems.html", function (loadHTML) {
    			html = loadHTML;
	      		
    			finalJson.forEach(function(value){
        			var htmldata = html;
        			htmldata = htmldata.replace(/:itemName/g,value.itemName);
					htmldata = htmldata.replace(/:mrp/g,parseFloat(value.itemSoldCost).toFixed(2));
					htmldata = htmldata.replace(/:quantity/g,value.newQty);
					htmldata = htmldata.replace(/:amount/g,parseFloat(value.itemSoldCost * value.newQty).toFixed(2));
					htmldata = htmldata.replace(/:itemId/g,value.itemId);
					if(value.itemUOM === "Each")
	    				htmldata = htmldata.replace(/:uom/g,"U");
	    			else
	    			{
	    					var uomValue = value.itemUOM;const[uom1,uom2] = uomValue.split("/");
	    					htmldata = htmldata.replace(/:uom/g,uom1);
	    					// htmldata = htmldata.replace(/:gram/g,uom2);
	    			}
					$('#previewItems').append(htmldata);
					totalAmount = totalAmount+(value.itemSoldCost * value.newQty);
					$("#TaPlusPa").text(parseFloat(totalAmount).toFixed(2))
					$("#totalAmount").text(parseFloat(totalAmount).toFixed(2));
					$("#totalAmountPaid").val(parseFloat(totalAmount).toFixed(2));
					$("#finalCredit").text(parseFloat(0).toFixed(2));
					$("#previousCredit").text(parseFloat(creditCustomer[0].cc_credit_amount).toFixed(2))
					$("#totalAmountCredit").text(parseFloat(totalAmount+creditCustomer[0].cc_credit_amount).toFixed(2));	
					$("#totalAmountPaid").val(parseFloat(totalAmount+creditCustomer[0].cc_credit_amount).toFixed(2));
					$("#finalCredit").text(parseFloat(0).toFixed(2));
					$("#discount").val(parseFloat(0).toFixed(2));
					if(JSON.parse(window.sessionStorage.getItem("userDetails")).discount == "NO"){
						document.getElementById("showDiscountColumn").style.display = "none";
					}
        		});
            });
		},100);
	}
	
}

function previewInvoice(e)
{
	var creditId = e.id;
	var customerTransactionId;
	var previousQuantity = 0;
	var creditValue = 0.00;
	var discount = 0.00;
	var finalJson = JSON.parse(window.sessionStorage.getItem("previewItems"));
	var updateTransactions = JSON.parse(window.sessionStorage.getItem("updateInvoiceTransactions"));
	if(updateTransactions != null && window.sessionStorage.getItem("customerTransactionId") != null 
			&& window.sessionStorage.getItem("customerTransactionId") != undefined && window.sessionStorage.getItem("customerTransactionId") != "")
	{	customerTransactionId = updateTransactions[0].customerTransactionId;
		creditValue = parseFloat(updateTransactions[0].oldCredit).toFixed(2);
		discount = parseFloat(updateTransactions[0].discount).toFixed(2);
	}
	var userDetails = callingAdminServices("GET","application/json","json","shopService/shopLogin/"+window.sessionStorage.getItem("userId"),null);
	window.sessionStorage.setItem("userDetails",JSON.stringify(userDetails));
	if(finalJson == null || finalJson == 0)
	{
        swal({
            title: 'Plese select items',
            text: "You won't be able to buy without selection",
            type: 'warning',
        });$('#loaderDiv').hide();
	}
	else
	{	
		$("#content").empty();
		aToZSort(finalJson);
		
		$.get("./pages/invoice.html", function (htmldata) {
			
			htmldata = htmldata.replace(/:cc_id/g,creditId);
			htmldata = htmldata.replace(/:customerTrasanctionId/g,customerTransactionId);
			$(htmldata).clone().appendTo("#content");
			$(".invoice_customer_back").show();
			$(".invoice_transactions_back").hide();
	        var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
			if(!isMobile)$(".print").html("save & Print");
			setTimeout(function(){
				$('#m_modal_2').modal('show');
				$("#previewItems").empty();
				var creditCustomers = JSON.parse(window.sessionStorage.getItem("creditCustomers"));
				var creditCustomer = creditCustomers.filter(a=>a.cc_id==creditId);
				if(customerTransactionId != null && customerTransactionId != undefined && customerTransactionId != ""){
					$(".invoice_customer_back").hide();
					$(".invoice_transactions_back").show();
				}
				else
					creditValue = parseFloat(creditCustomer[0].cc_credit_amount).toFixed(2);
			
				var totalAmount=0.0;
				var html;
	    		$.get("./holders/previewItems.html", function (loadHTML) {
	    			
	    			html = loadHTML;
	    			finalJson.forEach(function(value){
	        			var htmldata = html;
	        			htmldata = htmldata.replace(/:itemName/g,value.itemName);
	        			if(value.itemSoldCost != 0 && value.itemSoldCost != null && value.itemSoldCost != undefined && value.itemSoldCost != "")
	        				htmldata = htmldata.replace(/:mrp/g,parseFloat(value.newSoldCost).toFixed(2));
	        			if(value.customerTransactionId != null && value.customerTransactionId != undefined)
	        				htmldata = htmldata.replace(/:customerItemDel/g,12);
	        			else
	        				htmldata = htmldata.replace(/:customerItemDel/g,null);
	        			if(value.previousQuantity != undefined && value.previousQuantity != null && value.customerTransactionId != null && value.customerTransactionId != undefined)
	        				previousQuantity = value.previousQuantity;
	        			else
	        				previousQuantity = 0;
						htmldata = htmldata.replace(/:mrp/g,parseFloat(value.itemSoldCost).toFixed(2));
						htmldata = htmldata.replace(/:quantity/g,value.newQty);
						htmldata = htmldata.replace(/:totalQuantity/g,value.totalQuantity);
						htmldata = htmldata.replace(/:amount/g,parseFloat(value.itemSoldCost * value.newQty).toFixed(2));
						htmldata = htmldata.replace(/:totalAvailabeQunatity/g,parseFloat(value.totalQuantity + previousQuantity));
						htmldata = htmldata.replace(/:itemId/g,value.itemId);
						if(value.itemUOM === "Each")
		    				htmldata = htmldata.replace(/:uom/g,"U");
		    			else
		    			{
		    				var uomValue = value.itemUOM;const[uom1,uom2] = uomValue.split("/");
		    				htmldata = htmldata.replace(/:uom/g,uom1);
		    			} 	 
						$('#previewItems').append(htmldata);
						
						totalAmount = totalAmount+(value.itemSoldCost * value.newQty);
						$("#TaPlusPa").text(parseFloat(totalAmount).toFixed(2))
						$("#totalAmount").text(parseFloat(totalAmount).toFixed(2));
						$("#totalAmountPaid").val(parseFloat(totalAmount).toFixed(2));
						$("#finalCredit").text(parseFloat(0).toFixed(2));
						if(customerTransactionId != null && customerTransactionId != undefined && customerTransactionId != "")
						{
							
							if(discount != undefined && discount != null && discount != "" && discount != 0){
								$("#showDiscountColumn").show();
								$("#discount").val(parseFloat(discount).toFixed(2));
							}
							else
								$("#showDiscountColumn").hide();
	                        $("#previousCredit").text(parseFloat(creditValue).toFixed(2));
						}
	                    else
	                    {
	                    	$("#previousCredit").text(parseFloat(creditCustomer[0].cc_credit_amount).toFixed(2));
	                    	if(JSON.parse(window.sessionStorage.getItem("userDetails")).discount == "NO"){
	    						document.getElementById("showDiscountColumn").style.display = "none";
	    					}
	                    	$("#discount").val(parseFloat(0).toFixed(2));
	                    }
						$("#totalAmountCredit").text(parseFloat(totalAmount+parseFloat(creditValue)).toFixed(2));
		                $("#totalAmountPaid").val(parseFloat(totalAmount + parseFloat(creditValue) - parseFloat(discount) ).toFixed(2));
						$("#finalCredit").text(parseFloat(0).toFixed(2));	
						//$("#totalAmountCredit").text(parseFloat(totalAmount+creditCustomer[0].cc_credit_amount).toFixed(2));	
						//$("#totalAmountPaid").val(parseFloat(totalAmount+creditCustomer[0].cc_credit_amount).toFixed(2));
	        		});
	    			if(creditCustomer[0].cc_name === "GUEST")
	    				$("#totalAmountPaid").prop('disabled', true);
	    		});
			},100);
		});
	}
}

function onManageUpdate(e)
{
	var id;
	try{id = e.id.replace ( /[^\d.]/g, '' )}catch(f){id = e;}; 
	var manageTitle = $("#title").text();
	var calculatedValue;
	if(manageTitle === "Buy")
	{	
		calculatedValue = (($("#"+id+"_buy").val()) * ($("."+id+"_manage_value").val()));
	}
	else
	{
		calculatedValue = (($("#"+id+"_sell").val()) * ($("."+id+"_manage_value").val()));
		if($(".sell_alert ").is(":visible"))
			$(".on_save").prop('disabled',true);
		else
			$(".on_save").prop('disabled',false);
	}
	$("#"+id+"_auto_value").text(calculatedValue.toFixed(2)+"Rs");
}

var counter = 1;
function onPaidUpdate(e)
{	
	if(e.id=='discount')
	{
		if(parseFloat($("#discount").val()) > parseFloat($("#totalAmountCredit").text()))
		{
			//$("#discount").val($("#totalAmountCredit").text())
			$("#discount").val(parseFloat(0).toFixed(2));
		}
		//if($("#discount").val() === '') $("#discount").val(parseFloat(0).toFixed(2));
		$("#totalAmountPaid").val(parseFloat($("#totalAmountCredit").text()-$("#discount").val()).toFixed(2));
	}
	/*counter++;
	if(counter > 2)
	{ 
		if($("#totalAmountCredit").text()-$("#discount").val()-$("#totalAmountPaid").val() >0)
		{
			$("#finalCredit").text(parseFloat($("#totalAmountCredit").text()-$("#discount").val()-$("#totalAmountPaid").val()).toFixed(2));
		}else
		{
			$("#totalAmountPaid").val(parseFloat($("#totalAmountCredit").text()-$("#discount").val()).toFixed(2));
			counter = 1;
			$("#finalCredit").text(parseFloat(0).toFixed(2));
		}
	}*/
	
		if($("#totalAmountCredit").text()-$("#discount").val()-$("#totalAmountPaid").val() >0)
		{
			$("#finalCredit").text(parseFloat($("#totalAmountCredit").text()-$("#discount").val()-$("#totalAmountPaid").val()).toFixed(2));
			
			$('#totalAmountPaid').removeClass('quatity_alert');
			$("#totalAmountAlert").hide();
			$('.dis').prop('disabled',false);
			
			
			
		}else
		{
			if($("#totalAmountCredit").text()-$("#discount").val() < $("#totalAmountPaid").val()){
				//alert("not enter max value");
				$('#totalAmountPaid').addClass('quatity_alert');
				$("#totalAmountAlert").show();
				$("#showAlertValue").text(parseFloat($("#totalAmountCredit").text()-$("#discount").val()).toFixed(2))
				$('.dis').prop('disabled',true);
			}
			else{
			//$("#totalAmountPaid").val(parseFloat($("#totalAmountCredit").text()-$("#discount").val()).toFixed(2));
			//counter = 1;
			$('#totalAmountPaid').removeClass('quatity_alert');
			$("#totalAmountAlert").hide();
			$('.dis').prop('disabled',false);
			$("#finalCredit").text(parseFloat(0).toFixed(2));
			}
		}
	
	
	
	
	
	
	
	
	
	
	
}

function saveCreditItems(e,msg,customerTransactionId)
{
	
	var deletedItems = JSON.parse(window.sessionStorage.getItem("deletedItems"));
	Swal({
		title: "Do you want generate invoice ?",
		//text: "You will not be able to recover this Item",
		type: "warning",
		showCancelButton: true,
		confirmButtonText: "Yes",
							  
	}).then(function(result){
		if (result.value) {  
			
			$('#loaderDiv').show();
			var finalJson = JSON.parse(window.sessionStorage.getItem("previewItems")); 
			var payloadArr = [];
			var creditCustomerId = e.id.replace ( /[^\d.]/g, '' ); 
			$.each(finalJson, function(key,value){
				
				var item = value;
				item.customerId = creditCustomerId;
				if($("#finalCredit").text()>0)
				{
					item.status="Credit";
					item.paidAmount=$("#totalAmountPaid").val();
					item.discount = $("#discount").val();
					item.transactionAmount = $("#totalAmount").text();
				}else
				{
					item.status="Paid";
					item.paidAmount=$("#totalAmountPaid").val();
					item.discount = $("#discount").val();
					item.transactionAmount = $("#totalAmount").text();
				}
				
				item.oldCredit = $("#previousCredit").text();
			    if(item.itemUOM === "KG/Grams" || item.itemUOM === "Litre/ML" )
				{
					item.transactionQty = parseFloat($("#"+item.itemId+"_qty").val()).toFixed(3);
					item.newQty= parseFloat($("#"+item.itemId+"_qty").val()).toFixed(3); 
				}
			    else
				{
					item.transactionQty = $("#"+item.itemId+"_qty").val();
					item.newQty=$("#"+item.itemId+"_qty").val(); 
				}
				//item.transactionQty = $("#"+item.itemId+"_qty").val();
				item.newSoldCost = $("#"+item.itemId+"_mrp").val();
				delete item.lowQuantityValue;
				delete item.totalQuantity;
				if(customerTransactionId != null && customerTransactionId != undefined && customerTransactionId != "" )
				{
					item.customerTransactionId = customerTransactionId;
					item.transactionType = "Sold"
					delete item.totalQuantity;
					delete item.newQuantity;
					
				}
				
				payloadArr.push(item);
				console.log(deletedItems)
				if(deletedItems != null )
				{
					for(i =0;i<payloadArr.length;i++)
					{
						var filtered = deletedItems.filter(a=>a.itemId == payloadArr[i].itemId);  
						if(filtered.length != 0)
							deletedItems.splice(i,1);
					}
					
					for(i=0;i<deletedItems.length;i++)
					{
						delete deletedItems[i].lowQuantityValue;
						delete deletedItems[i].totalQuantity;
						delete deletedItems[i].newQuantity;
						deletedItems[i].newQty = "0";
						deletedItems[i].transactionQty = "0";
						if($("#finalCredit").text()>0)
						{
							deletedItems[i].status="Credit";
							deletedItems[i].paidAmount=$("#totalAmountPaid").val();
							deletedItems[i].discount = $("#discount").val();
							deletedItems[i].transactionAmount = $("#totalAmount").text();
						}else
						{
							deletedItems[i].status="Paid";
							deletedItems[i].paidAmount=$("#totalAmountPaid").val();
							deletedItems[i].discount = $("#discount").val();
							deletedItems[i].transactionAmount = $("#totalAmount").text();
						}
						deletedItems[i].transactionType = "Sold";
						deletedItems[i].oldCredit = $("#previousCredit").text();
						
						
						
						
						
						
						
						
						
						
						
						payloadArr.push(deletedItems[i]);
					}
				}
			});
			if(deletedItems != null ){
				
			
			
			}
			console.log(JSON.stringify(payloadArr))
			setTimeout(function(){
				var data = callingAdminServices("POST","application/json","json","shopService/updateTransaction",JSON.stringify(payloadArr));
				if(data.responseCode == 400)
				{	$('#loaderDiv').hide();
					$('#m_modal_2').modal('hide');
					setTimeout(function(){
						window.sessionStorage.removeItem("userDetails");
						window.sessionStorage.removeItem("creditCustomers");
						window.sessionStorage.removeItem("itemDetails");
						window.sessionStorage.removeItem("previewItems");
						window.sessionStorage.removeItem("deletedItems");
						window.sessionStorage.removeItem("updateInvoiceTransactions");
						if('whatsapp' == msg)
						{
							var ua = navigator.userAgent.toLowerCase();
							var isAndroid = ua.indexOf("android") > -1; // &&
																		// ua.indexOf("mobile");
							if(isAndroid) {
								var url = serviceURL+"rest/shopService/showPDF/"+data.responseMessage;
								window.open('whatsapp://send?phone='+window.sessionStorage.getItem("creditMobileNumber")+'&text='+encodeURI(constructMessage(payloadArr))+"&ru="+encodeURI(url), '_blank');
							}
							else
							{
								var url = serviceURL+"rest/shopService/showPDF/"+data.responseMessage;
								openPDF(url);
							}
							
						}
						payloadArr =[];
						
						if(customerTransactionId != null && customerTransactionId != undefined && customerTransactionId != "")
						{
							window.sessionStorage.removeItem("customerTransactions");
							loadCustomerTransactions(creditCustomerId+"_transactions");
							alertMessage("Customer transaction updated","primary");
						}
						else
							loadCredit();
					},300);
				}
				else
				{
					$('#loaderDiv').hide();
					/*$('#m_modal_2').modal('hide');
					var l = $("#error_msg");
					i(l, "danger", "Transaction failed or Please check your internet");*/
					alertMessage("Transaction failed or Please check your internet","danger");
				}
			},100);
		
			}
		});
}


/*function updateInvoiceTransaction(e,msg)
{
	Swal({
		title: "Do you want generate invoice ?",
		//text: "You will not be able to recover this Item",
		type: "warning",
		showCancelButton: true,
		confirmButtonText: "Yes",
							  
	}).then(function(result){
		if (result.value) {  
			
			$('#loaderDiv').show();
			var finalJson = JSON.parse(window.sessionStorage.getItem("previewItems")); 
			var payloadArr = [];
			var creditCustomerId = e.id.replace ( /[^\d.]/g, '' ); 
			$.each(finalJson, function(key,value){
				
				var item = value;
				$("#"+item.itemId+"_qty").val();
				$("#"+item.itemId+"_mrp").val();
				
				item.customerId = creditCustomerId;
				if($("#finalCredit").text()>0)
				{
					item.status="Credit";
					item.paidAmount=$("#totalAmountPaid").val();
					item.discount = $("#discount").val();
					item.transactionAmount = $("#totalAmount").text();
				}else
				{
					item.status="Paid";
					item.paidAmount=$("#totalAmountPaid").val();
					item.discount = $("#discount").val();
					item.transactionAmount = $("#totalAmount").text();
				}
				item.oldCredit = $("#previousCredit").text();
				item.transactionQty = $("#"+item.itemId+"_qty").val();
				item.newSoldCost = $("#"+item.itemId+"_mrp").val();
				delete item.lowQuantityValue;
				delete item.totalQuantity;
				delete item.newQuantity;
				item.transactionType = "Sold"
				payloadArr.push(item);
			});
			setTimeout(function(){
				var data = callingAdminServices("POST","application/json","json","shopService/updateTransaction",JSON.stringify(payloadArr));
				if(data.responseCode == 400)
				{	$('#loaderDiv').hide();
					$('#m_modal_2').modal('hide');
					setTimeout(function(){
						window.sessionStorage.removeItem("userDetails");
						window.sessionStorage.removeItem("creditCustomers");
						window.sessionStorage.removeItem("itemDetails");
						window.sessionStorage.removeItem("previewItems");
						if('whatsapp' == msg)
						{
							var ua = navigator.userAgent.toLowerCase();
							var isAndroid = ua.indexOf("android") > -1; // &&
																		// ua.indexOf("mobile");
							if(isAndroid) {
								var url = serviceURL+"rest/shopService/showPDF/"+data.responseMessage;
								window.open('whatsapp://send?phone='+window.sessionStorage.getItem("creditMobileNumber")+'&text='+encodeURI(constructMessage(payloadArr))+"&ru="+encodeURI(url), '_blank');
							}
							else
							{
								var url = serviceURL+"rest/shopService/showPDF/"+data.responseMessage;
								openPDF(url);
							}
							
						}
						payloadArr =[];
						loadCredit();
					},300);
				}
				else
				{
					$('#loaderDiv').hide();
					$('#m_modal_2').modal('hide');
					var l = $("#error_msg");
					i(l, "danger", "Transaction failed or Please check your internet")
				}
			},100);
		
			}
		
		});
	
}
*/

function invoiceChanges(e)
{
	if($(".invoice_alert ").is(":visible"))
		$(".dis").prop('disabled',true);
	else
		$(".dis").prop('disabled',false);
	var id = e.id.replace ( /[^\d.]/g, '' ); 
	var valueOne = parseFloat($("#"+id+"_qty").val()).toFixed(3);
	var valueTwo = parseFloat($("#"+id+"_mrp").val()).toFixed(3);
	var result  = valueOne * valueTwo;
	$("#"+id+"_amount").text(parseFloat(result).toFixed(2));
	if(valueOne === "NaN" || valueTwo === "NaN" )
		$("#"+id+"_amount").text("0.00");
	var amount = $(".amount");
	var value = 0;
	for(var i=0;i< amount.length;i++){
	    var eachValue = parseFloat(amount[i].innerHTML);
	    value += eachValue; console.log("type " +typeof value)
	    $("#totalAmount").text(value.toFixed(2));
	    $("totalAmountCredit").text()
	}
	var previousCredit = parseFloat($("#previousCredit").text());
	var totalAmount = parseFloat($("#totalAmount").text());
	$("#totalAmountPaid").val((previousCredit + totalAmount).toFixed(2));
	$("#totalAmountCredit").text((previousCredit + totalAmount).toFixed(2));
	var discount = parseFloat($("#discount").val()).toFixed(2);
	if(discount != 0 && discount != "NaN" )
		$("#totalAmountPaid").val(((previousCredit + totalAmount)-discount).toFixed(2));
	$("#finalCredit").text(parseFloat(0).toFixed(2));
}

function deleteInvoiceItem(e,set)
{
	var element = document.getElementById(e.id+"_list");
    element.parentNode.removeChild(element);
    var retrieveArray= JSON.parse(sessionStorage.previewItems);
    var deletedItems = [];
    for (i=0; i<retrieveArray.length; i++)
    {
        if (retrieveArray[i].itemId == e.id) {
        	retrieveArray[i].transactionQty = 0;
        	retrieveArray[i].newQty = 0;
        	deletedItems.push(retrieveArray[i]);
        	retrieveArray.splice(i,1);
        }
    }
    for (i=0; i<payloadArr.length; i++)
    {
        if (payloadArr[i].itemId == e.id) {
        	payloadArr.splice(i,1);
        }
    }
    console.log(JSON.stringify(deletedItems))
    var amount = $(".amount");
	var value = 0;
	for(var i=0;i< amount.length;i++)
	{
	    var eachValue = parseFloat(amount[i].innerHTML);
	    value += eachValue; console.log("type " +typeof value)
	    $("#totalAmount").text(value.toFixed(2));
	    $("totalAmountCredit").text()
	}
	if(amount.length == 0){
		$("#totalAmount").text((0).toFixed(2));
	    $("totalAmountCredit").text()
	}
	var previousCredit = parseFloat($("#previousCredit").text());
	var totalAmount = parseFloat($("#totalAmount").text());
	$("#totalAmountPaid").val((previousCredit + totalAmount).toFixed(2));
	$("#totalAmountCredit").text((previousCredit + totalAmount).toFixed(2));
	var discount = parseFloat($("#discount").val()).toFixed(2);
	if(discount != 0 && discount != undefined )
		$("#totalAmountPaid").val(((previousCredit + totalAmount) - discount).toFixed(2));
	$("#finalCredit").text(parseFloat(0).toFixed(2))
    window.sessionStorage.setItem("previewItems",JSON.stringify(retrieveArray));
	if(set == 12)
		window.sessionStorage.setItem("deletedItems",JSON.stringify(deletedItems));
    if(window.sessionStorage.getItem("previewItems") == null || window.sessionStorage.getItem("previewItems") == undefined || window.sessionStorage.getItem("previewItems") == "[]" ){
    	$("#totalAmountCredit").text("0.00");$("#totalAmountPaid").val("0.00");
    	$(".dis").prop("disabled", true);
    }
    else if($(".invoice_alert ").is(":visible"))
		$(".dis").prop('disabled',true);
	else
		$(".dis").prop('disabled',false);
}

function sendRemainder(name,mobile,amount)
{
	var str = "Dear "+name+",\nYou have credit of amount Rs "+amount+"\n We request you to clear the pending credit";
	var ua = navigator.userAgent.toLowerCase();
	var isAndroid = ua.indexOf("android") > -1; // && ua.indexOf("mobile");
	if(isAndroid) {
		swal.close();
		window.open('whatsapp://send?phone='+mobile+'&text='+encodeURI(str), '_blank');
		
	}else
	{
		swal.close();
		window.open('https://wa.me/'+mobile+'&text='+encodeURI(str), '_blank');
	}
}

function constructMessage(finalJson)
{
	var today = new Date();
	var date = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear(); 
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date+' '+time;
	var str = "Dear "+window.sessionStorage.getItem("creditCustomerName")+",\nYour Bill for the Purachse on "+dateTime+" is below\n\n*Item Qty Price Total*\n";
	var summary = finalJson[0];
	var totalAmount = 0.0;
	
	$.each(finalJson, function(key,value){
		var item = value;
		str = str+item.itemName+" "+item.transactionQty+" "+parseFloat(item.newSoldCost).toFixed(2)+" "+parseFloat(item.transactionQty*item.newSoldCost).toFixed(2)+"\n";
		totalAmount = totalAmount+(item.transactionQty*item.newSoldCost);
	});
	str = str+"\n*Total Amount* "+parseFloat(totalAmount).toFixed(2)+"\n"; 
	str = str+"*Paid Amount* "+parseFloat(summary.paidAmount).toFixed(2)+"\n";
	str = str+"*Credit Amount* "+parseFloat(totalAmount-summary.paidAmount).toFixed(2)+"\n";
	return str;
}

function loadCustomerTransactions(e)
{	
	$('#loaderDiv').show();
	var creditCustomerId = e.replace( /[^\d.]/g, '' ); 
	if(window.sessionStorage.getItem("userDetails") != null && window.sessionStorage.getItem("userDetails") != undefined && window.sessionStorage.getItem("userDetails") != "undefined")
		userDetails = JSON.parse(window.sessionStorage.getItem("userDetails"));
	else
	{
		userDetails = callingAdminServices("GET","application/json","json","shopService/shopLogin/"+window.sessionStorage.getItem("userId"),null);
    	window.sessionStorage.setItem("userDetails",JSON.stringify(userDetails));
	}
	setTimeout(function(){
		
		
		var date = new Date();
    	var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    	var dd = firstDay.getDate(); 
        var mm = firstDay.getMonth() + 1; 
        var yyyy = firstDay.getFullYear(); 
        if (dd < 10) { 
            dd = '0' + dd; 
        } 
        if (mm < 10) { 
            mm = '0' + mm; 
        } 
        var firstDay1 = mm + '/' + dd + '/' + yyyy; 
        var firstDay = yyyy + '/' + mm + '/' + dd; 
        
    	
    	var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    	var dd = lastDay.getDate(); 
        var mm = lastDay.getMonth() + 1; 

        var yyyy = lastDay.getFullYear(); 
        if (dd < 10) { 
            dd = '0' + dd; 
        } 
        if (mm < 10) { 
            mm = '0' + mm; 
        } 
        var lastDay1 = mm + '/' + dd + '/' + yyyy; 
        var lastDay = yyyy + '/' + mm + '/' + dd; 
    	
    	
    	//loadFilters();
    	var startdate=firstDay;
    	var enddate=lastDay;
    	//var transactionType=[];
    	//var itemIds=[];
    	
    	
    	var dataToSend = JSON.stringify({ 
    		customerId :creditCustomerId,
    		startDate:startdate,
    		endDate:enddate
        });
		
		
		
		
		
		
		
		
		
		
		
		
		//var data = callingAdminServices("POST","application/json","json","shopService/getCreditCustomerTransactions/"+e.id.replace("_transactions",""),null);
		var data = callingAdminServices("POST","application/json","json","shopService/getCreditCustomerTransactions/",dataToSend);
		var finalJson =data.data;
		window.sessionStorage.setItem("customerTransactions",JSON.stringify(finalJson))
		if(finalJson.length !== 0)
		{	
			$("#content").empty();
			$.get("./pages/customerTransactions.html", function (data) {
				data = data.replace(/:cc_id/g,creditCustomerId);
				$("#content").append(data);
				$('#start').val(firstDay1);
				$('#end').val(lastDay1);
				var count = 0;
				setTimeout(function(){
					//mApp.init();
					$('#m_datepicker_5').datepicker({
						rtl: mUtil.isRTL(),
						todayHighlight: true,
						templates: arrows
					});
					$('.selectpicker').selectpicker();
				},60);
				$.get("./holders/customerTransaction.html", function (loadHTML) {
					
					var html = loadHTML;
		      		finalJson.forEach(function(val){
		      			$('#loaderDiv').show();
		      			count = count+1;
		      			var transactions = val.transactions;
		      			transactions.forEach(function(value){
		      				var htmldata = html;
		      				if(value.transactionQty != 0)
		      				{
		      					htmldata = htmldata.replace(/:id/g,val.cc_id);
		      					htmldata = htmldata.replace(/:paid/g,parseFloat(val.cc_paid_amount).toFixed(2));
		      					htmldata = htmldata.replace(/:total/g,parseFloat(val.cc_total_amount).toFixed(2));
		      					if(val.invoiceNumber != null && val.invoiceNumber != "")
		      					{
		      						htmldata = htmldata.replace(/:date/g,val.transactionDate+" - "+ val.invoiceNumber);
		      					}else
		      					{
		      						htmldata = htmldata.replace(/:date/g,val.transactionDate);
		      					}
							
		      					htmldata = htmldata.replace(/:itemName/g,value.itemName);
		      					htmldata = htmldata.replace(/:quantity/g,value.transactionQty);
		      					htmldata = htmldata.replace(/:mrp/g,parseFloat(value.itemSoldCost).toFixed(2));
		      					htmldata = htmldata.replace(/:amount/g,parseFloat(value.totalAmount).toFixed(2));
		      					htmldata = htmldata.replace(/:discount/g,parseFloat(val.cc_discount).toFixed(2));
		      					htmldata = htmldata.replace(/:previousCredit/g,parseFloat(val.cc_prev_credit_amount).toFixed(2));
		      					htmldata = htmldata.replace(/:currentCredit/g,parseFloat(val.cc_prev_credit_amount+val.cc_total_amount).toFixed(2));
		      					htmldata = htmldata.replace(/:credit/g,parseFloat(val.cc_prev_credit_amount+val.cc_total_amount-val.cc_discount-val.cc_paid_amount).toFixed(2));
		      					htmldata = htmldata.replace(/:imageName/g,serviceURL+"rest/shopService/showImage/"+window.sessionStorage.getItem("userId")+"/"+value.imageName);
		      					htmldata = htmldata.replace(/:cc_id/g,parseFloat(val.customer_id));
		      					htmldata = htmldata.replace(/:invoiceNumber/g,val.invoiceNumber);
		      					$('#previewItems').append(htmldata);
		      				}
		      			});
		      			onScrollValue = false;
		      			setTimeout(function(){
		      				
							var table = $('#m_table_1');
							// table.destroy();
							// begin first table
							var i=0;
							table.DataTable({
								 order: [[9, 'desc']],
								 responsive: true,
								destroy: true,
								startClassName: 'm-table__row--primary',
								rowGroup: {
									  endRender: function ( rows, group ) {
							               //alert(i);
							               if(i==0){
							            	 //  alert(i);
							            	   i++;
							            	   return '<div align="right" style="width:100%;border-bottom:1px solid black;padding:0px;margin:0px;color:black;"><table border="0" style="width:50%;padding:0px;margin:0px;color:black;"><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Current Purchase </td><td style="white-space: nowrap;" align="right"> '+rows
							                    .data().pluck(1).reduce( function (a, b) {
							                        return a
							                    })
							                     +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Previous Credit </td><td style="white-space: nowrap;" align="right"> ' + rows
								                    .data()
								                    .pluck(10).reduce( function (a, b) {
								                        return a
								                    })
								                    +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Current Credit </td><td style="white-space: nowrap;" align="right"> ' + rows
								                    .data()
								                    .pluck(11).reduce( function (a, b) {
								                        return a
								                    })
							                     +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Discount Amount</td><td style="white-space: nowrap;" align="right"> ' + rows
								                    .data()
								                    .pluck(2).reduce( function (a, b) {
								                        return a
								                    })
							                    +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Paid Amount</td><td style="white-space: nowrap;" align="right"> ' + rows
								                    .data()
								                    .pluck(3).reduce( function (a, b) {
								                        return a
								                    })
							                    +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Final Credit </td><td style="white-space: nowrap;" align="right">  ' + rows
							                    .data()
							                    .pluck(4).reduce( function (a, b) {
							                        return a
							                    })
							                    
							                     +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left"></td> <td align="right"> '+'<a href="#" onclick="updateInvoice(this)" id='+rows.data()
							                    .pluck(12).reduce( function (a, b) {
							                    	return a;
							                    })
							                    +'><i class="flaticon-edit-1" style="color: blueviolet;">'
							               }else
						            	   {
							            	   return '<div align="right" style="width:100%;border-bottom:1px solid black;padding:0px;margin:0px;color:black;"><table border="0" style="width:50%;padding:0px;margin:0px;color:black;"><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Current Purchase </td><td style="white-space: nowrap;" align="right"> '+rows
							                    .data().pluck(1).reduce( function (a, b) {
							                        return a
							                    })
							                     +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Previous Credit </td><td style="white-space: nowrap;" align="right"> ' + rows
								                    .data()
								                    .pluck(10).reduce( function (a, b) {
								                        return a
								                    })
								                    +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Current Credit </td><td style="white-space: nowrap;" align="right"> ' + rows
								                    .data()
								                    .pluck(11).reduce( function (a, b) {
								                        return a
								                    })
							                     +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Discount Amount</td><td style="white-space: nowrap;" align="right"> ' + rows
								                    .data()
								                    .pluck(2).reduce( function (a, b) {
								                        return a
								                    })
							                    +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Paid Amount</td><td style="white-space: nowrap;" align="right"> ' + rows
								                    .data()
								                    .pluck(3).reduce( function (a, b) {
								                        return a
								                    })
							                    +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Final Credit </td><td style="white-space: nowrap;" align="right">  ' + rows
							                    .data()
							                    .pluck(4).reduce( function (a, b) {
							                        return a
							                    })
							                    
							                     +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left"></td> <td align="right"> '+'<a href="#" id='+rows.data()
							                    .pluck(12).reduce( function (a, b) {
							                    	return a;
							                    })
							                    +'>'
						            	   }
										
										  
							            },
									dataSrc:  0,
								},
						        columnDefs: [ {
						            targets: [ 0,1,2,3,4,9,10,11,12 ],
						            visible: false
						        } ],
						      
							});
							$('#loaderDiv').hide();
						},200);
					});
		      		//$('#loaderDiv').hide();
		        }); //$('#loaderDiv').hide();
			});
			
		}
		else
		{
			swal({
	            title: 'No transactions done',
	            type: 'error',
	        });
		}
		
	},50)
	
}

function applyCustTransactionFilters(e)
{
	
	setTimeout(function(){
		
	$('#loaderDiv').show();
	var startdate= $('#start').val();
	var enddate=$('#end').val();
	const[mm,dd,yyyy] = startdate.split("/");
	const[mm1,dd1,yyyy1] = enddate.split("/");
	
	var newStartDate = yyyy+"/"+mm+"/"+dd;
	var newEndDate = yyyy1+"/"+mm1+"/"+dd1;
	var creditCustomerId = e.replace ( /[^\d.]/g, '' ); 
	if(window.sessionStorage.getItem("userDetails") != null && window.sessionStorage.getItem("userDetails") != undefined && window.sessionStorage.getItem("userDetails") != "undefined")
		userDetails = JSON.parse(window.sessionStorage.getItem("userDetails"));
	else
	{
		userDetails = callingAdminServices("GET","application/json","json","shopService/shopLogin/"+window.sessionStorage.getItem("userId"),null);
    	window.sessionStorage.setItem("userDetails",JSON.stringify(userDetails));
	}
	setTimeout(function(){
    	
    	var dataToSend = JSON.stringify({ 
    		customerId :creditCustomerId,
    		startDate:newStartDate,
    		endDate:newEndDate
        });
		
		//var data = callingAdminServices("POST","application/json","json","shopService/getCreditCustomerTransactions/"+e.id.replace("_transactions",""),null);
		var data = callingAdminServices("POST","application/json","json","shopService/getCreditCustomerTransactions/",dataToSend);
		var finalJson =data.data;
		window.sessionStorage.setItem("customerTransactions",JSON.stringify(finalJson));
		if(finalJson.length !== 0)
		{	
			$("#content").empty();
			$.get("./pages/customerTransactions.html", function (data) {
				data = data.replace(/:cc_id/g,creditCustomerId);
				$("#content").append(data);
				$('#start').val(startdate);
				$('#end').val(enddate);
				var count = 0;
				
				$.get("./holders/customerTransaction.html", function (loadHTML) {
					
					var html = loadHTML;
		      		finalJson.forEach(function(val){
		      			$('#loaderDiv').show();
		      			count = count+1;
		      			var transactions = val.transactions;
		      			transactions.forEach(function(value){
		      				var htmldata = html;
		      				if(value.transactionQty != 0)
		      				{
		      					htmldata = htmldata.replace(/:id/g,val.cc_id);
		      					htmldata = htmldata.replace(/:paid/g,parseFloat(val.cc_paid_amount).toFixed(2));
		      					htmldata = htmldata.replace(/:total/g,parseFloat(val.cc_total_amount).toFixed(2));
		      					if(val.invoiceNumber != null && val.invoiceNumber != "")
		      					{
		      						htmldata = htmldata.replace(/:date/g,val.transactionDate+" - "+ val.invoiceNumber);
		      					}else
		      					{
		      						htmldata = htmldata.replace(/:date/g,val.transactionDate);
		      					}
							
		      					htmldata = htmldata.replace(/:itemName/g,value.itemName);
		      					htmldata = htmldata.replace(/:quantity/g,value.transactionQty);
		      					htmldata = htmldata.replace(/:mrp/g,parseFloat(value.itemSoldCost).toFixed(2));
		      					htmldata = htmldata.replace(/:amount/g,parseFloat(value.totalAmount).toFixed(2));
		      					htmldata = htmldata.replace(/:discount/g,parseFloat(val.cc_discount).toFixed(2));
		      					htmldata = htmldata.replace(/:previousCredit/g,parseFloat(val.cc_prev_credit_amount).toFixed(2));
		      					htmldata = htmldata.replace(/:currentCredit/g,parseFloat(val.cc_prev_credit_amount+val.cc_total_amount).toFixed(2));
		      					htmldata = htmldata.replace(/:credit/g,parseFloat(val.cc_prev_credit_amount+val.cc_total_amount-val.cc_discount-val.cc_paid_amount).toFixed(2));
		      					htmldata = htmldata.replace(/:imageName/g,serviceURL+"rest/shopService/showImage/"+window.sessionStorage.getItem("userId")+"/"+value.imageName);
		      					htmldata = htmldata.replace(/:cc_id/g,parseFloat(val.customer_id));
		      					htmldata = htmldata.replace(/:invoiceNumber/g,val.invoiceNumber);
		      					$('#previewItems').append(htmldata);
		      				}
		      			});
		      			onScrollValue = false;
		      			setTimeout(function(){
		      				
							var table = $('#m_table_1');
							// table.destroy();
							// begin first table
							var i=0;
							table.DataTable({
								 order: [[9, 'desc']],
								 responsive: true,
								destroy: true,
								startClassName: 'm-table__row--primary',
								rowGroup: {
									  endRender: function ( rows, group ) {
							               //alert(i);
							               if(i==0){
							            	 //  alert(i);
							            	   i++;
							            	   return '<div align="right" style="width:100%;border-bottom:1px solid black;padding:0px;margin:0px;color:black;"><table border="0" style="width:50%;padding:0px;margin:0px;color:black;"><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Current Purchase </td><td style="white-space: nowrap;" align="right"> '+rows
							                    .data().pluck(1).reduce( function (a, b) {
							                        return a
							                    })
							                     +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Previous Credit </td><td style="white-space: nowrap;" align="right"> ' + rows
								                    .data()
								                    .pluck(10).reduce( function (a, b) {
								                        return a
								                    })
								                    +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Current Credit </td><td style="white-space: nowrap;" align="right"> ' + rows
								                    .data()
								                    .pluck(11).reduce( function (a, b) {
								                        return a
								                    })
							                     +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Discount Amount</td><td style="white-space: nowrap;" align="right"> ' + rows
								                    .data()
								                    .pluck(2).reduce( function (a, b) {
								                        return a
								                    })
							                    +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Paid Amount</td><td style="white-space: nowrap;" align="right"> ' + rows
								                    .data()
								                    .pluck(3).reduce( function (a, b) {
								                        return a
								                    })
							                    +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Final Credit </td><td style="white-space: nowrap;" align="right">  ' + rows
							                    .data()
							                    .pluck(4).reduce( function (a, b) {
							                        return a
							                    })
							                   /* 
							                     +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left"></td> <td align="right"> '+'<a href="#" onclick="updateInvoice(this)" id='+rows.data()
							                    .pluck(12).reduce( function (a, b) {
							                    	return a;
							                    })
							                    +'><i class="flaticon-edit-1" style="color: blueviolet;">'*/
							               }else
						            	   {
							            	   return '<div align="right" style="width:100%;border-bottom:1px solid black;padding:0px;margin:0px;color:black;"><table border="0" style="width:50%;padding:0px;margin:0px;color:black;"><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Current Purchase </td><td style="white-space: nowrap;" align="right"> '+rows
							                    .data().pluck(1).reduce( function (a, b) {
							                        return a
							                    })
							                     +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Previous Credit </td><td style="white-space: nowrap;" align="right"> ' + rows
								                    .data()
								                    .pluck(10).reduce( function (a, b) {
								                        return a
								                    })
								                    +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Current Credit </td><td style="white-space: nowrap;" align="right"> ' + rows
								                    .data()
								                    .pluck(11).reduce( function (a, b) {
								                        return a
								                    })
							                     +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Discount Amount</td><td style="white-space: nowrap;" align="right"> ' + rows
								                    .data()
								                    .pluck(2).reduce( function (a, b) {
								                        return a
								                    })
							                    +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Paid Amount</td><td style="white-space: nowrap;" align="right"> ' + rows
								                    .data()
								                    .pluck(3).reduce( function (a, b) {
								                        return a
								                    })
							                    +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left">Final Credit </td><td style="white-space: nowrap;" align="right">  ' + rows
							                    .data()
							                    .pluck(4).reduce( function (a, b) {
							                        return a
							                    })
							                    
							                     +' Rs  </td><tr><td width="50%" style="margin:0px;padding:0px;white-space: nowrap;" align="left"></td> <td align="right"> '+'<a href="#" id='+rows.data()
							                    .pluck(12).reduce( function (a, b) {
							                    	return a;
							                    })
							                    +'>'
						            	   }
										
										  
							            },
									dataSrc:  0,
								},
						        columnDefs: [ {
						            targets: [ 0,1,2,3,4,9,10,11,12 ],
						            visible: false
						        } ],
						      
							});
							$('#loaderDiv').hide();
						},200);
					});
		      		//$('#loaderDiv').hide();
		        }); //$('#loaderDiv').hide();
			});
			
		}
		else
		{
			//$("#list").empty();
			swal({
	            title: 'No transactions done',
	            type: 'error',
	        });
			 //$("#view_body").empty()
			/*setTimeout(function(){
				
				
				 $.get("./pages/noContent.html", function (data) {
					 $("#view_body").empty()
					//$("#list").removeClass("m-timeline-1 m-timeline-1--fixed");
					$("#view_body").append(data);
					$("#icon_animation").addClass("pulsing");
					$("#items_error").text("No Transactions");
					
				});},50)*/
		}
		setTimeout(function(){
			//mApp.init();
			$('#m_datepicker_5').datepicker({
				rtl: mUtil.isRTL(),
				todayHighlight: true,
				templates: arrows
			});
			$('.selectpicker').selectpicker();
		},40);
	},100)
	
	},1000)


}























/**
 * ***********************************************************End CreditCustomer
 * Module***********************************************************************
 */


function loadReports()
{
	$("#content").empty();
	$('ul li').removeClass("m-menu__item--active");
	$("#reports").addClass("m-menu__item--active");
	$("#m_aside_left").removeClass("m-aside-left--on");
	$(".m-aside-left-overlay").removeClass("m-aside-left-overlay");
	$("#content").html("<h2>This Page is under process<h2>");
																
}

function Map() {
    this.keys = new Array();
    this.data = new Object();

    this.put = function (key, value) {
        if (this.data[key] == null) {
            this.keys.push(key);
        }
        this.data[key] = value;
    };

    this.get = function (key) {
        return this.data[key];
    };

    this.remove = function (key) {
        this.keys.remove(key);
        this.data[key] = null;
    };

    this.each = function (fn) {
        if (typeof fn != 'function') {
            return;
        }
        var len = this.keys.length;
        for (var i = 0; i < len; i++) {
            var k = this.keys[i];
            fn(k, this.data[k], i);
        }
    };

    this.entrys = function () {
        var len = this.keys.length;
        var entrys = new Array(len);
        for (var i = 0; i < len; i++) {
            entrys[i] = {
                key: this.keys[i],
                value: this.data[i]
            };
        }
        return entrys;
    };

    this.isEmpty = function () {
        return this.keys.length == 0;
    };

    this.size = function () {
        return this.keys.length;
    };
}

/**
 * *******************************************SORTING CALLING METHODS FROM
 * UI**************************************
 */

/** *Sorting order By A TO Z** */
function AtoZ(e)
{											
	$('#loaderDiv').show();
	$("#list").empty();
	sortValue = 1;
	var value = e.id;
	var storedArray = JSON.parse(window.sessionStorage.getItem("itemDetails"));
	var manageSortArray = JSON.parse(window.sessionStorage.getItem("purchaseOrSellItems"));
	var transactionArray = JSON.parse(window.sessionStorage.getItem("inventoryTransactions"));
	var ccSortArray = JSON.parse(window.sessionStorage.getItem("creditCustomers"));
	if(value == "viewStock_1")
	{  
		scrollCondtion = false;
		aToZSort(storedArray);
    	loadViewStock(storedArray);
	}
	else if(value == "stockManage_1")
	{	
		scrollCondtion = false;
		aToZSort(storedArray);
		loadManageStock(storedArray);
	}
	else if(value == "setup_1")
	{	
		scrollCondtion = false;
		collectArray = aToZSort(storedArray);
		loadSettings(collectArray,"","AtoZ","AtoZ");
	}
	else if(value == "purchaseOrsell_1")
	{	
		scrollCondtion = false;
		aToZSort(manageSortArray);
		sortedDataAppend4(manageSortArray);
	}
	else if(value == "cc_itemsShow1")
	{		
		scrollCondtion = false;
		var filtered = storedArray.filter(a=>a.totalQuantity>0); 
		aToZSort(filtered);
		sortedDataAppend4(filtered,true);
	}
	else if(value == "cc_1")
	{	
		scrollCondtion = false;
		aToZSort(ccSortArray,true);
		loadCredit(ccSortArray);
	}
	else 
	{	
		scrollCondtion = false;
		aToZSort(transactionArray);
		sortedDataAppend5(transactionArray);
	}
	
}
/** *Sorting Order By Z TO A** */
function ZtoA(e)
{						
	$('#loaderDiv').show();
	$("#list").empty();
	sortValue = 1;
	var value = e.id;
	var storedArray = JSON.parse(window.sessionStorage.getItem("itemDetails"));
	var manageSortArray = JSON.parse(window.sessionStorage.getItem("purchaseOrSellItems"));
	var transactionArray = JSON.parse(window.sessionStorage.getItem("inventoryTransactions"));
	var ccSortArray = JSON.parse(window.sessionStorage.getItem("creditCustomers"));
	
	if(value == "viewStock_2")
	{   
		scrollCondtion = false;
		zToASort(storedArray);
		loadViewStock(storedArray);
	}
	else if(value == "stockManage_2")
	{	
		scrollCondtion = false;
		zToASort(storedArray);
		loadManageStock(storedArray);
	}
	else if(value == "setup_2")
	{	
		scrollCondtion = false;
		collectArray = zToASort(storedArray);
    	loadSettings(collectArray,"","ZtoA");
    	
	}
	else if(value == "purchaseOrsell_2")
	{	
		scrollCondtion = false;
		zToASort(manageSortArray);
		sortedDataAppend4(manageSortArray);
	}
	else if(value == "cc_2")
	{	
		scrollCondtion = false;
		zToASort(ccSortArray,true);
		loadCredit(ccSortArray);
	}
	else if(value == "cc_itemsShow2")
	{
		scrollCondtion = false;
		var filtered = storedArray.filter(a=>a.totalQuantity>0);
		zToASort(filtered);
		sortedDataAppend4(filtered,true);
	}
	else if(value == "Pending")
	{
		scrollCondtion = false;
		zToASort(ccSortArray,"pending");
		loadCredit(ccSortArray);
	}
	else 
	{	
		scrollCondtion = false;
		zToASort(transactionArray);
		sortedDataAppend5(transactionArray);
	}
}
/** *Sorting Order By quantity Low To High** */
function quantityLowToHigh(e)
{																			
	$('#loaderDiv').show();
	$("#list").empty();
	sortValue = 1;
	var value = e.id;
	var storedArray = JSON.parse(window.sessionStorage.getItem("itemDetails"));
	var manageSortArray = JSON.parse(window.sessionStorage.getItem("purchaseOrSellItems"));
	var transactionArray = JSON.parse(window.sessionStorage.getItem("inventoryTransactions"));
	if(value == "viewStock_3")
	{  
		scrollCondtion = false;
		quantityLowToHighSort(storedArray);
		loadViewStock(storedArray);
	}
	else if(value == "stockManage_3")
	{	
		scrollCondtion = false;
		quantityLowToHighSort(storedArray);
		loadManageStock(storedArray);
    	
	}
	else if(value == "setup_3")
	{	
		scrollCondtion = false;
		collectArray = quantityLowToHighSort(storedArray);
    	sortedDataAppend3(collectArray);
	}
	else if(value == "purchaseOrsell_3")
	{	
		scrollCondtion = false;
		quantityLowToHighSort(manageSortArray);
		sortedDataAppend4(manageSortArray);
	}
	else 
	{
		scrollCondtion = false;
		quantityLowToHighSort(transactionArray);
		sortedDataAppend5(transactionArray);
	}
	
}
/** *Sorting order By quantity High To Low** */
function quantityHighToLow(e)
{										
	$('#loaderDiv').show();
	$("#list").empty();
	sortValue = 1;
	var value = e.id;
	var storedArray = JSON.parse(window.sessionStorage.getItem("itemDetails"));
	var manageSortArray = JSON.parse(window.sessionStorage.getItem("purchaseOrSellItems"));
	var transactionArray = JSON.parse(window.sessionStorage.getItem("inventoryTransactions"));
	
	if(value == "viewStock_4")
	{  
		scrollCondtion = false;
		quantityHighToLowSort(storedArray);
		loadViewStock(storedArray);
	}
	else if(value == "stockManage_4")
	{	
		scrollCondtion = false;
		quantityHighToLowSort(storedArray);
		loadManageStock(storedArray);
	}
	else if(value == "setup_4")
	{	
		scrollCondtion = false;
		collectArray = quantityHighToLowSort(storedArray);
    	sortedDataAppend3(collectArray);
	}
	else if(value == "purchaseOrsell_4")
	{	
		scrollCondtion = false;
		quantityHighToLowSort(manageSortArray);
		sortedDataAppend4(manageSortArray);
	}
	else 
	{
		scrollCondtion = false;
		quantityHighToLowSort(transactionArray);
		sortedDataAppend5(transactionArray);
	}
	
}

/**
 * ************************************SORTED METHODS FOR APPENDING SORTED DATA
 * ON UI*******************************************
 */

var customerIdFor;
function sortedDataAppend4(sortedArrayFour,condition)
{	 
	$('#loaderDiv').show();
	var inputs; 
	var chooseOption = false;
	$(".for_auto_complete").val("");
	$(".close_action").removeClass("la-close");
	sortValue = 0;var countValue = 0;
	
	var startIndex = 0,stopIndex,serachInput,finaArray = [];var id = 1
	loadData = function(closeCondtion,value)
	{ 
		onScrollValue = false;
		scrollCondtion = false;
		filtered = sortedArrayFour;
		serachInput = value;
		if(closeCondtion == true)
		{
			startIndex = 0;countValue= 0;
		}
		if(filtered.length > 0)
		{
			var html;
			if(serachInput != null && serachInput != undefined && serachInput != "") 
			{
				$('#list').empty();
				finaArray = filtered.filter(a=>a.itemName == serachInput || a.upcCode == serachInput);
				if(finaArray.length == 0) $("#myInput").val("no item available");
			}
			else if(filtered.length >= 10)
			{	
				var displayCount = 30;
				stopIndex = startIndex + displayCount;
				finaArray = filtered.slice(startIndex,stopIndex);
				startIndex = stopIndex;
				countValue += finaArray.length; 
				console.log(countValue);
				if(filtered.length == countValue)
					scrollCondtion = false;
				else
					scrollCondtion = true;
			}
			else
			{
				finaArray = filtered; 
				scrollCondtion = false;
			}
			$.get("./holders/purchaseorsellItems.html", function (loadHTML) {
				var html = loadHTML;
				onScrollValue = false;
		  		finaArray.forEach(function(value){ 
					
		  			var htmldata = html;
					htmldata = htmldata.replace(/:itemName/g,value.itemName);
					htmldata = htmldata.replace(/:itemDescription/g,value.itemDesc);
					htmldata = htmldata.replace(/:buyPrice/g,parseFloat(value.itemPurchaseCost).toFixed(2));
					htmldata = htmldata.replace(/:sellingPrice/g,parseFloat(value.itemSoldCost).toFixed(2));
					if((condition == null) || (condition == undefined))
						htmldata = htmldata.replace(/:uom/g,value.itemUOM);
					else
					{
						if(value.itemUOM === "Each")
							htmldata = htmldata.replace(/:uom/g,value.itemUOM);
						else
						{
								var uomValue = value.itemUOM;const[uom1,uom2] = uomValue.split("/");
								htmldata = htmldata.replace(/:uom/g,uom1);
								htmldata = htmldata.replace(/:gram/g,uom2);
						}
					}	
					var disabledCount = id;
					htmldata = htmldata.replace(/:quantity/g,parseFloat(value.totalQuantity).toFixed(2));
					htmldata = htmldata.replace(/:amount/g,parseFloat(value.totalAmount).toFixed(2));
					htmldata = htmldata.replace(/:itemId/g,value.itemId);
		    		htmldata = htmldata.replace(/:imageName/,serviceURL+"rest/shopService/showImage/"+window.sessionStorage.getItem("userId")+"/"+value.imageName);
		    		htmldata = htmldata.replace(/:formId/g,"form_"+ id++);
		    		htmldata = htmldata.replace(/:offset/g, value.itemId);
		    		$('#list').append(htmldata);
					$('.m_touchspin').TouchSpin({
						buttondown_class: 'btn btn-secondary',
		                buttonup_class: 'btn btn-secondary',
		                min: 0,
		                max: maxLimit,
		                step: 1,
		                decimals: 0,
		                boostat: 5,
		                maxboostedstep: 10,
		            });
					$('.m_touchspin_1').TouchSpin({
						buttondown_class: 'btn btn-secondary',
		                buttonup_class: 'btn btn-secondary',
		                min: 0,
		                max: 999,
		                step: 50,
		                decimals: 0,
		                boostat: 5,
		                maxboostedstep: 10,
		               // postfix: '%'
		            });
					var maxLimit = 100000000;
					var buttonClass = $(".m_touchspin")[0].parentNode.children[0].classList[0];$("."+buttonClass).hide();
					if((condition == null) || (condition == undefined))
					{	
						chooseOption = false;
						$(".customer_quantity").empty();
						inputs = $(".qunatity");
						if(inputValueOfPs == 'Sell')
						{		
							maxLimit = value.totalQuantity;
							$('.purchase').hide();
							$('.sell').show();
							
						}
						else
						{   $('.purchase').show();
							$('.sell').hide();
						}
						
						var onSaveItems = JSON.parse(window.sessionStorage.getItem("onSaveItem"));
						if(onSaveItems !== null && onSaveItems !== 0 )
						{	var onSaveItem = onSaveItems.filter(a=>a.itemId == value.itemId);
							
							if(onSaveItem.length !== 0){
								$("."+value.itemId+"_manage_value").val(onSaveItem[0].newQty);
								$("#"+value.itemId+"_buy").val(onSaveItem[0].newPurchaseCost)
								$("#"+value.itemId+"_sell").val(onSaveItem[0].newSoldCost)
								onManageUpdate(value.itemId);
							}
						}
					}
					else
					{ 	
						chooseOption = true;
						$(".manage_quantity").empty();
						var buttonClass = $(".m_touchspin")[0].parentNode.children[0].classList[0];$("."+buttonClass).hide();
						$("#mrp_change_"+ value.itemId).val(parseFloat(value.itemSoldCost).toFixed(2));
						if(value.itemUOM === "Each"){
		    				$("#width_changes_"+ value.itemId).addClass("width_changes_1");
		    				$("#width_changes_"+ value.itemId).removeClass("width_changes");
		    				$("#form_form_"+disabledCount).removeClass("qunatity");
		    				document.getElementById("qunatity_gram_form_"+disabledCount).style.display = "none";
		    			}else 
		    			{	$("#width_changes_"+ value.itemId).removeClass("width_changes_1");
		    				$("#width_changes_"+ value.itemId).addClass("width_changes");
		    			}
						
						inputs = $(".qunatity");
						$(".purchase,.sell").hide();
						$('.cc_mrp').show();
						maxLimit = value.totalQuantity;
						var previewItems = JSON.parse(window.sessionStorage.getItem("previewItems"));
						if(previewItems !== null && previewItems !== 0  )
						{	var previewItem = previewItems.filter(a=>a.itemId == value.itemId);
							try{$("#mrp_change_"+ value.itemId).val(parseFloat(previewItem[0].itemSoldCost).toFixed(2))}catch(e){};
							if(previewItem.length !== 0)
							{
								if(previewItem[0].itemUOM === "Each")
									$("."+value.itemId).val(previewItem[0].transactionQty);
								else
								{
									var transactionQty = previewItem[0].transactionQty; 
									const[unit1,unit2] = transactionQty.split(".");
									$("."+value.itemId).val(unit1)
									$(".gram_"+value.itemId).val(unit2)
								}
							}
						}
					}
					$(inputs).keypress(function(e)
					{  
						if((condition == null) || (condition == undefined))
						{	
							if (e.keyCode == 13)
							{
								try {
									myFunction(e.target.id);
									inputs[inputs.index(this)+1].focus();
									function myFunction(e) {
										  var elmnt = document.getElementById("position_form_"+e);
										  var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
									    	if(!isMobile) 
									    		 window.scroll({top: elmnt.offsetTop- (-40), behavior: 'smooth'});
									    	else
									    		window.scroll({top: elmnt.offsetTop-55, behavior: 'smooth'});
										}
								}
								catch (e) {   
									
									if($(".m-widget5__item ").length == filtered.length)
									{
										$("html").animate({ scrollTop: 0 }, "slow");
										document.getElementById("save").focus();
									}
								}        	        		  
							}
						}
						else
						{
							var value = this.placeholder
							var id = this.name;
							if (e.keyCode == 13)
							{ 	
								try {
									inputs[inputs.index(this)+1].focus();
									if(value === "Grams" || value === "ML" || value === "Each")
										myFunction(id);
									function myFunction(e) {
										  var elmnt = document.getElementById("position_form_"+e);
										  var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
									    	if(isMobile) {
											  	const userAgent = navigator.userAgent.toLowerCase();
											  	const isTablet = /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(userAgent);
											  	if(isTablet){
											  		window.scroll({top: elmnt.offsetTop-(-160), behavior: 'smooth'});
											  	}
											  	else
											  		window.scroll({top: elmnt.offsetTop-20, behavior: 'smooth'});
									    	}
									    	else
									    	//desktop
									    		window.scroll({top: elmnt.offsetTop- (-85), behavior: 'smooth'});
										}	
								}
								catch (e) 
								{ 
									if(scanPageId == "creditItems")
									{
										if(scan && ($(".m-widget5__item ").length == 1)){
										var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
										if(!isMobile)
											send(window.sessionStorage.getItem("returnDbDeviceId"),window.sessionStorage.getItem("userId"));
										else{}
											//app.scanBarcode();
											scan = false;
										}
									}
									if($(".m-widget5__item ").length == filtered.length)
									{
										$("html").animate({ scrollTop: 0 }, "slow");
										document.querySelector(".credit_items_save").focus();
									}
								}        	        		  
							}
						}
			        });
					
				});
		  		onScrollValue = true;
		  		$(".scaner").unbind().click(function()
		  		{ 
		  			scan = true;
					var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
				    if(!isMobile) 
				    	send(window.sessionStorage.getItem("returnDbDeviceId"),window.sessionStorage.getItem("userId"));
				    else
				    {
				    	mobileVersion = true;
				    	app.scanBarcode();
				    }
				});
		  		
		  		$('.buy_quatity').on('change', function() { 
	    			
					if(chooseOption)
					{	
						setTimeout(function(){
							previewCreditItems(customerIdFor,customerTransactionId);
						},40);
					}
					else
						onSave();
		    	});
		  		
		  		$('.buy_quatity').on('focusout', function() { 
		  			if(chooseOption)
						previewCreditItems(customerIdFor,customerTransactionId);
					else
						onSave();
		  		});	
		  		
		  		setTimeout(function(){
					$('#loaderDiv').hide();
				},100)
				$("input[name='demo2']").on("touchspin.on.max", function () {
					var id = $(this)[0].id; 
					var item = filtered.filter(a=>a.itemId==$(this)[0].form[1].id);
					var kgValue = ++($(this)[0].form[1].value);
					if(kgValue > item[0].totalQuantity)
						$(this)[0].form[1].value = item[0].totalQuantity;
					$(".bootstrap-touchspin-up").click(function(){
						if(($("#"+id).val()) >=999)
							document.getElementById(id).value = 0;
					})
				});
				$( ".m_touchspin_1" ).keydown(function(e) {  
		    		var id = $(this)[0].id;
		 			var autoCompleteForGrams = ["50","100","150","200","250","300","350","400","450","500","550","600","650","700","750","800","850","900","950"];
					autoComplete(document.getElementById(id), autoCompleteForGrams);
			  	});
			});
    }
   
}
	loadData();
	setTimeout(function(){
		inputs[0].focus();
		if((condition == null) || (condition == undefined))
			onManageUpdate(inputs[0].id);
	},180)
}

function sortedDataAppend5(transactionArray)
{	
	$('#loaderDiv').show();	
	sortValue = 0;var countValue = 0;
	
	var startIndex = 0,stopIndex,serachInput,finaArray = [];
	loadData = function(closeCondtion,value)
	{	
		scrollCondtion = false;
		onScrollValue = false;
		finalJson = transactionArray;
		serachInput = value;
		if(closeCondtion == true)
		{
			startIndex = 0;countValue= 0;
		}
		
		if(finalJson.length !== 0)
		{
			if(serachInput != null && serachInput != undefined && serachInput != "")
			{
				$('#list').empty();
				finalJson = JSON.parse(window.sessionStorage.getItem("inventoryTransactions"));
				finaArray = finalJson.filter(a=>a.itemName == serachInput || a.customerName == serachInput );
			}
			else if(finalJson.length >= 10)
			{	
				var displayCount = 30;
				stopIndex = startIndex + displayCount;
				finaArray = finalJson.slice(startIndex,stopIndex);
				startIndex = stopIndex;
				countValue += finaArray.length; 
				console.log("fjhdjgh "+countValue)
				if(finalJson.length == countValue)
					scrollCondtion = false;
				else
					scrollCondtion = true;
			}	
			else
			{
				finaArray = finalJson; 
				scrollCondtion = false;
			}
		}
		$.get("./holders/inventoryTransactions_costupdate.html", function (costupdateHTML) {
			var costupdate = costupdateHTML;
			onScrollValue = true;
			$.get("./holders/inventoryTransactions.html", function (inventoryTransactionsHTML) {
				var inventoryTransactions = inventoryTransactionsHTML;
			
				finaArray.forEach(function(value){
					$('#loaderDiv').show();
					if("Cost Update" == value.transactionType)
					{	
						var htmldata = costupdate;
						htmldata = htmldata.replace(/:itemId/g,value.itemId);
						htmldata = htmldata.replace(/:itemName/g,value.itemName);
						htmldata = htmldata.replace(/:dateTime/g,value.transactionDate);
           		 		htmldata = htmldata.replace(/:transactionType/g,value.transactionType);
           		 		htmldata = htmldata.replace(/:itemPurchaseCost/g,parseFloat(value.itemPurchaseCost).toFixed(2));
           		 		htmldata = htmldata.replace(/:itemSoldCost/g,parseFloat(value.itemSoldCost).toFixed(2));
           		 		htmldata = htmldata.replace(/:newPurchaseCost/g,parseFloat(value.newPurchaseCost).toFixed(2));
           		 		htmldata = htmldata.replace(/:newSoldCost/g,parseFloat(value.newSoldCost).toFixed(2));
           		 		htmldata = htmldata.replace(/:transactionQty/g,value.transactionQty);
           		 		$('#list').append(htmldata);
					}
					else
					{
						var htmldata = inventoryTransactions;
						htmldata = htmldata.replace(/:itemId/g,value.itemId);
						htmldata = htmldata.replace(/:itemName/g,value.itemName);
						htmldata = htmldata.replace(/:dateTime/g,value.transactionDate);
						htmldata = htmldata.replace(/:transactionType/g,value.transactionType);
						htmldata = htmldata.replace(/:customerName/g,value.customerName);
						htmldata = htmldata.replace(/:itemPurchaseCost/g,parseFloat(value.itemPurchaseCost).toFixed(2));
						htmldata = htmldata.replace(/:itemSoldCost/g,parseFloat(value.itemSoldCost).toFixed(2));
						htmldata = htmldata.replace(/:newPurchaseCost/g,parseFloat(value.newPurchaseCost).toFixed(2));
						htmldata = htmldata.replace(/:newSoldCost/g,parseFloat(value.newSoldCost).toFixed(2));
						htmldata = htmldata.replace(/:transactionQty/g,value.transactionQty);
						htmldata = htmldata.replace(/:uom/g,value.uom);
						var txnAmount = 0;
						if("Buy" == value.transactionType)
						{
							htmldata = htmldata.replace(/:costOrPrice/g,"Price");
							htmldata = htmldata.replace(/:txnVal/g,parseFloat(value.itemPurchaseCost).toFixed(2));
							txnAmount = value.transactionQty*value.itemPurchaseCost;
						}
						else
						{
							htmldata = htmldata.replace(/:costOrPrice/g,"Cost");
							htmldata = htmldata.replace(/:txnVal/g,parseFloat(value.itemSoldCost).toFixed(2));
							txnAmount = value.transactionQty*value.itemSoldCost;
						}
						htmldata = htmldata.replace(/:txnAmount/g,parseFloat(txnAmount).toFixed(2));
						$('#list').append(htmldata);
					}
					
				});
				
				$('#loaderDiv').hide();
			});
		});
	}
	loadData();
	
	setTimeout(function(){
		$('#loaderDiv').hide();
	},100);
}

function sortArrayDepend()
{	
	var ordered_array;
	var	storeOrderData = [];
	for(var i =0;i<collectArray.length;i++)
	{
		storeOrderData[i] = collectArray[i].itemId;
	}
	var data = callingAdminServices("GET","application/json","json","shopService/getShopItems/"+window.sessionStorage.getItem("userId"),null);
	var storedArray = data.data;
	window.sessionStorage.setItem("itemDetails",JSON.stringify(storedArray))
	ordered_array = mapOrder(storedArray, storeOrderData, 'itemId');
	function mapOrder (array, sortedOrder, key) 
	{
		array.sort( function (a, b) 
		{
			var A = a[key], B = b[key];
		    let indA = sortedOrder.indexOf(A);
		    let indB = sortedOrder.indexOf(B);
		    if (indA == -1 )
		        indA = sortedOrder.length-1
		    if( indB == -1)
		        indB = sortedOrder.length-1

		    if (indA < indB ) 
		    {
		        return -1;
		    } 
		    else if (indA > indB) 
		    {
		        return 1;
		    }
		    return 0;
		    
		});
		return array;
	};
	setTimeout(function(){
		$('#loaderDiv').show();
	},30)
	if($("#myInput")[0].value === null || $("#myInput")[0].value === undefined || $("#myInput")[0].value === "")
		loadSettings(ordered_array);
	else
	{
		var data = callingAdminServices("GET","application/json","json","shopService/getShopItems/"+window.sessionStorage.getItem("userId"),null);
		var finalJson =data.data;
		window.sessionStorage.setItem("itemDetails",JSON.stringify(finalJson));
		loadData('',$("#myInput")[0].value);
	}
	// loadSettings(ordered_array);
}
/**
 * ********************************************************SORTING
 * METHODS**********************************************************
 */
function aToZSort(aToZArray,value)
{ 	
	aToZArray.sort(function(a,b)
			{	
				if(value == true)
				{
					var nameA = a.cc_name.toLowerCase(), 
			    		nameB = b.cc_name.toLowerCase();
				}
				else
				{
					var nameA = a.itemName.toLowerCase(), 
			    		nameB = b.itemName.toLowerCase();
				}
			    if(nameA < nameB)
			    	return -1;
			    if (nameA > nameB)
			    	return 1;
			    return 0;
			}); 
	return aToZArray;
}

function zToASort(zToaArray,value)
{
	zToaArray.sort(function(a,b){	
			if(value == true){
				var nameA = a.cc_name.toLowerCase(), 
					nameB = b.cc_name.toLowerCase();
			}
			else if(value == "pending")
			{
				var nameA = a.status.toLowerCase(), 
				nameB = b.status.toLowerCase();
			}
			else
			{
				var nameA = a.itemName.toLowerCase(), 
					nameB = b.itemName.toLowerCase();
			}
			if(nameA > nameB)
				return -1;
			if (nameA < nameB)
				return 1;
			return 0;
	});
	return zToaArray;
}

function quantityLowToHighSort(quantityLowArray)
{
	quantityLowArray.sort(numberAs);
	function numberAs(a,b){
			return a.totalQuantity-b.totalQuantity;
	}
	return quantityLowArray;
}

function quantityHighToLowSort(quantityHighArray)
{
	quantityHighArray.sort(numberDes);
	function numberDes(a,b){
			return b.totalQuantity-a.totalQuantity;
	}
	return quantityHighArray; 
}

function priceLowToHighSort(priceLowArray)
{
	priceLowArray.sort(numberAs);
	function numberAs(a,b){
			return a.totalAmount-b.totalAmount;
	}
	return priceLowArray;
}

function priceHighToLowSort(priceHighArray)
{
	priceHighArray.sort(numberDes);
	function numberDes(a,b){
			return b.totalAmount-a.totalAmount;
	}
	return priceHighArray;
}

/** ************************************************************************************************************************************** */
function serach()
{
	var finalvalue = $("#cc").val()
	var arr = autocompleteArray.filter(a=>a == $("#cc").val());
	if(arr.length == 0 && finalvalue.length == 10)
		$("#number").val($("#cc").val())
	else
		$("#number").val("");
}
function forClosing(){
	if($(".for_auto_complete").val() == null ||($(".for_auto_complete").val() == undefined ||$(".for_auto_complete").val() == "")) 
		filter();
	else
		$(".close_action").addClass("la-close");
}
var count = 1;
var closeValue;
function filter(ele,upcomevalue) 
{	                                         
	var forNextValue;
	var pageOffset;
	var item;
	setTimeout(function(){
		if(ele.id == "inventory")
		{  
			var matcher = new RegExp(ele.value, 'i');
			$('.m-timeline-1__items').show().not(function(){
				item =  matcher.test($(this).find('.item_title,.transaction_type,.customer_name').text());
				return item;
			}).hide();
		}
		else if(ele.id == "cc")
		{
			
			var matcher = new RegExp(ele.value, 'i');
			$('.m-widget4').show().not(function(){
				item =  matcher.test($(this).find('.m-widget4__title,.m-widget4__text').text());
				return item;
			}).hide();
		}
		else if(ele.id == "cusItemsSerach" || ele.id == "myInput")
		{ 
			var forNextValue = ele.value;
			var matcher = new RegExp(ele.value, 'i'); 
			$('.m-widget5__item').show().not(function(){
				 var item =  matcher.test($(this).find('.m-widget5__title').text());
				 return item;
			}).hide();
			
		}
	},20);
	var ele = document.querySelector(".for_auto_complete");
	if(ele.value != "") 
		$(".close_action").addClass("la-close");
	else 
	{	
		$(".close_action").removeClass("la-close");
		$('#paginationLoader').addClass("pagination_loader_adjust").show();$('#list').empty();
        setTimeout(function(){  $('#paginationLoader').removeClass("pagination_loader_adjust").hide();  },100)
		
		if(closeValue == true)
			loadData(true);
		else
			loadData(true);
	}
}

var autocompleteArray = []; var serachRetrive = false;
function autoComplete(inp, arr) 
{ 	
	autocompleteArray = arr;
	var sucess = false;
	try{var intListLength = $(".for_auto_number")[0].children.length;}catch(e){};
	var currentFocus;
	  inp.addEventListener("input", function(e) { 
	      var a, b, i, val = this.value;
	      closeAllLists();
	      if (!val) { return false;}
	      currentFocus = -1;
	      a = document.createElement("DIV");
	      a.setAttribute("id", this.id + "autocomplete-list");
	      a.setAttribute("class", "autocomplete-items");
	      
	      this.parentNode.appendChild(a);
	      for (i = 0; i < arr.length; i++) {
	        
	        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
	          b = document.createElement("DIV");
	          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
	          b.innerHTML += arr[i].substr(val.length);
	          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
	          b.addEventListener("click", function(e) {
	        	  $("#"+inp.id).focus();
	              inp.value = this.getElementsByTagName("input")[0].value;
				  var pageFun = $("#"+inp.id)[0].name; 
	              if( $("#list .m-widget5__item").length == 1 && $(".m-widget5__item")[0].classList[1] === inp.value){ }
	              else if($(".for_auto_complete")[0].value)
	              {
	            	 $('#paginationLoader').addClass("pagination_loader_adjust").show();
		             setTimeout(function(){  
						if(pageFun == "") 
							loadData('',inp.value);
						$('#paginationLoader').removeClass("pagination_loader_adjust").hide();
		             closeAllLists();
		             },150) 
	             }
	            closeAllLists();
	  	  	   });
	          a.appendChild(b);
	        }
	      }
	  });
	  inp.addEventListener('blur', function() {  
      setTimeout(function(e){
    	 if(inp.id == "myInput" || inp.id == "cc" || inp.id == "inventory" || inp.id == "cusItemsSerach" ){}
    	 else
    		 closeAllLists(); 
    	 
      },150);
    });
	 $(".for_auto_complete").on("empty", function(event) {}); 
	 $(".close_action").unbind().click(function(e){ 
		$(".for_auto_complete").val("");
		$('#paginationLoader').addClass("pagination_loader_adjust").show();
		setTimeout(function(){  
			$('#paginationLoader').removeClass("pagination_loader_adjust").hide();  
		},150)
		closeValue = true;
		serachRetrive = false;
		filter(); 
		closeAllLists(); 
		$("#list").empty();
		if(inp.id == "myInput")
			 $("#myInput").focus();
		else if(inp.id == "cc")
			 $("#cc").focus();
		else
			$("#inventory").focus();
	  });
	  inp.addEventListener("keydown", function(e) {
	    
		  var x = document.getElementById(this.id + "autocomplete-list");
	      if (x) x = x.getElementsByTagName("div");
	     
	      if (e.keyCode == 40) {
	        currentFocus++;
	        addActive(x);
	      } else if (e.keyCode == 38) { 
	        currentFocus--;
	        addActive(x);
	      } else if (e.keyCode == 13) { 
	    	 // e.preventDefault();
	        if (currentFocus > -1) {  
	          if (x) x[currentFocus].click();
	        }
	      }
	  });   
	  function addActive(x) {
	    if (!x) return false;
	    removeActive(x);
	    if (currentFocus >= x.length) currentFocus = 0;
	    if (currentFocus < 0) currentFocus = (x.length - 1);
	    x[currentFocus].classList.add("autocomplete-active");
	  }
	  function removeActive(x) {
	    for (var i = 0; i < x.length; i++) {
	      x[i].classList.remove("autocomplete-active");
	    }
	  }
	  function closeAllLists(elmnt) {
		var x = document.getElementsByClassName("autocomplete-items");
	    for (var i = 0; i < x.length; i++) {
	      if (elmnt != x[i] && elmnt != inp) {
	        x[i].parentNode.removeChild(x[i]);
	      }
	    } 
	  }
}

// ANDROID INTERFACE FUNCTIONS FROM HERE
function scanCode(){ 
	mobileVersion = true;
	app.scanBarcode();
}

function addContact(){
	app.addContact();
}

// ANDROID FUNCTIONS CALLING FROM ANDROID
function contact(contact)
{	
	const[name,phoneNo] = contact.split("&");
	var phone = phoneNo.replace ( /[^\d.]/g, '' );
	if(phone.length == 12){ phone = phone.substring(2);}
	if (!($("#m_modal_12").is(":hidden"))) {
		setTimeout(function(){
			$("#editCustomerName").val(name);
			$("#editCustomerNumber").val(phone);
		},120)
	}
	else{
		setTimeout(function(e){
			$("#customerName").val(name);
			$("#number").val(phone);
		},120);
	}
	
}
var scan = false;
function scanAppendData(barcode)
{
	if(scanPageId == "creditItems")
	{  
		if(barcode)
		{                     
			$("#myInput").val(barcode);
			$(".close_action").addClass("la-close");
			scan = true;
			loadData('',barcode);
		}
		else
		{	 
			$("#myInput").val("Barcode not detected");
			$(".close_action").addClass("la-close");
			$("#list").empty();
		}
	}
	else if(scanPageId == "settings")
	{ 
		if(barcode == 0 || barcode == null)
			$('#upc').val();
		else
			$('#upc').val(barcode);
	}
	else if(scanPageId !== "settings" && scanPageId !== "creditItems")
	{
		if(barcode == 0 || barcode == null)
			$('#'+scanPageId+"_item").val();
		else
			$('#'+scanPageId+"_item").val(barcode);
	}	
}

function scans(barcode)
{	
	if(mobileVersion)
	{
		scanAppendData(barcode);
		mobileVersion = false;
	}
	else
		webSocket.send(window.sessionStorage.getItem("userId")+"/"+barcode);
}


function openPDF(url)
{
	// Send Request
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url, true);
	xhr.responseType = 'arraybuffer';
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.onload = function (e) {
		
	// Check status
	if (this.status == 200) {
	 
	// convert response to Blob, then create URL from blob.
	var blob = new Blob([this.response], { type: 'application/pdf' }),
	fileURL = URL.createObjectURL(blob);
	$('#abc_frame').prop('src', fileURL)
	
	setTimeout(function(){
	
	$("#abc_frame").get(0).contentWindow.print();
	},200);
	} else 
	{
	console.error('response: ', this);
	 
	}
	};
	 
	xhr.send();
}


function loadInvoiceSettings()
{
	var userDetails = JSON.parse(window.sessionStorage.getItem("userDetails"));
	if(userDetails.shopLogo == "YES")
	{
		$("input[name='checkBoxLogo']").prop( "checked", true );
	 	$("input[name='checkBoxLogo']").val(userDetails.shopLogo);
	}
	if(userDetails.showDetails == "YES")
	{
		$("input[name = 'checkBoxDetails']").prop("checked",true);
	 	$("input[name = 'checkBoxDetails']").val(userDetails.showDetails);
	}
	$("input[name='checkBoxLogo']").change(function() 
	{
		if ($('#checkBoxLogo').is(":checked"))
			$("input[name='checkBoxLogo']").val("YES");
		else
			$("input[name='checkBoxLogo']").val("NO");
	});	
	$("input[name='checkBoxDetails']").change(function() 
	{
		if ($('#checkBoxDetails').is(":checked"))
			$("input[name='checkBoxDetails']").val("YES");
		else
			$("input[name='checkBoxDetails']").val("NO");
	});	
}

function saveInvoiceSettings()
{
	var existedUser = JSON.parse(window.sessionStorage.getItem("userDetails"));
	var dataToSend = JSON.stringify({ 
		shopName: 		$('#shopname').val(),
		address: 		$('#address').val(),
		city: 			$('#city').val(),
		state:		 	$('#state').val(),
		zip: 			$('#pincode').val(),
		userId:window.sessionStorage.getItem("userId"),
		discount:		$("#checkBox").val(),
		shopLogo: 		$("#checkBoxLogo").val(),
		showDetails: 	$("#checkBoxDetails").val(),
		imageName : 	existedUser.imageName
	});
	var data = callingAdminServices("POST","application/json","json","loginService/updateShopInvoice",dataToSend);
	window.sessionStorage.removeItem("userDetails");
	var userDetails = data;
	userDetails.currentSoldValue = existedUser.currentSoldValue;
	window.sessionStorage.setItem("userDetails",JSON.stringify(userDetails));
	loadInvoiceSettings();
}


varCheckId = false;
function updateInvoice(e)
{
	$("#loaderDiv").show();
	const[customer_id,invoiceNumber,cc_id] = e.id.split("/");
	window.sessionStorage.setItem("customerTransactionId",parseFloat(cc_id));
	var finalJson = JSON.parse(window.sessionStorage.getItem("customerTransactions"));
	var arr = finalJson.filter(a=>a.customer_id == parseInt(customer_id) && a.cc_id == parseInt(cc_id));
	if(window.sessionStorage.getItem("itemDetails") != null && window.sessionStorage.getItem("itemDetails") != undefined && window.sessionStorage.getItem("itemDetails") != "undefined")
	{
		var itemDetails = JSON.parse(window.sessionStorage.getItem("itemDetails"));
	}
	else
	{
		var data = callingAdminServices("GET","application/json","json","shopService/getShopItems/"+window.sessionStorage.getItem("userId"),null);
	    var itemDetails =data.data;
	    window.sessionStorage.setItem("itemDetails",JSON.stringify(itemDetails));
	}
	
	var invoiceItems = [];
	$.each(arr[0].transactions,function(key,value){
		var item = value;
		if(item.transactionQty != 0)
		{	
			var items = itemDetails.filter(a=>a.itemId == item.itemId);
			varCheckId = true;
			customerTransactionId = arr[0].cc_id;
			$.each(items,function(key,value){
				value.customerTransactionId = arr[0].cc_id;
				value.newSoldCost = item.itemSoldCost;
				value.transactionQty = item.transactionQty;
				value.existingQty= value.totalQuantity;
				value.previousQuantity= value.transactionQty;		
				value.oldCredit = arr[0].cc_prev_credit_amount;
				value.paidAmount = arr[0].cc_paid_amount;
				//value.transactionAmount = arr[0].cc_total_amount;
				//value.cc_prev_credit_amount = 
				value.transactionAmount = 0;
				value.discount = arr[0].cc_discount;
				invoiceItems.push(value);
			});
		}
	});
	window.sessionStorage.setItem("previewItems",JSON.stringify(invoiceItems))
	window.sessionStorage.setItem("updateInvoiceTransactions",JSON.stringify(invoiceItems));
	$("#content").empty();
	$.get("./pages/invoice.html", function (htmldata) {
		var totalAmount = 0;
		htmldata = htmldata.replace(/:cc_id/g,customer_id);
		htmldata = htmldata.replace(/:customerTrasanctionId/g,arr[0].cc_id);
		$(htmldata).clone().appendTo("#content");
		$(".invoice_customer_back").hide();
		$(".invoice_transactions_back").show();
	    var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
		if(!isMobile)$(".print").html("Save & Print");
		setTimeout(function(){
			$('#m_modal_2').modal('show');
			$("#previewItems").empty();
			var html;
    		$.get("./holders/previewItems.html", function (loadHTML) {
    			
    			html = loadHTML;
    			invoiceItems.forEach(function(value){
        			var htmldata = html;
        			htmldata = htmldata.replace(/:itemName/g,value.itemName);
					htmldata = htmldata.replace(/:mrp/g,parseFloat(value.newSoldCost).toFixed(2));
					htmldata = htmldata.replace(/:quantity/g,value.transactionQty);
					htmldata = htmldata.replace(/:amount/g,parseFloat(value.newSoldCost * value.transactionQty).toFixed(2));
					htmldata = htmldata.replace(/:itemId/g,value.itemId);
					htmldata = htmldata.replace(/:totalAvailabeQunatity/g,value.totalQuantity+value.previousQuantity);
					htmldata = htmldata.replace(/:totalQuantity/g,value.totalQuantity);
					htmldata = htmldata.replace(/:customerItemDel/g,12);
					$('#previewItems').append(htmldata);
					totalAmount = totalAmount + (value.newSoldCost * value.transactionQty);
					$("#TaPlusPa").text(parseFloat(totalAmount).toFixed(2));   
					$("#totalAmount").text(parseFloat(totalAmount).toFixed(2));	
					$("#previousCredit").text(parseFloat(arr[0].cc_prev_credit_amount).toFixed(2));
					$("#totalAmountCredit").text(parseFloat(arr[0].cc_prev_credit_amount+arr[0].cc_total_amount).toFixed(2));
					$("#discount").val(parseFloat(arr[0].cc_discount).toFixed(2));	
					
					$("#totalAmountPaid").val(parseFloat(arr[0].cc_paid_amount).toFixed(2));
					
					
					
					
					//$("#totalAmountPaid").val(parseFloat(totalAmount - arr[0].cc_discount).toFixed(2));
					$("#finalCredit").text(parseFloat(arr[0].cc_prev_credit_amount+arr[0].cc_total_amount-arr[0].cc_discount-arr[0].cc_paid_amount).toFixed(2));	
					//$("#totalAmountCredit").text(parseFloat(totalAmount + (-arr[0].cc_prev_credit_amount)).toFixed(2));
					//$("#totalAmountPaid").val(parseFloat(totalAmount + (-arr[0].cc_prev_credit_amount)).toFixed(2));
        		});
    			if(arr[0].cc_discount != undefined && arr[0].cc_discount != null && arr[0].cc_discount != "" && arr[0].cc_discount != 0 || JSON.parse(window.sessionStorage.getItem("userDetails")).discount == "YES"){
					$("#showDiscountColumn").show();
					$("#discount").val(parseFloat(arr[0].cc_discount).toFixed(2));
				}
				else
					$("#showDiscountColumn").hide();
            });
    		if(invoiceItems.length == 0)
    			$(".dis,.addItemTra").prop('disabled',true);
    		
		},100);
		setTimeout(function(){
			$("#loaderDiv").hide();
		},130)
	});
	
		
}

function saveExcel(transactions) 
{
	//const _ = require('lodash');
	var finalJson; 
	var excelData = [];
	var excelHeader = [];
	if(transactions != null && transactions != undefined)
	{
		//excelHeader = ["TRANSACTION DATE","ITEM NAME","UOM","CUSTOMER NAME","TRANSACTION TYPE","QUANTITY","ITEM SOLD COST","TOTAL AMOUNT"];
		//excelData = transactions;
		excelHeader = ["customerName","itemName","itemSoldCost","totalAmount","transactionDate","transactionQty","transactionType","uom"];

		$.each(transactions,function(key,value){
			
			/*var obj = {x:1, y:2, z:3};
			var result = _.omit(obj, ['x','y']);
			console.log(result);*/
			var obj = value;
			var result = _.omit(obj, ['customerId','customerTransactionId','discount','discountAmount','existingQty','gstValue','id','imageName','itemDesc','itemId','itemPurchaseCost',
				'itemUOM','newPurchaseCost','newQty','newSoldCost','oldCredit','paidAmount','previousQuantity','status','totalAmount','transactionAmount','transactionId','upcCode','userId'
			]);
			//console.log(result)
			result.totalAmount = parseFloat( result.transactionQty * result.itemSoldCost).toFixed(2); 
			excelData.push(result);
		});
	}
	else
	{
		
	excelHeader = ["ITEM NAME","ITEM DESCRIPTION","UOM","TOTAL QUANTITY","PURCHASE PRICE","MRP","STOCK VALUE"]
	if(window.sessionStorage.getItem("itemDetails") != null && window.sessionStorage.getItem("itemDetails") != undefined && window.sessionStorage.getItem("itemDetails") != "undefined")
	{
		finalJson = JSON.parse(window.sessionStorage.getItem("itemDetails"));
	}
	else
	{
		var data = callingAdminServices("GET","application/json","json","shopService/getShopItems/"+window.sessionStorage.getItem("userId"),null);
    	var finalJson =data.data;
    	window.sessionStorage.setItem("itemDetails",JSON.stringify(finalJson));
	}
	
	$.each(finalJson,function(key,value){
		
		delete value.itemId;
		delete value.imageName;
		delete value.userId;
		//delete value.itemPurchaseCost;
		delete value.newQuantity;
		delete value.newPurchaseCost;
		delete value.newSoldCost;
		delete value.gstValue;
		delete value.upcCode;
		delete value.totalAmount;
		delete value.lowQuantityValue;
		delete value.id;
		value.stockValue = parseFloat(value.itemSoldCost * value.totalQuantity).toFixed(2); 
		excelData.push(value);
	});
	}
	var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    if(isMobile)
    {
    	if(transactions != null && transactions != undefined)
    	{
    		var jsonArrays = JSON.stringify(excelData);
    		app.generateExcelFile("Transactions",excelHeader,jsonArrays,"2");
    	}
    	else
    	{
    		var jsonArray = JSON.stringify(excelData);
    		app.generateExcelFile(window.sessionStorage.getItem("shopName"),excelHeader,jsonArray,"1");
    	}
    	
    } 
    else
    {
    	if(transactions != null && transactions != undefined)
    	{
    		downloadExcel("Transactions",excelHeader,excelData);
    	}
    	else
    	{
    		downloadExcel(window.sessionStorage.getItem("shopName"),excelHeader,excelData);
    	}
    }
    	
	
	
}
